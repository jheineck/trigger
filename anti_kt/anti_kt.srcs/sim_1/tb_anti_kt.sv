`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/30/2023 02:16:40 PM
// Design Name: 
// Module Name: tb_anti_kt_stage
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "defines.vh"
module tb_anti_kt ();
localparam integer LAST_WRITE = BC_SIZE%32;
localparam integer FULL_WRITES = BC_SIZE/32;
	logic clk;
	logic reset;
//	bc_type bc_in;
	logic [WAIT_STATES_NUM_BITS-1:0] num_wait_states; // typically 36?
	logic fsm_go, fsm_done, fsm_busy;
//	logic report_go, report_done, report_busy;
	logic new_bunch_crossing, end_bunch_crossing;
	logic [($clog2(NUM_ROIS))-1:0] report_roi_num;
	logic [($clog2(MAX_CLUSTERS))-1:0] report_cluster_num;
	cluster_type report_jet;
	logic report_is_a_jet;
	logic [($clog2(MAX_CLUSTERS)):0] report_merge_count;
`ifdef POST_SYNTHESIS
	wire [ERROR_NUM_BITS-1:0]\report_jet[error_bits] ;
	wire [ENERGY_NUM_BITS-1:0]\report_jet[ptt_x] ;
	wire [ENERGY_NUM_BITS-1:0]\report_jet[ptt_y] ;
	wire [ENERGY_NUM_BITS-1:0]\report_jet[ptt_z] ;
	wire [PHI_NUM_BITS:0]\report_jet[phi] ;
	wire [ETA_NUM_BITS:0]\report_jet[eta] ;
	assign report_jet.error_bits = \report_jet[error_bits] ;
	assign report_jet.ptt_x = \report_jet[ptt_x] ;
	assign report_jet.ptt_y = \report_jet[ptt_y] ;
	assign report_jet.ptt_z = \report_jet[ptt_z] ;
	assign report_jet.phi = \report_jet[phi] ;
	assign report_jet.eta = \report_jet[eta] ;
	
	wire [ERROR_NUM_BITS-1:0]\cluster_in[error_bits] ;
	wire [ENERGY_NUM_BITS-1:0]\cluster_in[ptt_x] ;
	wire [ENERGY_NUM_BITS-1:0]\cluster_in[ptt_y] ;
	wire [ENERGY_NUM_BITS-1:0]\cluster_in[ptt_z] ;
	wire [PHI_NUM_BITS:0]\cluster_in[phi] ;
	wire [ETA_NUM_BITS:0]\cluster_in[eta] ;
	assign \cluster_in[error_bits] = cluster_in.error_bits;
	assign \cluster_in[ptt_x] = cluster_in.ptt_x;
	assign \cluster_in[ptt_y] = cluster_in.ptt_y;
	assign \cluster_in[ptt_z] = cluster_in.ptt_z;
	assign \cluster_in[phi] = cluster_in.phi;
	assign \cluster_in[eta] = cluster_in.eta;
	
	wire [ENERGY_NUM_BITS-1:0]\center[ptt] ;
	wire [PHI_NUM_BITS:0]\center[phi] ;
	wire [ETA_NUM_BITS:0]\center[eta] ;
	assign \center[ptt] = center.ptt;
	assign \center[phi] = center.phi;
	assign \center[eta] = center.eta;
`endif
	logic report_jet_valid;
`ifdef LOAD_BC_PARTS	
	typedef union packed {
		bc_type bc_part;
		logic [BC_SIZE-1:0] io_part;
	} bc_io;
	bc_io bc_stuff;
	reg [31:0] bc_io_stuff;
	reg write_bc_io_stuff;
	wire bc_io_stuff_ack;
	reg [($clog2(BC_SIZE/BC_IO_NUM_BITS))-1:0] io_part_num;
	reg [63:0] bc_checksum;
`endif
`ifdef FULL_BC
	cluster_type cluster_in;
	reg cluster_in_valid;
//	wire bc_io_stuff_ack; // might not need

	cluster_type cluster_mem [0:199];
	reg [($clog2(NUM_ROIS))-1:0] roi_indx;
	gblock_type center;
	reg center_valid;
	gblock_type roi_centers [NUM_ROIS-1:0];
	assign center = roi_centers[roi_indx];
//	logic preproc_go, preproc_busy, preproc_done, preproc_rdy;
//	logic [31:0] disabled [3:0]; // there are always 32 clusters/roi, 4 rois in the datasets
`endif

	anti_kt_guts aktg (
		.clk(clk),
		.reset(reset),
//		.bc_io_stuff_ack(bc_io_stuff_ack),
`ifdef LOAD_BC_PARTS
		.bc_io_stuff(bc_io_stuff),
		.write_bc_io_stuff(write_bc_io_stuff),
		.io_part_num(io_part_num),
`endif
`ifdef FULL_BC
		.cluster_in_valid(cluster_in_valid),
`ifdef POST_SYNTHESIS
		.\cluster_in[error_bits] (\cluster_in[error_bits] ),
		.\cluster_in[ptt_x] (\cluster_in[ptt_x] ),
		.\cluster_in[ptt_y] (\cluster_in[ptt_y] ),
		.\cluster_in[ptt_z] (\cluster_in[ptt_z] ),
		.\cluster_in[phi] (\cluster_in[phi] ),
		.\cluster_in[eta] (\cluster_in[eta] ),
		.\center[ptt] (\center[ptt] ),
		.\center[phi] (\center[phi] ),
		.\center[eta] (\center[eta] ),
`else
		.cluster_in(cluster_in),
		.center(center),
`endif
		.roi_indx(roi_indx),
		.center_valid(center_valid),
//		.preproc_go(preproc_go),
//		.preproc_busy(preproc_busy),
//		.preproc_rdy(preproc_rdy),
//		.preproc_done(preproc_done),
`endif
		.num_wait_states(num_wait_states),
//		.report_go(report_go),
//		.report_busy(report_busy),
//		.report_done(report_done),
		.report_roi_num(report_roi_num),
		.report_jet_valid(report_jet_valid),
		.report_cluster_num(report_cluster_num),
		.report_is_a_jet(report_is_a_jet),
		.report_merge_count(report_merge_count),
`ifdef POST_SYNTHESIS
		.\report_jet[error_bits] (\report_jet[error_bits] ),
		.\report_jet[ptt_x] (\report_jet[ptt_x] ),
		.\report_jet[ptt_y] (\report_jet[ptt_y] ),
		.\report_jet[ptt_z] (\report_jet[ptt_z] ),
		.\report_jet[phi] (\report_jet[phi] ),
		.\report_jet[eta] (\report_jet[eta] ),
`else		
		.report_jet(report_jet),
`endif		
        .new_bunch_crossing(new_bunch_crossing),
        .end_bunch_crossing(end_bunch_crossing),
		.fsm_go(fsm_go),
		.fsm_busy(fsm_busy),
		.fsm_done(fsm_done)
	);

`ifdef PRE_PROCESS_BC
//`define SORT_CLUSTERS
`ifdef SORT_CLUSTERS
	parameter type OBJECT_TYPE = cluster_ptt_type;
	reg cluster_in_valid;
	OBJECT_TYPE cluster_ptt_in;
	cluster_type cluster_in;
	logic [($clog2(MAX_CLUSTERS))-1:0] cluster_in_num;
	
	OBJECT_TYPE cluster_val [MAX_CLUSTERS-1:0];
	logic [($clog2(MAX_CLUSTERS))-1:0] cluster_val_num [MAX_CLUSTERS-1:0];
	OBJECT_TYPE cluster_pass [MAX_CLUSTERS-1:0];
	logic [($clog2(MAX_CLUSTERS))-1:0] cluster_pass_num [MAX_CLUSTERS-1:0];
	reg clear_clusters;
	
	genvar jj;
	generate
		for(jj=0;jj<MAX_CLUSTERS;jj++) begin
			roi_serial_sort #(.OBJECT_TYPE(OBJECT_TYPE),
								.NUM_STAGES(MAX_CLUSTERS)) ss (
				.clk(clk),
				.reset(reset),
				.new_bunch_crossing(clear_clusters),
				.roi_in_valid(cluster_in_valid),
				.roi_in((jj==0) ? cluster_ptt_in : cluster_pass[jj-1]),
				.roi_in_num((jj==0) ? cluster_in_num : cluster_pass_num[jj-1]),
				.roi_val(cluster_val[jj]),
				.roi_val_num(cluster_val_num[jj]),
				.roi_pass(cluster_pass[jj]),
				.roi_pass_num(cluster_pass_num[jj])
			);
		end
	endgenerate
	
	cluster_ptt_type cluster_ptt;
    assign cluster_ptt_in.eta = cluster_in.eta;
    assign cluster_ptt_in.phi = cluster_in.phi;
    assign cluster_ptt_in.ptt_x = cluster_in.ptt_x;
    assign cluster_ptt_in.ptt_y = cluster_in.ptt_y;
    assign cluster_ptt_in.ptt_z = cluster_in.ptt_z;
//    assign cluster_ptt_in.ptt = cluster_in.ptt_x + cluster_in.ptt_y + cluster_in.ptt_z;
    assign cluster_ptt_in.ptt = (cluster_in.ptt_x*cluster_in.ptt_x) + (cluster_in.ptt_y*cluster_in.ptt_y);
    assign cluster_ptt_in.error_bits = cluster_in.error_bits;
		
`endif			
	logic [DR2_NUM_BITS-1:0] computed_dr2;
	logic cluster_got_energy;
	function [DR2_NUM_BITS-1:0] new_dr2 (input integer f_roi_num,x,y);
		integer dx, dy, dya;
		integer fetax, fphix, fetay, fphiy;
		begin
//			$display("roi=%1d, x=%2d, y=%2d",f_roi_num,x,y);
            fetax = (bc_in.roi[f_roi_num].clusters.cluster[x].eta);
            fetay = (bc_in.roi[f_roi_num].clusters.cluster[y].eta);
            fphix = (bc_in.roi[f_roi_num].clusters.cluster[x].phi);
            fphiy = (bc_in.roi[f_roi_num].clusters.cluster[y].phi);
			if(x==y) begin
//				dx = bc_in.roi[f_roi_num].clusters.cluster[x].eta;
//				dy = bc_in.roi[f_roi_num].clusters.cluster[x].phi;
//				$display("x=y=%2d, roi %1d, dx %5d dy %5d",x,f_roi_num,dx,dy);
//				$display("x=y=%2d, roi %1d",x,f_roi_num);
				new_dr2 = R2PAR; // R2par = 16 corresponds to RakT = 0.4
				dx = 0;
				dy = 0;
			end else begin
//			    $display("x=%2d, y=%2d, roi %1d, fetax %d, fphix %d, fetay %d, fphiy %d",x,y,f_roi_num,fetax,fphix,fetay,fphiy);
			    dx = (fetay > fetax) ? (fetay - fetax) : (fetax - fetay);
			    dya = (fphiy > fphix) ? (fphiy - fphix) : (fphix - fphiy);
			    dy = (dya > 32) ? (64 - dya) : dya;
//				dx = (bc_in.roi[f_roi_num].clusters.cluster[x].eta -
//					bc_in.roi[f_roi_num].clusters.cluster[y].eta);
//				dy = (bc_in.roi[f_roi_num].clusters.cluster[x].phi -
//					bc_in.roi[f_roi_num].clusters.cluster[y].phi);
//				$display("x=%2d, y=%2d, roi %1d, dx %5d dy %5d",x,y,f_roi_num,dx,dy);
				new_dr2 = (dx*dx)+(dy*dy);
			end
//			$display("new dr2 %6d",new_dr2);
		end
	endfunction
	
	function pt2inv_type new_pt2inv (input integer f_roi_num,row);
		logic [PT2INV_NUM_BITS-1:0] ptx2, pty2, pt2;
		begin
			ptx2 = bc_in.roi[f_roi_num].clusters.cluster[row].ptt_x * bc_in.roi[f_roi_num].clusters.cluster[row].ptt_x;
			pty2 = bc_in.roi[f_roi_num].clusters.cluster[row].ptt_y * bc_in.roi[f_roi_num].clusters.cluster[row].ptt_y;
			pt2 = ptx2 + pty2;
			new_pt2inv = ~pt2;
		end
	endfunction
`endif
	initial begin
		clk = 0;
	end
	always #1.5625 clk = ~clk; // 320 MHz
//	always #1.785 clk = ~clk; // 280 MHz
//	always #20 clk = ~clk; // REAL SLOW!
	
//`define FULL_ROIS
`ifdef FULL_ROIS
	cluster_type cluster_mem [0:127];
`endif
	integer fd;
	integer result_num;
	integer result_roi_num [20:0];
	integer result_phi [20:0];
	integer result_eta [20:0];
	integer num_jets_found;
	integer result_test_num;
	integer result_good;
	integer results_good;
`ifdef REPORT_PROGRESS
	integer progress_fd;
`endif
	always @(posedge clk or posedge reset) begin
		if(reset) begin
			result_good = 0;
		end else if(report_jet_valid) begin
			num_jets_found++;
`ifdef REPORT_PROGRESS
			if(progress_fd != 0) begin
				$fwrite(progress_fd,"%1d, %2d, %p, %1d, %2d ",report_roi_num,report_cluster_num,report_jet,report_is_a_jet,report_merge_count);
			end
`endif
			$write("%1d, %2d, %p, %1d, %2d ",report_roi_num,report_cluster_num,report_jet,report_is_a_jet,report_merge_count);
			if(fd != 0) begin
				result_good = 0;
				for(result_test_num=0;result_test_num<result_num;result_test_num++) begin
					if(result_roi_num[result_test_num] == report_roi_num) begin
						if((result_phi[result_test_num] == report_jet.phi) &&
							(result_eta[result_test_num] == report_jet.eta)) begin
`ifdef REPORT_PROGRESS
								if(progress_fd != 0) begin
									$fwrite(progress_fd,"it's good\n");
								end
`endif
							$write("it's good\n");
							result_good = 1;
						end
					end
				end
				if(result_good != 1) begin
`ifdef REPORT_PROGRESS
					if(progress_fd != 0) begin
						$fwrite(progress_fd,"it's bad!\n");
					end
`endif
					$write("it's bad!\n");
				end else begin
					results_good++;
				end
			end
		end
	end

	integer i,j,k;
	integer dataset, dummyset;
	string sdataset;
	string prefix;
	string prefix2;
	initial begin
//	   $display($bits(bc_in));
		reset = 1;
//		bc_in = 0;
		fsm_go = 0;
//		report_go = 0;
		new_bunch_crossing = 0;
		end_bunch_crossing = 0;
		num_wait_states = NUM_WAIT_STATES; // TODO: get this from a file
`ifdef SORT_CLUSTERS
		cluster_in = 0;
		cluster_in_num = 0;
		cluster_in_valid = 0;
		clear_clusters = 0;
`endif		
`ifdef REPORT_PROGRESS
		progress_fd = 0;
`endif
`ifdef LOAD_BC_PARTS
		bc_io_stuff = 0;
		io_part_num = 0;
		write_bc_io_stuff = 0;
		bc_checksum = 0;
`endif
		fd = 0;
`ifdef FULL_BC
		roi_indx = 0;
//		center = 0;
//		preproc_go = 0;
		cluster_in_valid = 0;
		cluster_in = 0;
		center_valid = 0;
		for(i=0;i<200;i++) begin
			cluster_mem[i] = '{0,0,0,0,0,0};
		end
`endif
		num_jets_found = 0;
		results_good = 0;
        for(dataset=1;dataset<7;dataset++) begin
//        if(dataset==2) dummyset = 2; else dummyset = 3;
        sdataset.itoa(dataset);
//        sdataset.itoa(dummyset);
        $display({"Using Dataset_",sdataset});
`ifdef REPORT_PROGRESS
		if(progress_fd != 0) begin
			$fclose(progress_fd);
		end
		$stop;
`ifdef RIVIERA
`ifdef POST_SYNTHESIS
		prefix = "../../../../../../";
`else
		prefix = "../../../../../";
`endif
`else
		prefix = "../../../../../";
`endif
`endif
		prefix2 = "jets/Datasets_32/Dataset_";
		progress_fd = $fopen({prefix,prefix2,sdataset,"/progress.txt"},"w");
		$readmemh({prefix,prefix2,sdataset,"/full_rois.txt"},cluster_mem);
		$readmemh({prefix,prefix2,sdataset,"/roi_centers.txt"},roi_centers);
//		$readmemh({prefix,prefix2,sdataset,"/disabled.txt"},disabled);
		fd = $fopen({prefix,prefix2,sdataset,"/expected.txt"},"r");
		if(fd != 0) begin
			result_num = 0;
			while(!$feof(fd)) begin
				$fscanf(fd,"%d,%d,%d",result_roi_num[result_num],result_phi[result_num],result_eta[result_num]);
				result_num = result_num + 1;
			end
			$fclose(fd);
		end
//		$readmemh(JETS,cluster_mem);
		for(i=0;i<128;i++) begin
			cluster_mem[i].error_bits = i;
		end
//		for(i=0;i<3;i++) begin
//			for(j=0;j<32;j++) begin
//				if(disabled[i][j]) begin
//					cluster_mem[i*32+j].ptt_x = 0;
//					cluster_mem[i*32+j].ptt_y = 0;
//					cluster_mem[i*32+j].ptt_z = 0;
//				end
//			end
//		end
//`define DISABLE_ONES
`ifdef DISABLE_ONES
		for(i=0;i<NUM_ROIS;i++) begin
			for(j=0;j<MAX_CLUSTERS;j++) begin
				k = i*MAX_CLUSTERS+j;
				if((cluster_mem[k].ptt_x +
						cluster_mem[k].ptt_y +
						cluster_mem[k].ptt_z) == 1) begin
					cluster_mem[k].ptt_x = 0;
					cluster_mem[k].ptt_y = 0;
					cluster_mem[k].ptt_z = 0;
				end
			end
		end
`endif
		repeat (5) @(posedge clk);
		@(posedge clk) reset <= 0;
		#200;
		@(posedge clk);
`ifdef PRE_PROCESS_BC
`ifdef SORT_CLUSTERS
		for(i=0;i<NUM_ROIS;i++) begin
			@(posedge clk) clear_clusters <= 1;
			@(posedge clk) clear_clusters <= 0;
			for(j=0;j<MAX_CLUSTERS;j++) begin
				@(posedge clk) cluster_in <= cluster_mem[i*MAX_CLUSTERS+j];
				@(posedge clk) cluster_in_valid <= 1;
				@(posedge clk) cluster_in_valid <= 0; cluster_in_num <= cluster_in_num + 1;
			end
			@(posedge clk);
			for(j=0;j<MAX_CLUSTERS;j++) begin
//				bc_in.roi[i].clusters.cluster[j] = cluster_val[j];
				 bc_in.roi[i].clusters.cluster[j].eta = cluster_val[j].eta;
				 bc_in.roi[i].clusters.cluster[j].phi = cluster_val[j].phi;
				 bc_in.roi[i].clusters.cluster[j].ptt_x = cluster_val[j].ptt_x;
				 bc_in.roi[i].clusters.cluster[j].ptt_y = cluster_val[j].ptt_y;
				 bc_in.roi[i].clusters.cluster[j].ptt_z = cluster_val[j].ptt_z;
				 bc_in.roi[i].clusters.cluster[j].error_bits = cluster_val[j].error_bits;
			end
		end
				
`else
		for(i=0;i<NUM_ROIS;i++) begin
			for(j=0;j<MAX_CLUSTERS;j++) begin
				bc_in.roi[i].clusters.cluster[j] = cluster_mem[i*MAX_CLUSTERS+j];

//				bc_in.roi[0].clusters.cluster[j] = cluster_mem[1*MAX_CLUSTERS+j];
//				bc_in.roi[0].clusters.cluster[j] = 0;
//				bc_in.roi[1].clusters.cluster[j] = cluster_mem[1*MAX_CLUSTERS+j];
//				bc_in.roi[1].clusters.cluster[j] = 0;
//				bc_in.roi[2].clusters.cluster[j] = cluster_mem[1*MAX_CLUSTERS+j];
//				bc_in.roi[2].clusters.cluster[j] = 0;
//				bc_in.roi[3].clusters.cluster[j] = cluster_mem[1*MAX_CLUSTERS+j];
//				bc_in.roi[3].clusters.cluster[j] = 0;
			end
		end
`endif
		for(i=0;i<NUM_ROIS;i++) begin
			for(j=0;j<MAX_CLUSTERS;j++) begin
				cluster_got_energy = ((bc_in.roi[i].clusters.cluster[j].ptt_x != 0) |
										(bc_in.roi[i].clusters.cluster[j].ptt_y != 0) |
										(bc_in.roi[i].clusters.cluster[j].ptt_z != 0));
//`define GOOSE_PTTX
`ifdef GOOSE_PTTX
				if(cluster_got_energy) begin
					bc_in.roi[i].clusters.cluster[j].ptt_x = bc_in.roi[i].clusters.cluster[j].ptt_x + 20;
				end
`endif
				
				for(k=0;k<MAX_CLUSTERS;k++) begin
//					bc_in.roi[i].roi_dr2.dr2_row[j].dr2[k] = new_dr2(i,j,k);
                    computed_dr2 = new_dr2(i,j,k);
					bc_in.roi[i].roi_dr2.dr2_row[j].dr2[k] = computed_dr2;
//					@(posedge clk);
				end
				bc_in.roi[i].roi_dr2.dr2_row[j].pt2inv = new_pt2inv(i,j);
//				bc_in.roi[i].roi_dr2.dr2_row[j].pt2inv_update_required = 0;
				
				bc_in.roi[i].disabled[j] = ((j > MAX_CLUSTERS) || (!cluster_got_energy)) ? 1'b1 : 1'b0;
				bc_in.roi[i].is_a_jet[j] = 0;
//				bc_in.roi[i].token[j] = 0;
			end
		end
		for(i=0;i<NUM_ROIS;i++) begin
			bc_in.roi[i].roi_num = i;
			bc_in.roi[i].iteration = 0;
		end
		
/*
		// sanity check
		for(j=0;j<MAX_CLUSTERS;j++) begin
		  for(k=0;k<32;k++) begin
		      if(bc_in.roi[1].roi_dr2.dr2_row[j].dr2[k] != bc_in.roi[3].roi_dr2.dr2_row[j].dr2[k]) begin
		          $display("at (%2d,%2d), roi[1] gets %d, roi[3] gets %d",j,k,
		          bc_in.roi[1].roi_dr2.dr2_row[j].dr2[k], bc_in.roi[3].roi_dr2.dr2_row[j].dr2[k]);
		      end
		  end
        end
*/
`endif // PRE_PROCESS_BC
//`endif
`ifdef LOAD_BC_PARTS
		bc_stuff.bc_part.roi[0] = bc_in.roi[0];
		bc_stuff.bc_part.roi[1] = bc_in.roi[1];
		bc_stuff.bc_part.roi[2] = bc_in.roi[2];
		bc_stuff.bc_part.roi[3] = bc_in.roi[3];
		bc_checksum = 0;
//		$display("FULL_WRITES %d, LAST_WRITE %d",FULL_WRITES,LAST_WRITE);
		for(i=0;i<FULL_WRITES;i++) begin
//			$display("writing [%1d:%1d]",(i*32)+31,(i*32));
			@(negedge clk) begin
				bc_io_stuff <= bc_stuff.io_part[(i*32) +: 32];
				io_part_num <= i;
			end
			@(negedge clk) write_bc_io_stuff <= 1;
			wait(bc_io_stuff_ack);
			@(negedge clk) write_bc_io_stuff <= 0;
			wait(!bc_io_stuff_ack);
			bc_checksum = bc_checksum + bc_io_stuff;
//			assert (bc_checksum == aktg.akt.bc_checksum);
`ifndef POST_SYNTHESIS
			if(bc_checksum !== aktg.akt.bc_checksum) begin
				$display("checksum fault");
				$stop;
			end
`endif
		end
		if(LAST_WRITE > 0) begin
//			$display("writing [%1d:%1d] to [%1d:%1d]",(FULL_WRITES*32)+LAST_WRITE-1,FULL_WRITES*32,LAST_WRITE-1,0);
			bc_io_stuff = 0;
			@(negedge clk) begin
				bc_io_stuff[0 +: LAST_WRITE] <= bc_stuff.io_part[(FULL_WRITES*32) +: LAST_WRITE];
				io_part_num <= FULL_WRITES;
			end
			@(negedge clk) write_bc_io_stuff <= 1;
			wait(bc_io_stuff_ack);
			@(negedge clk) write_bc_io_stuff <= 0;
			wait(!bc_io_stuff_ack);
			bc_checksum = bc_checksum + bc_io_stuff;
		end
//		$display(bc_checksum);
`endif // LOAD_BC_PARTS
`ifdef FULL_BC
		// just for testing
//		roi_centers[0] = '{1,2,3};
//		roi_centers[1] = '{3,4,5};
//		roi_centers[2] = '{5,6,7};
//		roi_centers[3] = '{7,8,9};
/*
		for(roi_indx=0;roi_indx<NUM_ROIS-1;roi_indx++) begin
//				center = roi_centers[roi_indx];
				@(negedge clk) write_center <= 1;
//				@(posedge clk) write_center <= 0;
		end	
		@(negedge clk) write_center <= 0;
*/			
		@(posedge clk) num_jets_found <= 0; new_bunch_crossing <= 1; results_good <= 0;
		repeat (2) @(posedge clk);
		@(posedge clk) new_bunch_crossing <= 0;
		@(negedge clk) begin
			center_valid <= 1; roi_indx <= 0;
		end
		@(negedge clk) roi_indx <= 1;
		@(negedge clk) roi_indx <= 2;
		@(negedge clk) roi_indx <= 3;
		@(negedge clk) center_valid <= 0;

//		for(j=0;j<200;j++) begin
//			bc_io_stuff = cluster_mem[j];
//			@(negedge clk) write_bc_io_stuff <= 1;
//			wait(bc_io_stuff_ack);
//			@(negedge clk) write_bc_io_stuff <= 0;
//			wait(!bc_io_stuff_ack);
//		end
		@(negedge clk) begin
			cluster_in = cluster_mem[0];
			cluster_in_valid <= 1;
		end
		for(j=1;j<200;j++) begin
			@(negedge clk) cluster_in <= cluster_mem[j];
		end
		@(negedge clk) cluster_in_valid <= 0;
`endif	// FULL_BC
					
//		@(posedge clk) preproc_go <= 1;
//		wait(preproc_busy);
//		@(posedge clk) preproc_go <= 0;
//		wait(preproc_done);
//		wait(!preproc_done);
		@(posedge clk) fsm_go <= 1;
		wait(fsm_busy);
		@(posedge clk) fsm_go <= 0;
		wait(fsm_done);
		wait(!fsm_done);
		
/*
		for(i=0;i<(NUM_BUCKETS*STAGES_PER_BUCKET);i++) begin
			for(read_addr=0;read_addr<num_stored[i];read_addr++) begin
				$display("queue %1d has %1d, %2d, %p",i,o_roi_num[i],o_cluster_num[i],o_jet[i]);
				@(posedge clk) pop_enable[i] <= 1;
				@(posedge clk) pop_enable[i] <= 0;
				@(posedge clk);
			end
		end
*/		
//		@(posedge clk) report_go <= 1;
//		wait(report_busy);
//		@(posedge clk) report_go <= 0;
//		wait(report_done);
//		wait(!report_done);
		@(posedge clk) end_bunch_crossing <= 1;
		@(posedge clk) end_bunch_crossing <= 0;
		
`ifdef REPORT_PROGRESS
			if(progress_fd != 0) begin
				$fwrite(progress_fd,"%d jets found, ",num_jets_found);
				if(fd != 0) begin
					if(results_good == num_jets_found) begin
						$fwrite(progress_fd,"%0d are good, ",results_good);
					end else begin
						$fwrite(progress_fd,"only %0d are good, ",results_good);
					end
					if(num_jets_found == result_num) begin
						$fwrite(progress_fd,"correct\n");
					end else begin
						$fwrite(progress_fd,"expected %1d jets!\n",result_num);
					end
				end
				$fclose(progress_fd);
				progress_fd = 0;
			end
`endif
		$write("%d jets found, ",num_jets_found);
		if(fd != 0) begin
			if(results_good == num_jets_found) begin
				$write("%0d are good, ",results_good);
			end else begin
				$write("only %0d are good, ",results_good);
			end
			if(num_jets_found == result_num) begin
				$write("correct\n");
			end else begin
				$write("expected %1d jets!\n",result_num);
			end
//			$fclose(fd);
			fd = 0;
		end
		end
		#10 $stop;
		$finish;
	end
endmodule

