`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/21/2023 05:11:08 PM
// Design Name: 
// Module Name: tb_bitonicsort32
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "defines.vh"


module tb_bitonicsort32();
`ifdef DOIT
	logic reset;
    dr2_row_type dr2_row;
    dr2_type minimum;
    logic [MAX_CLUSTERS-1:0] disabled;
    logic [($clog2(MAX_CLUSTERS))-1:0] cluster_num;
    
    bitonicsort32 bs (.*);
    integer i;
`endif
	integer size, io_num;
    initial begin
//    	$display("%d: bc is %d, DEPTH is %d, WIDTH is %d",MAX_CLUSTERS,$bits(bc_type),IO.DEPTH,IO.WIDTH);
		size = $bits(bc_type);
    	$display("%d: bc is %d",MAX_CLUSTERS,size);
    	for(io_num=0;(io_num+1)*32 <= size;io_num++) begin
    		$display("for io_num = %d, io_stuff[%d:%d] -> bc_stuff[%d,%d]",io_num,(io_num*32)+31,(io_num*32),31,0);
    	end
    	if((size-((io_num)*32)) > 0) begin
    		$display("last for io_num = %d, io_stuff[%d:%d] -> bc_stuff[%d,%d]",io_num,size-1,((io_num)*32),(size-((io_num)*32))-1,0);
    	end
`ifdef DOIT
    	disabled = 0;
    	dr2_row = 0;
    	for(i=0;i<MAX_CLUSTERS;i++) begin
    		dr2_row.dr2[i].deltar2 = i;
//    		dr2_row.dr2[i].ptt = i*2;
//    		dr2_row.dr2[i].cluster_num = i;
    	end
//    	dr2_row.cluster_num = 999;
		
		for(i=0;i<MAX_CLUSTERS;i++) begin
			#1 $display("minimum is %d: %p",cluster_num, minimum);
//			dr2_row.dr2[minimum.cluster_num].dr2 = ~0;
			disabled[cluster_num] = 1'b1;
		end
`endif
		#1 $stop;
	end
endmodule
