`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/30/2023 10:35:50 AM
// Design Name: 
// Module Name: tb_update_roi
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "defines.vh"

module tb_update_roi();
	logic reset;
	roi_type roi_ur_in, roi_ur_out;
	logic disable_N;
	logic [($clog2(MAX_CLUSTERS))-1:0] N_to_disable;
	logic [($clog2(MAX_CLUSTERS))-1:0] M_to_give_N_to;
	cluster_type jet;
	logic N_is_a_jet;
	
	update_roi uut (.*);
	
	initial begin
		reset = 1;
		roi_ur_in = 0;
		disable_N = 0;
		N_to_disable = 0;
		M_to_give_N_to = 0;
		N_is_a_jet = 0;
		
		#10 reset = 0;
		#10 roi_ur_in.disabled = 32'h10000;
			roi_ur_in.is_a_jet = 32'h10000; // ensure incoming gets preserved
			$display("should be 00010000 : 00010000");
		#10 $display("%h : %h",roi_ur_in.disabled, roi_ur_out.disabled);
		#10 disable_N = 1;
			$display("should be 00010000 : 00010001");
		#10 $display("%h : %h",roi_ur_in.disabled, roi_ur_out.disabled);
			$display("should be 00010000 : 00010000");
			$display("%h : %h",roi_ur_in.is_a_jet, roi_ur_out.is_a_jet);
		#10 N_is_a_jet = 1;
			$display("should be 00010000 : 00010001");
		#10 $display("%h : %h",roi_ur_in.is_a_jet, roi_ur_out.is_a_jet);
		#10 roi_ur_in.disabled = 32'hFFFF0000;
			roi_ur_in.is_a_jet = 32'hFFFF0000; // ensure incoming gets preserved
			disable_N = 0; N_is_a_jet = 0;
			M_to_give_N_to = 2;
			roi_ur_in.clusters.cluster[1].ptt_x = 5;
			roi_ur_in.clusters.cluster[1].ptt_y = 10;
			roi_ur_in.clusters.cluster[1].ptt_z = 15;
			roi_ur_in.clusters.cluster[1].eta = 20;
			roi_ur_in.clusters.cluster[1].phi = 25;
			$display("should be ffff0000 : ffff0000");
		#10 $display("%h : %h",roi_ur_in.disabled, roi_ur_out.disabled);
			$display("should be 0,5,10,15,25,20    0,0,0,0,0,0");
			$display("%p, %p",roi_ur_in.clusters.cluster[1],roi_ur_in.clusters.cluster[2]);
			$display("should be 0,5,10,15,25,20    0,0,0,0,0,0");
			$display("%p, %p",roi_ur_out.clusters.cluster[1],roi_ur_out.clusters.cluster[2]);
		#10 disable_N = 1;
			$display("should be ffff0000 : ffff001");
		#10 $display("%h : %h",roi_ur_in.disabled, roi_ur_out.disabled);
			$display("should be 0,5,10,15,25,20    0,0,0,0,0");
			$display("%p, %p",roi_ur_in.clusters.cluster[1],roi_ur_in.clusters.cluster[2]);
			$display("should be 0,5,10,15,25,20    0,5,10,15,0,0");
			$display("%p, %p",roi_ur_out.clusters.cluster[1],roi_ur_out.clusters.cluster[2]);
			$display("should be ffff0000 : ffff0000");
			$display("%h : %h",roi_ur_in.is_a_jet, roi_ur_out.is_a_jet);
		#10 N_is_a_jet = 1;
			$display("should be ffff0000 : ffff0001");
		#10 $display("%h : %h",roi_ur_in.is_a_jet, roi_ur_out.is_a_jet);
			$display("should be 00010000 : 00010001");
			$display("%h : %h",roi_ur_in.disabled, roi_ur_out.disabled);
			$display("should be 00010000 : 00010001");
			$display("%h : %h",roi_ur_in.is_a_jet, roi_ur_out.is_a_jet);
			$display("should be 0,5,10,15,25,20    0,0,0,0,0");
			$display("%p, %p",roi_ur_in.clusters.cluster[1],roi_ur_in.clusters.cluster[2]);
			$display("should be 0,5,10,15,25,20    0,0,0,0,0");
			$display("%p, %p",roi_ur_out.clusters.cluster[1],roi_ur_out.clusters.cluster[2]);
/*
		#10 roi_num = 2; disable_N = 0; N_is_a_jet = 0;
			$display("should be 0 0");
			$display(roi_ur_in.roi_dr2.dr2_row[0].pt2inv_update_required,
					roi_ur_out.roi_dr2.dr2_row[0].pt2inv_update_required);
		#10 roi_ur_in.roi_dr2.dr2_row[0].pt2inv_update_required = 1;
			$display("should be 1 0");
		#10 $display(roi_ur_in.roi_dr2.dr2_row[0].pt2inv_update_required,
					roi_ur_out.roi_dr2.dr2_row[0].pt2inv_update_required);
		#10 disable_N = 1; N_is_a_jet = 1;
			$display("should be 1 0");
		#10 $display(roi_ur_in.roi_dr2.dr2_row[0].pt2inv_update_required,
					roi_ur_out.roi_dr2.dr2_row[0].pt2inv_update_required);
		#10 N_is_a_jet = 0;
			$display("should be 1 0");
		#10 $display(roi_ur_in.roi_dr2.dr2_row[0].pt2inv_update_required,
					roi_ur_out.roi_dr2.dr2_row[0].pt2inv_update_required);
			$display("should be 0 1");
			$display(roi_ur_in.roi_dr2.dr2_row[2].pt2inv_update_required,
					roi_ur_out.roi_dr2.dr2_row[2].pt2inv_update_required);
		#10 roi_ur_in.roi_dr2.dr2_row[0].pt2inv_update_required = 0;
			$display("should be 0 0");
		#10 $display(roi_ur_in.roi_dr2.dr2_row[0].pt2inv_update_required,
					roi_ur_out.roi_dr2.dr2_row[0].pt2inv_update_required);
			$display("should be 0 : 0");
			$display("%d : %d",roi_ur_in.roi_dr2.dr2_row[0].pt2inv,
								roi_ur_out.roi_dr2.dr2_row[0].pt2inv);
		#10 roi_ur_in.roi_dr2.dr2_row[0].pt2inv = 99;
			$display("should be 99 : 99");
		#10 $display("%d : %d",roi_ur_in.roi_dr2.dr2_row[0].pt2inv,
								roi_ur_out.roi_dr2.dr2_row[0].pt2inv);
*/
		$stop;
	end
endmodule
