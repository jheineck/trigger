`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03/12/2024 04:15:51 PM
// Design Name: 
// Module Name: akt_vio
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "defines.vh"

// remember that inputs to the guts are outputs from vio
// remember that outputs from the guts are inputs to vio
module akt_vio(
    input clk,
    output [0:0] reset,
    output [WAIT_STATES_NUM_BITS-1:0] num_wait_states,
    output [0:0] new_bunch_crossing,
    output [0:0] end_bunch_crossing,
    output [0:0] fsm_go,
    input [0:0] fsm_busy,
    input [0:0] fsm_done,
    input [($clog2(NUM_ROIS))-1:0] report_roi_num,
    input [($clog2(MAX_CLUSTERS))-1:0] report_cluster_num,
    input cluster_type report_jet,
    input [0:0] report_jet_valid,
    output [0:0] report_go,
    input [0:0] report_busy,
    input [0:0] report_done,
`ifdef LOAD_BC_PARTS
    output [0:0] write_bc_io_stuff,
    input [0:0] bc_io_stuff_ack,
    output [BC_IO_NUM_BITS-1:0] bc_io_stuff,
    output [($clog2(BC_SIZE/BC_IO_NUM_BITS))-1:0] io_part_num
`endif // LOAD_BC_PARTS
`ifdef FULL_BC
	output cluster_type cluster_in,
	output [0:0] cluster_in_valid,
	output [($clog2(NUM_ROIS))-1:0] roi_indx,
	output gblock_type center,
	output [0:0] center_valid,
	output [0:0] preproc_go,
	input [0:0] preproc_busy,
	input [0:0] preproc_done
`endif
    );
    
    vio_0 vio_0 (
    	.clk(clk),
    	.probe_in0(fsm_busy),
    	.probe_in1(fsm_done),
    	.probe_in2(report_busy),
    	.probe_in3(report_done),
    	.probe_in4(report_jet_valid),
    	.probe_in5(report_roi_num),
    	.probe_in6(report_cluster_num),
    	.probe_in7(report_jet.error_bits),
    	.probe_in8(report_jet.ptt_x),
    	.probe_in9(report_jet.ptt_y),
    	.probe_in10(report_jet.ptt_z),
    	.probe_in11(report_jet.phi),
    	.probe_in12(report_jet.eta),
`ifdef LOAD_BC_PARTS
    	.probe_in13(bc_io_stuff_ack),
`endif
`ifdef FULL_BC
		.probe_in13(preproc_busy),
		.probe_in14(preproc_done),
`endif
    	
    	.probe_out0(reset),
    	.probe_out1(num_wait_states),
    	.probe_out2(new_bunch_crossing),
    	.probe_out3(end_bunch_crossing),
    	.probe_out4(fsm_go),
    	.probe_out5(report_go),
`ifdef LOAD_BC_PARTS
    	.probe_out6(bc_io_stuff),
    	.probe_out7(write_bc_io_stuff),
    	.probe_out8(io_part_num)
`endif
`ifdef FULL_BC
		.probe_out6(cluster_in.error_bits),
		.probe_out7(cluster_in.ptt_x),
		.probe_out8(cluster_in.ptt_y),
		.probe_out9(cluster_in.ptt_z),
		.probe_out10(cluster_in.phi),
		.probe_out11(cluster_in.eta),
		.probe_out12(cluster_in_valid),
		.probe_out13(roi_indx),
		.probe_out14(center.ptt),
		.probe_out15(center.phi),
		.probe_out16(center.eta),
		.probe_out17(center_valid),
		.probe_out18(preproc_go)
`endif
    );
    
endmodule
