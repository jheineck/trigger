`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12/05/2023 02:44:07 PM
// Design Name: 
// Module Name: anti_kt
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "defines.vh"
module anti_kt(
	input clk,
	input reset,
`ifdef FULL_BC
	input cluster_type cluster_in,
	input cluster_in_valid,
	input [($clog2(NUM_ROIS))-1:0] roi_indx,
	input gblock_type center,
	input center_valid,
//	input preproc_go,
//	output reg preproc_busy,
//	output reg preproc_rdy,
//	output reg preproc_done,
`endif
	input [WAIT_STATES_NUM_BITS-1:0] num_wait_states,
    input new_bunch_crossing,
    input end_bunch_crossing,
	input fsm_go,
	output reg fsm_busy,
	output reg fsm_done,
	output logic [($clog2(NUM_ROIS))-1:0] report_roi_num,
	output logic [($clog2(MAX_CLUSTERS))-1:0] report_cluster_num,
	output cluster_type report_jet,
	output reg report_jet_valid,
	output logic report_is_a_jet,
	output logic [($clog2(MAX_CLUSTERS)):0] report_merge_count
//	input report_go,
//	output reg report_busy,
//	output reg report_done
    );
//localparam unsigned FULL_WRITES = BC_SIZE/32;
//localparam unsigned LAST_WRITE = BC_SIZE%32;
    
	logic [NUM_BUCKETS-1:0] load_enable, bucket_enable, bucket_enable_stretch;
	logic feedback_sel;
	logic [($clog2(NUM_ROIS))-1:0] roi_num;
	logic [($clog2(NUM_ROIS))-1:0] preproc_roi_num;
	logic post_it;
	
//	bc_type bc_in;
	roi_type roi_in;
	dr2matrix_type dr2matrix_in;
	roi_type roi_out;

	// i think these are no longer necesssary with no jet_fifo
//	cluster_type bucket_clusters [(NUM_BUCKETS*STAGES_PER_BUCKET)-1:0];
//	cluster_num_type bucket_cluster_nums [(NUM_BUCKETS*STAGES_PER_BUCKET)-1:0];
//	logic [(NUM_BUCKETS*STAGES_PER_BUCKET)-1:0] bucket_jets_valid;
//	reg [($clog2(MAX_CLUSTERS))-1:0] bucket_capture_num [NUM_BUCKETS-1:0];
`ifdef REPORT_PROGRESS_2
	logic [NUM_BUCKETS-1:0] bucket_capture;
`endif
	// TODO: identify [4] highest local maxima to select as ROI centers
	// TODO: sort through all of cluster_mem to assign clusters to ROIs
	// TODO: run assigned clusters through chained roi_serial_sort to pick hightest [32]
	// TODO: fill out dr2 matrix and pt2inv
	// TODO: do all this in less than 500ns.  Yeah, right.
	
`ifdef FULL_BC
	logic [NUM_ROIS-1:0] phi_must_adjust;
	logic [NUM_ROIS-1:0] roi_select;
	roi_cluster_type [NUM_ROIS-1:0] bc_in_clusters;
	assign_cluster_to_roi acr (
		.clk(clk),
		.reset(reset),
		.cluster_in(cluster_in),
		.cluster_in_valid(cluster_in_valid),
		.new_bunch_crossing(new_bunch_crossing),
		.roi_indx(roi_indx),
		.center(center),
		.center_valid(center_valid),
		.phi_must_adjust(phi_must_adjust),
		.roi_select(roi_select)
	);
	integer i;
	logic [($clog2(MAX_CLUSTERS))-1:0] cluster_num [NUM_ROIS-1:0];
	always @(posedge clk or posedge reset) begin
		if(reset) begin
			bc_in_clusters <= 0;
			for(i=0;i<NUM_ROIS;i++) begin
				cluster_num[i] <= 0;
			end
		end else if(new_bunch_crossing) begin
			bc_in_clusters <= 0;
			for(i=0;i<NUM_ROIS;i++) begin
				cluster_num[i] <= 0;
			end
		end else begin
			if(cluster_in_valid) begin
				for(i=0;i<NUM_ROIS;i++) begin
					if(roi_select[i]) begin
						// PRE-SORTED
						cluster_num[i] <= cluster_num[i] + 1;
`ifndef SORT_CLUSTERS
						bc_in_clusters[i].cluster[cluster_num[i]] <= cluster_in;
`endif
						// NOT PRE_SORTED
						// send cluster to another serial sort, NUM_ROIS * MAX_CLUSTERS
					end
				end
			end
		end
	end
	
//	logic [($clog2(NUM_ROIS))-1:0] f_roi_num;
//	logic [($clog2(MAX_CLUSTERS))-1:0] row, col;
	
	logic sel;
	logic ce;
	dr2_type dr2_P [MAX_CLUSTERS-1:0][MAX_CLUSTERS-1:0];
//	pt2_type pt2_P [MAX_CLUSTERS-1:0];
	pt2inv_type pt2inv_P [MAX_CLUSTERS-1:0];
	genvar ROW, COL;
	for(ROW=0;ROW<MAX_CLUSTERS;ROW++) begin : dr2_row
		for(COL=0;COL<MAX_CLUSTERS;COL++) begin : dr2_col
			if(ROW == COL) begin
//				assign dr2_P[ROW][COL].deltar2 = R2PAR;
//				assign bc_in.roi[roi_num].roi_dr2.dr2_row[ROW].dr2[COL].deltar2 = R2PAR;
//				assign roi_in.roi_dr2.dr2_row[ROW].dr2[COL].deltar2 = R2PAR;
				assign dr2matrix_in.dr2_row[ROW].dr2[COL].deltar2 = R2PAR;
			end else if(COL > ROW) begin
//				assign dr2_P[COL][ROW].deltar2 = dr2_P[ROW][COL].deltar2;
//				assign bc_in.roi[roi_num].roi_dr2.dr2_row[COL].dr2[ROW].deltar2 = 
//						bc_in.roi[roi_num].roi_dr2.dr2_row[ROW].dr2[COL].deltar2;
//				assign roi_in.roi_dr2.dr2_row[ROW].dr2[COL].deltar2 =
//						roi_in.roi_dr2.dr2_row[COL].dr2[ROW].deltar2;
				assign dr2matrix_in.dr2_row[ROW].dr2[COL].deltar2 =
						dr2matrix_in.dr2_row[COL].dr2[ROW].deltar2;
			end else begin
				logic [57:0] dr2_PP;
				logic [PHI_NUM_BITS-1:0] D1, A1;
				assign A1 = ((phi_must_adjust[preproc_roi_num] == 1'b1) ?
							bc_in_clusters[preproc_roi_num].cluster[ROW].phi + PHI_ADJUST_AMOUNT :
							bc_in_clusters[preproc_roi_num].cluster[ROW].phi) % ROI_PHI_SIZE;
				assign D1 = ((phi_must_adjust[preproc_roi_num] == 1'b1) ?
							bc_in_clusters[preproc_roi_num].cluster[COL].phi + PHI_ADJUST_AMOUNT :
							bc_in_clusters[preproc_roi_num].cluster[COL].phi) % ROI_PHI_SIZE;
				DR2_MAC dr2_mac (
					.CLK(clk),
					.CE(ce),
					.SCLR(reset),
					.SEL(sel),
					.A((sel) ? ({1'b0,A1}) :
								(bc_in_clusters[preproc_roi_num].cluster[ROW].eta) ),
					.D((sel) ? ({1'b0,D1}) :
								(bc_in_clusters[preproc_roi_num].cluster[COL].eta) ),
					.P(dr2_PP)
				);
//				assign dr2_P[ROW][COL].deltar2 = dr2_PP[DR2_NUM_BITS-1:0];
//				assign bc_in.roi[roi_num].roi_dr2.dr2_row[ROW].dr2[COL].deltar2 = dr2_PP[DR2_NUM_BITS-1:0];
//				assign roi_in.roi_dr2.dr2_row[ROW].dr2[COL].deltar2 = dr2_PP[DR2_NUM_BITS-1:0];
				assign dr2matrix_in.dr2_row[ROW].dr2[COL].deltar2 = dr2_PP[DR2_NUM_BITS-1:0];
			end
		end
	end
	for(ROW=0;ROW<MAX_CLUSTERS;ROW++) begin : pt2_row
		logic [ENERGY_NUM_BITS-1:0] jet_M_ptt_x, jet_M_ptt_y;
		pt2inv_type new_pt2inv;
	// note that these are taken from the accumulated ptt in the OUTPUT roi
	// which are computed below
		assign jet_M_ptt_x = bc_in_clusters[preproc_roi_num].cluster[ROW].ptt_x;
		assign jet_M_ptt_y = bc_in_clusters[preproc_roi_num].cluster[ROW].ptt_y;
		logic [57:0] pt2_PP;
		PT2_MAC pt2_mac (
			.CLK(clk),
			.CE(ce),
			.SCLR(reset),
			.SEL(sel),
			.D((sel) ? jet_M_ptt_x : jet_M_ptt_y),
			.P(pt2_PP)
		);
//		assign pt2_P[ROW] = pt2_PP[PT2_NUM_BITS-1:0];
		assign pt2inv_P[ROW] = ~(pt2_PP[PT2INV_NUM_BITS-1:0]);
//		assign bc_in.roi[roi_num].roi_dr2.dr2_row[ROW].pt2inv = ~(pt2_PP[PT2INV_NUM_BITS-1:0]);
		assign roi_in.roi_dr2.dr2_row[ROW].pt2inv.pt2inv = ~(pt2_PP[PT2INV_NUM_BITS-1:0]);
		assign roi_in.roi_dr2.dr2_row[ROW].merge_count = 1;
		
		assign roi_in.clusters.cluster[ROW] = bc_in_clusters[preproc_roi_num].cluster[ROW];
		
	end
	assign roi_in.disabled = 0;
	assign roi_in.is_a_jet = 0;
	assign roi_in.roi_num = preproc_roi_num;
	assign roi_in.iteration = 0;

`endif // FULL_BC


`ifdef REPORT_PROGRESS_2
	logic capture;
`endif
	anti_kt_block akb (
		.clk(clk),
		.reset(reset),
`ifdef REPORT_PROGRESS_2
		.bucket_capture(bucket_capture),
`endif
        .new_bunch_crossing(new_bunch_crossing),
        .end_bunch_crossing(end_bunch_crossing),
		.load_enable(load_enable),
		.bucket_enable(bucket_enable|bucket_enable_stretch),
		.feedback_sel(feedback_sel),
		.roi_block_in(roi_in), // (bc_in.roi[roi_num]),
		.dr2matrix_in(dr2matrix_in),
		.roi_block_out(roi_out)
	// I don't think these three are used anymore
//		.bucket_clusters(bucket_clusters),
//		.bucket_cluster_nums(bucket_cluster_nums),
//		.bucket_jets_valid(bucket_jets_valid)
	);
`ifdef REPORT_PROGRESS_3
	integer rrrow, cccol;
	always @(posedge clk) begin
		if(load_enable[0] & feedback_sel) begin
			for(rrrow=0;rrrow<MAX_CLUSTERS;rrrow++) begin
				$write("row %3d:  ",rrrow);
				for(cccol=0;cccol<MAX_CLUSTERS;cccol++) begin
//					$write(" %3d",roi_in.roi_dr2.dr2_row[rrrow].dr2[cccol].deltar2);
					$write(" %3d",dr2matrix_in.dr2_row[rrrow].dr2[cccol].deltar2);
				end
				$write("\n");
			end
		end
	end
`endif
	logic [2:0] fsm_state;
	logic [1:0] post_state;
	logic [($clog2(ITERATION_LOOPS))-1:0] loop_counter;
	logic [5:0] wait_counter; // extra bit for rollover/rollunder protection

	always @(posedge clk or posedge reset) begin
		if(reset) begin
			bucket_enable_stretch <= 0;
		end else begin
			bucket_enable_stretch <= bucket_enable;
		end
	end
`ifdef REPORT_PROGRESS_2
	assign bucket_capture = {NUM_BUCKETS{capture}} & bucket_enable_stretch;
`endif
	
	localparam FSM_IDLE = 3'd0,
				FSM_PREPROC0 = 3'd1,
				FSM_ROI0 = 3'd2,
				FSM_ROI123 = 3'd3,
				FSM_FINISH = 3'd4,
				FSM_WAIT = 3'd5,
				FSM_WAIT1 = 3'd6;
	
	// This FSM is NOT sufficient.  Hard coded for two buckets.
	// This affects load_enable and bucket_enable.
	// for two buckets, they go as 00->01->11>11>11>10->00
	// for four buckets, they go as 0000->0001->0011->0111->1111->1110->1100->1000->0000
	// I suppose three would be 000->001->011->111->111->110->100->000
	// probably won't work for one bucket, but it would be 0->1->0
	// works for 4 ROIs, probably ok for 3 or more.  Probably not for 2.
	// forget about 1 ROI.
// JET_FIFO	reg jet_fifo_clear;
	always @(posedge clk or posedge reset) begin
		if(reset) begin
			load_enable <= 0;
			roi_num <= 0;
			preproc_roi_num <= 0;
			feedback_sel <= 0;
			bucket_enable <= 0;
			fsm_done <= 0;
			fsm_busy <= 0;
			loop_counter <= 0;
			wait_counter <= 0;
			ce <= 0;
			sel <= 0;
`ifdef REPORT_PROGRESS_2
			capture <= 0;
`endif
// JET_FIFO			jet_fifo_clear <= 1;
			post_it <= 0;
			fsm_state <= FSM_IDLE;
		end else case (fsm_state)
			FSM_IDLE: begin
				roi_num <= 0;
				preproc_roi_num <= 0;
				fsm_done <= 0;
`ifdef REPORT_PROGRESS_2
				capture <= 0;
`endif
				post_it <= 0;
				if(fsm_go) begin
					fsm_busy <= 1;
					fsm_state <= FSM_PREPROC0;
					ce <= 1;
					wait_counter <= MAC_WAIT_STATES;	// 6 clocks for MAC results
				end else begin
					fsm_busy <= 0;
					load_enable <= 2'b00;
					bucket_enable <= 2'b00;
					feedback_sel <= 0;
					loop_counter <= 0;
					wait_counter <= 0;
				end
			end
			FSM_PREPROC0: begin
				if(wait_counter == 0) begin
					ce <= 0;
					fsm_state <= FSM_ROI0;
// JET_FIFO					jet_fifo_clear <= 1;
					load_enable <= 2'b01;
					bucket_enable <= 2'b01;
					feedback_sel <= 1; // load new data
					loop_counter <= 0;
					wait_counter <= num_wait_states;
				end else begin
					wait_counter <= wait_counter-1;
					if(wait_counter == MAC_WAIT_STATES) begin
						sel <= 1;
					end else if(wait_counter == MAC_WAIT_STATES-1) begin
						sel <= 0;
					end
				end
						
			end
			FSM_ROI0: begin
// JET_FIFO				jet_fifo_clear <= 0;
				if(wait_counter == 0) begin
					if(loop_counter == ITERATION_LOOPS-1) begin
						roi_num <= roi_num + 1;
						wait_counter <= num_wait_states;
						fsm_state <= FSM_ROI123;
						loop_counter <= 0;
						feedback_sel <= 1;
						load_enable <= 2'b11;
						bucket_enable <= 2'b11;
`ifdef REPORT_PROGRESS_2
						capture <= 1;
`endif
					end else begin
						feedback_sel <= 0; // feedback to do next iteration
						loop_counter <= loop_counter + 1;
						load_enable <= 2'b01;
						wait_counter <= num_wait_states;
`ifdef REPORT_PROGRESS_2
						capture <= 1;
`endif
					end
				end else begin
					load_enable <= 2'b00;
					wait_counter <= wait_counter - 1;
`ifdef REPORT_PROGRESS_2
					capture <= 0;
`endif
					feedback_sel <= 0;
					if(loop_counter == ITERATION_LOOPS-1) begin
						if(wait_counter == 7) begin
							preproc_roi_num <= preproc_roi_num + 1;
						end else if(wait_counter == 6) begin
							ce <= 1;
						end else if(wait_counter == 5) begin
							sel <= 1;
						end else if(wait_counter == 4) begin
							sel <= 0;
						end else if(wait_counter == 1) begin
							ce <= 0;
						end
					end
				end
			end
			FSM_ROI123: begin
				if(wait_counter == 0) begin
					if(loop_counter == ITERATION_LOOPS-1) begin
						loop_counter <= 0;
						roi_num <= roi_num + 1;
						feedback_sel <= 1;
						wait_counter <= num_wait_states;
`ifdef REPORT_PROGRESS_2
						capture <= 1;
`endif
						post_it <= 1;
						if(roi_num == NUM_ROIS-1) begin
							load_enable <= 2'b10;
							bucket_enable <= 2'b10;
							fsm_state <= FSM_FINISH;
						end else begin
							load_enable <= 2'b11;
						end
					end else begin
						feedback_sel <= 0;
						loop_counter <= loop_counter + 1;
						load_enable <= 2'b11;
						wait_counter <= num_wait_states;
`ifdef REPORT_PROGRESS_2
						capture <= 1;
`endif
						post_it <= 0;
					end
				end else begin
					load_enable <= 2'b00;
					wait_counter <= wait_counter - 1;
`ifdef REPORT_PROGRESS_2
					capture <= 0;
`endif
					feedback_sel <= 0;
					post_it <= 0;
					if(loop_counter == ITERATION_LOOPS-1) begin
						if(wait_counter == 7) begin
							preproc_roi_num <= preproc_roi_num + 1;
						end else if(wait_counter == 6) begin
							ce <= 1;
						end else if(wait_counter == 5) begin
							sel <= 1;
						end else if(wait_counter == 4) begin
							sel <= 0;
						end else if(wait_counter == 1) begin
							ce <= 0;
						end
					end
				end
			end
			FSM_FINISH: begin
				if(wait_counter == 0) begin
					if(loop_counter == ITERATION_LOOPS-1) begin
						loop_counter <= 0;
						feedback_sel <= 0;
// not yet						fsm_done <= 1;
// not yet						fsm_busy <= 0;
// not yet						fsm_state <= FSM_IDLE;
						fsm_state <= FSM_WAIT;
`ifdef REPORT_PROGRESS_2
						capture <= 1; // one last time
`endif
						post_it <= 1;
					end else begin
						load_enable <= 2'b10;
						feedback_sel <= 0;
						loop_counter <= loop_counter + 1;
						wait_counter <= num_wait_states;
`ifdef REPORT_PROGRESS_2
						capture <= 1;
`endif
						post_it <= 0;
					end
				end else begin
					load_enable <= 2'b00;
					post_it <= 0;
					wait_counter <= wait_counter - 1;
`ifdef REPORT_PROGRESS_2
					capture <= 0;
`endif
					feedback_sel <= 0;
				end
			end
			FSM_WAIT: begin
				post_it <= 0;
				fsm_state <= FSM_WAIT1; // FSM_IDLE;
			end
			FSM_WAIT1: begin
				if(post_state == POST_IDLE) begin
					fsm_busy <= 0;
					fsm_done <= 1;
					fsm_state <= FSM_IDLE;
				end
			end
					
		endcase
	end
	
	function [($clog2(MAX_CLUSTERS))-1:0] mylog2 (input [MAX_CLUSTERS-1:0] arg);
		integer i;
		reg [MAX_CLUSTERS-1:0] j;
		begin
			j = arg - 1;
			mylog2 = 0;
			for(i=0;i<MAX_CLUSTERS;i++) begin
				if(j[i]) mylog2 = i+1;
			end
		end
	endfunction

	roi_type roi_out_reg;
	logic [MAX_CLUSTERS-1:0] post_candidates;
	for(ROW=0;ROW<MAX_CLUSTERS;ROW++) begin
		assign post_candidates[ROW] = roi_out_reg.is_a_jet[ROW] | ((~roi_out_reg.disabled[ROW]) && (roi_out_reg.roi_dr2.dr2_row[ROW].merge_count > MERGE_THRESHOLD));
	end
	logic [MAX_CLUSTERS-1:0] post_candidates_r;
	logic [MAX_CLUSTERS-1:0] post_candidate;
	logic [($clog2(MAX_CLUSTERS))-1:0] post_candidate_num;
	assign post_candidate = (~post_candidates_r + 1) & (post_candidates_r);
	assign post_candidate_num = mylog2(post_candidate);
	// post results
	localparam POST_IDLE = 0,
				POST_SETUP = 1,
				POST_SCAN = 2;
//	logic [1:0] post_state;
	always @(posedge clk or posedge reset) begin
		if(reset) begin
			post_state <= POST_IDLE;
			report_roi_num <= 0;
			report_cluster_num <= 0;
			report_jet <= 0;
			report_jet_valid <= 0;
			report_merge_count <= 0;
			report_is_a_jet <= 0;
			roi_out_reg <= 0;
		end else begin
			case(post_state)
				POST_IDLE: begin
					if(post_it) begin
						roi_out_reg <= roi_out;
						post_state <= POST_SETUP;
					end else begin
						report_roi_num <= 0;
						report_cluster_num <= 0;
						report_jet <= 0;
						report_jet_valid <= 0;
						report_merge_count <= 0;
						report_is_a_jet <= 0;
					end
				end
				POST_SETUP: begin
					post_candidates_r <= post_candidates;
					post_state <= POST_SCAN;
				end
				POST_SCAN: begin
					if(post_candidates_r == 0) begin
						report_jet_valid <= 0;
						post_state <= POST_IDLE;
					end else begin
						report_jet_valid <= 1;
						report_roi_num <= roi_out_reg.roi_num;
						report_cluster_num = post_candidate_num;
						report_jet <= roi_out_reg.clusters.cluster[post_candidate_num];
						report_merge_count <= roi_out_reg.roi_dr2.dr2_row[post_candidate_num].merge_count;
						report_is_a_jet <= roi_out_reg.is_a_jet[post_candidate_num];
						post_candidates_r <= post_candidates_r ^ (1'b1 << post_candidate_num);
					end
				end
			endcase
		end
	end

endmodule

