`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12/01/2023 05:11:38 PM
// Design Name: 
// Module Name: anti_kt_block
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "defines.vh"

module anti_kt_block (
	input clk,
	input reset,
`ifdef REPORT_PROGRESS_2
	input [NUM_BUCKETS-1:0] bucket_capture,
`endif
	input [NUM_BUCKETS-1:0] load_enable,
	input [NUM_BUCKETS-1:0] bucket_enable,
	input feedback_sel,
    input new_bunch_crossing,
    input end_bunch_crossing,
	input roi_type roi_block_in,
	input dr2matrix_type dr2matrix_in,
	output roi_type roi_block_out
	
	// I don't think these three are used anymore
//	output cluster_type bucket_clusters [(NUM_BUCKETS*STAGES_PER_BUCKET)-1:0],
//	output cluster_num_type bucket_cluster_nums [(NUM_BUCKETS*STAGES_PER_BUCKET)-1:0],
//	output logic [(NUM_BUCKETS*STAGES_PER_BUCKET)-1:0] bucket_jets_valid
);

	roi_type roi_interbucket [NUM_BUCKETS:0];
	dr2matrix_type dr2_interbucket [NUM_BUCKETS:0];
	assign roi_interbucket[0] = roi_block_in;
	assign dr2_interbucket[0] = dr2matrix_in;
//    logic [($clog2(MAX_CLUSTERS))-1:0] N_to_disable [(NUM_BUCKETS*STAGES_PER_BUCKET)-1:0];
	assign roi_block_out = roi_interbucket[NUM_BUCKETS];
	genvar ROI_STAGE, ROI_BUCKET;
	for(ROI_BUCKET=0;ROI_BUCKET<NUM_BUCKETS;ROI_BUCKET++) begin
		roi_type roi_interstage [STAGES_PER_BUCKET:0];
		assign roi_interbucket[ROI_BUCKET+1] = roi_interstage[STAGES_PER_BUCKET];
//		assign dr2_interbucket[ROI_BUCKET+1] = dr2_interbucket[ROI_BUCKET];
		
		bucket_setup bs (
			.clk(clk),
			.ce(load_enable[ROI_BUCKET]),
			.reset(reset),
            .new_bunch_crossing(new_bunch_crossing),
            .end_bunch_crossing(end_bunch_crossing),
			.feedback_sel(feedback_sel),
			.roi_bucket_in(roi_interbucket[ROI_BUCKET]),
			.dr2matrix_in(dr2_interbucket[ROI_BUCKET]),
			.dr2matrix_out(dr2_interbucket[ROI_BUCKET+1]),
			.roi_bucket_out(roi_interstage[0]),
			.roi_bucket_feedback(roi_interstage[STAGES_PER_BUCKET])
		);
	
		for (ROI_STAGE=0;ROI_STAGE<STAGES_PER_BUCKET;ROI_STAGE++) begin
			cluster_type jet;
			cluster_num_type cluster_num;
			logic jet_valid;
			anti_kt_stage #(.THIS_STAGE(ROI_STAGE+(ROI_BUCKET*STAGES_PER_BUCKET))) aks (
				.clk(clk),
				.reset(reset),
`ifdef REPORT_PROGRESS_2
				.bucket_capture(bucket_capture),
`endif
				.stage_enable(bucket_enable[ROI_BUCKET]),
				.roi_stage_in(roi_interstage[ROI_STAGE]),
				.dr2matrix_in(dr2_interbucket[ROI_BUCKET+1]),
				.roi_stage_out(roi_interstage[ROI_STAGE+1]),
				.jet(jet),
				.cluster_num(cluster_num),
//				.N_to_disable(N_to_disable[ROI_STAGE+(ROI_BUCKET*STAGES_PER_BUCKET)]),
				.jet_valid(jet_valid)
			);
	// I don't think these three are used anymore
//			assign bucket_clusters[ROI_STAGE+(ROI_BUCKET*STAGES_PER_BUCKET)] = jet;
//			assign bucket_cluster_nums[(ROI_STAGE+(ROI_BUCKET*STAGES_PER_BUCKET))] = cluster_num;
//			assign bucket_jets_valid[(ROI_STAGE+(ROI_BUCKET*STAGES_PER_BUCKET))] = jet_valid;
		end
	end
	
endmodule

module bucket_setup (
	input clk,
	input ce,
	input reset,
	input feedback_sel,
    input new_bunch_crossing,
    input end_bunch_crossing,
	input roi_type roi_bucket_in,
	input dr2matrix_type dr2matrix_in,
	input roi_type roi_bucket_feedback,
	output roi_type roi_bucket_out,
	output dr2matrix_type dr2matrix_out
);
	roi_type bucket_reg_in;
	
	assign bucket_reg_in = (feedback_sel) ? roi_bucket_in : roi_bucket_feedback;
	
	bucket_reg #(.OBJECT_TYPE(roi_type)) br (
		.clk(clk),
		.reset(reset),
        .new_bunch_crossing(new_bunch_crossing),
		.ce(ce),
		.D(bucket_reg_in),
		.Q(roi_bucket_out)
	);
	
	dr2matrix_type dr2_reg_in;
	
	assign dr2_reg_in = dr2matrix_in;
	
	bucket_reg #(.OBJECT_TYPE(dr2matrix_type)) dr (
		.clk(clk),
		.reset(reset),
		.new_bunch_crossing(new_bunch_crossing),
		.ce(ce & feedback_sel),
		.D(dr2_reg_in),
		.Q(dr2matrix_out)
	);
endmodule
module bucket_reg #(
	parameter type OBJECT_TYPE = roi_type) (
	input clk,
	input reset,
    input new_bunch_crossing,
	input ce,
	input OBJECT_TYPE D,
	output OBJECT_TYPE Q
);
	always @(posedge clk or posedge reset) begin
		if(reset) begin
			Q <= 0;
		end else begin
		    if(new_bunch_crossing) begin
		        Q <= 0;
			end else if(ce) begin
				Q <= D;
			end
		end
	end
endmodule
