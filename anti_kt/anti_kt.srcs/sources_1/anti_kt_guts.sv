`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12/05/2023 02:44:07 PM
// Design Name: 
// Module Name: anti_kt_guts
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "defines.vh"
module anti_kt_guts(
	input clk,
	input reset,
`ifdef LOAD_BC_PARTS	
	input write_bc_io_stuff,
	output bc_io_stuff_ack,
	input [BC_IO_NUM_BITS-1:0] bc_io_stuff,
	input [($clog2(BC_SIZE/BC_IO_NUM_BITS))-1:0] io_part_num,
`endif // LOAD_BC_PARTS
`ifdef FULL_BC
	input cluster_type cluster_in,
	input cluster_in_valid,
	input [($clog2(NUM_ROIS))-1:0] roi_indx,
	input gblock_type center,
	input center_valid,
//	input preproc_go,
//	output preproc_busy,
//	output preproc_rdy,
//	output preproc_done,
`endif // FULL_BC
	input [WAIT_STATES_NUM_BITS-1:0] num_wait_states,
    input new_bunch_crossing,
    input end_bunch_crossing,
	input fsm_go,
	output reg fsm_busy,
	output reg fsm_done,
	output logic [($clog2(NUM_ROIS))-1:0] report_roi_num,
	output logic [($clog2(MAX_CLUSTERS))-1:0] report_cluster_num,
	output cluster_type report_jet,
	output report_jet_valid,
	output report_is_a_jet,
	output [($clog2(MAX_CLUSTERS)):0] report_merge_count
//	input report_go,
//	output report_busy,
//	output report_done
    );
`ifndef SIMULATION
    mycips_wrapper mycips_wrapper_0 ( );
`endif // SIMULATION

	logic reset_IBUF;
	logic clk_IBUF, clk_IBUF_BUFG;
`ifdef LOAD_BC_PARTS
	logic [($clog2(BC_SIZE/BC_IO_NUM_BITS))-1:0] io_part_num_IBUF;
	logic [BC_IO_NUM_BITS-1:0] bc_io_stuff_IBUF;
	logic write_bc_io_stuff_IBUF;
	logic bc_io_stuff_ack_OBUF;
`endif // LOAD_BC_PARTS
`ifdef FULL_BC
	cluster_type cluster_in_IBUF;
	logic cluster_in_valid_IBUF;
	logic [($clog2(NUM_ROIS))-1:0] roi_indx_IBUF;
	gblock_type center_IBUF;
	logic center_valid_IBUF;
//	logic preproc_go_IBUF;
//	logic preproc_busy_OBUF;
//	logic preproc_rdy_OBUF;
//	logic preproc_done_OBUF;
`endif // FULL_BC
	logic [WAIT_STATES_NUM_BITS-1:0] num_wait_states_IBUF;
//	logic report_go_IBUF;
	logic new_bunch_crossing_IBUF;
	logic end_bunch_crossing_IBUF;	
	logic fsm_go_IBUF;
	
//	logic report_busy_OBUF;
//	logic report_done_OBUF;
	logic [($clog2(NUM_ROIS))-1:0] report_roi_num_OBUF;
	logic report_jet_valid_OBUF;
	logic [($clog2(MAX_CLUSTERS))-1:0] report_cluster_num_OBUF;
	cluster_type report_jet_OBUF;
	logic report_is_a_jet_OBUF;
	logic [($clog2(MAX_CLUSTERS)):0] report_merge_count_OBUF;
	logic fsm_busy_OBUF;
	logic fsm_done_OBUF;
	
	anti_kt akt (
		.clk(clk_IBUF_BUFG),
		.reset(reset_IBUF),
`ifdef LOAD_BC_PARTS
		.bc_io_stuff(bc_io_stuff_IBUF),
		.write_bc_io_stuff(write_bc_io_stuff_IBUF),
		.bc_io_stuff_ack(bc_io_stuff_ack_OBUF),
		.io_part_num(io_part_num_IBUF),
`endif // LOAD_BC_PARTS
		.num_wait_states(num_wait_states_IBUF),
`ifdef FULL_BC
		.cluster_in(cluster_in_IBUF),
		.cluster_in_valid(cluster_in_valid_IBUF),
		.center(center_IBUF),
		.roi_indx(roi_indx_IBUF),
		.center_valid(center_valid_IBUF),
//		.preproc_go(preproc_go_IBUF),
//		.preproc_busy(preproc_busy_OBUF),
//		.preproc_rdy(preproc_rdy_OBUF),
//		.preproc_done(preproc_done_OBUF),
`endif // FULL_BC
//		.report_go(report_go_IBUF),
//		.report_busy(report_busy_OBUF),
//		.report_done(report_done_OBUF),
		.report_roi_num(report_roi_num_OBUF),
		.report_jet_valid(report_jet_valid_OBUF),
		.report_cluster_num(report_cluster_num_OBUF),
		.report_jet(report_jet_OBUF),
		.report_is_a_jet(report_is_a_jet_OBUF),
		.report_merge_count(report_merge_count_OBUF),
        .new_bunch_crossing(new_bunch_crossing_IBUF),
        .end_bunch_crossing(end_bunch_crossing_IBUF),
		.fsm_go(fsm_go_IBUF),
		.fsm_busy(fsm_busy_OBUF),
		.fsm_done(fsm_done_OBUF)
	);
	
	IBUF clk_IBUF_inst (.I(clk), .O(clk_IBUF));
	BUFGCE #(
		.CE_TYPE("ASYNC"),
		.IS_CE_INVERTED(1'b0),
		.IS_I_INVERTED(1'b0),
		.SIM_DEVICE("VERSAL_PRIME")
	) clk_IBUF_BUFG_inst (.I(clk_IBUF), .CE(1'b1), .O(clk_IBUF_BUFG));
	
`ifdef VIO
	akt_vio akt_vio_0 (
		.clk(clk_IBUF_BUFG),
		.reset(reset_IBUF),
		.num_wait_states(num_wait_states_IBUF),
		.new_bunch_crossing(new_bunch_crossing_IBUF),
		.end_bunch_crossing(end_bunch_crossing_IBUF),
		.fsm_go(fsm_go_IBUF),
		.fsm_busy(fsm_busy_OBUF),
		.fsm_done(fsm_done_OBUF),
		.report_roi_num(report_roi_num_OBUF),
		.report_cluster_num(report_cluster_num_OBUF),
		.report_jet(report_jet_OBUF),
		.report_jet_valid(report_jet_valid_OBUF),
		.report_is_a_jet(report_is_a_jet_OBUF),
		.report_merge_count(report_merge_count_OBUF),
//		.report_go(report_go_IBUF),
//		.report_busy(report_busy_OBUF),
//		.report_done(report_done_OBUF),
		
`ifdef LOAD_BC_PARTS
		.write_bc_io_stuff(write_bc_io_stuff_IBUF
		.bc_io_stuff_ack(bc_io_stuff_ack_OBUF),
		.bc_io_stuff(bc_io_stuff_IBUF),
		.io_part_num(io_part_num_IBUF)
`endif // LOAD_BC_PARTS
`ifdef FULL_BC
		.cluster_in(cluster_in_IBUF),
		.cluster_in_valid(cluster_in_valid_IBUF),
		.roi_indx(roi_indx_IBUF),
		.center(center_IBUF),
		.center_valid(center_valid_IBUF)
//		.preproc_go(preproc_go_IBUF),
//		.preproc_busy(preproc_busy_OBUF),
//		.preproc_rdy(preproc_rdy_OBUF),
//		.preproc_done(preproc_done_OBUF)
`endif // FULL_BC
	);
`else // VIO
	genvar I;
`ifdef LOAD_BC_PARTS
	OBUF bc_io_stuff_ack_OBUF_inst (.I(bc_io_stuff_ack_OBUF), .O(bc_io_stuff_ack));
`endif // LOAD_BC_PARTS
	OBUF fsm_busy_OBUF_inst (.I(fsm_busy_OBUF), .O(fsm_busy));
	OBUF fsm_done_OBUF_inst (.I(fsm_done_OBUF), .O(fsm_done));
//	OBUF report_busy_OBUF_inst (.I(report_busy_OBUF), .O(report_busy));
//	OBUF report_done_OBUF_inst (.I(report_done_OBUF), .O(report_done));
	OBUF report_jet_valid_OBUF_inst (.I(report_jet_valid_OBUF), .O(report_jet_valid));
	OBUF report_is_a_jet_OBUF_inst (.I(report_is_a_jet_OBUF), .O(report_is_a_jet));
	
	for(I=0;I<($clog2(NUM_ROIS));I++) begin
		OBUF \report_roi_num_OBUF[I]_inst (.I(report_roi_num_OBUF[I]), .O(report_roi_num[I]));
	end
	
	for(I=0;I<($clog2(MAX_CLUSTERS));I++) begin
		OBUF \report_cluster_num_OBUF[I]_inst (.I(report_cluster_num_OBUF[I]), .O(report_cluster_num[I]));
	end
	for(I=0;I<=($clog2(MAX_CLUSTERS));I++) begin
		OBUF \report_merge_count_OBUF[I]_inst (.I(report_merge_count_OBUF[I]), .O(report_merge_count[I]));
	end
	
`ifdef SIMULATION
	for(I=0;I<CLUSTER_SIZE;I++) begin
		OBUF \report_jet_OBUF[I]_inst (.I(report_jet_OBUF[I]), .O(report_jet[I]));
	end
`else // SIMULATION
	logic [ERROR_NUM_BITS-1:0] \report_jet[error_bits]_OBUF ;
	logic [ERROR_NUM_BITS-1:0] \report_jet[error_bits] ;
	for(I=0;I<ERROR_NUM_BITS;I++) begin
		OBUF \report_jet[error_bits]_OBUF[I]_inst (.I(\report_jet[error_bits]_OBUF [I]), .O(\report_jet[error_bits] [I]));
		assign \report_jet[error_bits]_OBUF [I] = report_jet_OBUF.error_bits[I];
		assign report_jet.error_bits[I] = \report_jet[error_bits] [I];
	end
	
	logic [ENERGY_NUM_BITS-1:0] \report_jet[ptt_x]_OBUF ;
	logic [ENERGY_NUM_BITS-1:0] \report_jet[ptt_x] ;
	for(I=0;I<ENERGY_NUM_BITS;I++) begin
		OBUF \report_jet[ptt_x]_OBUF[I]_inst (.I(\report_jet[ptt_x]_OBUF [I]), .O(\report_jet[ptt_x] [I]));
		assign \report_jet[ptt_x]_OBUF [I] = report_jet_OBUF.ptt_x[I];
		assign report_jet.ptt_x[I] = \report_jet[ptt_x] [I];
	end
	
	logic [ENERGY_NUM_BITS-1:0] \report_jet[ptt_y]_OBUF ;
	logic [ENERGY_NUM_BITS-1:0] \report_jet[ptt_y] ;
	for(I=0;I<ENERGY_NUM_BITS;I++) begin
		OBUF \report_jet[ptt_y]_OBUF[I]_inst (.I(\report_jet[ptt_y]_OBUF [I]), .O(\report_jet[ptt_y] [I]));
		assign \report_jet[ptt_y]_OBUF [I] = report_jet_OBUF.ptt_y [I];
		assign report_jet.ptt_y[I] = \report_jet[ptt_y] [I];
	end
	
	logic [ENERGY_NUM_BITS-1:0] \report_jet[ptt_z]_OBUF ;
	logic [ENERGY_NUM_BITS-1:0] \report_jet[ptt_z] ;
	for(I=0;I<ENERGY_NUM_BITS;I++) begin
		OBUF \report_jet[ptt_z]_OBUF[I]_inst (.I(\report_jet[ptt_z]_OBUF [I]), .O(\report_jet[ptt_z] [I]));
		assign \report_jet[ptt_z]_OBUF [I] = report_jet_OBUF.ptt_z [I];
		assign report_jet.ptt_z[I] = \report_jet[ptt_z] [I];
	end
	
	logic [PHI_NUM_BITS-1:0] \report_jet[phi]_OBUF ;
	logic [PHI_NUM_BITS-1:0] \report_jet[phi] ;
	for(I=0;I<PHI_NUM_BITS;I++) begin
		OBUF \report_jet[phi]_OBUF[I]_inst (.I(\report_jet[phi]_OBUF [I]), .O(\report_jet[phi] [I]));
		assign \report_jet[phi]_OBUF [I] = report_jet_OBUF.phi [I];
		assign report_jet.phi[I] = \report_jet[phi] [I];
	end
	
	logic [ETA_NUM_BITS-1:0] \report_jet[eta]_OBUF ;
	logic [ETA_NUM_BITS-1:0] \report_jet[eta] ;
	for(I=0;I<ETA_NUM_BITS;I++) begin
		OBUF \report_jet[eta]_OBUF[I]_inst (.I(\report_jet[eta]_OBUF [I]), .O(\report_jet[eta] [I]));
		assign \report_jet[eta]_OBUF [I] = report_jet_OBUF.eta [I];
		assign report_jet.eta[I] = \report_jet[eta] [I];
	end
`endif // SIMULATION

	IBUF reset_IBUF_inst (.I(reset), .O(reset_IBUF));
`ifdef LOAD_BC_PARTS
	IBUF write_bc_io_stuff_IBUF_inst (.I(write_bc_io_stuff), .O(write_bc_io_stuff_IBUF));
	for(I=0;I<BC_IO_NUM_BITS;I++) begin
		IBUF \bc_io_stuff_IBUF[I]_inst (.I(bc_io_stuff[I]), .O(bc_io_stuff_IBUF[I]));
	end

	for(I=0;I<($clog2(BC_SIZE/BC_IO_NUM_BITS));I++) begin
		IBUF \io_part_num_IBUF[I]_inst (.I(io_part_num[I]), .O(io_part_num_IBUF[I]));
	end
`endif // LOAD_BC_PARTS
`ifdef FULL_BC
	IBUF cluster_in_valid_IBUF_inst (.I(cluster_in_valid), .O(cluster_in_valid_IBUF));
	
`ifdef SIMULATION
	for(I=0;I<CLUSTER_SIZE;I++) begin
		IBUF \cluster_in_IBUF[I]_inst (.I(cluster_in[I]), .O(cluster_in_IBUF[I]));
	end
	for(I=0;I<($bits(gblock_type));I++) begin
		IBUF \center_IBUF[I]_inst (.I(center[I]), .O(center_IBUF[I]));
	end
`else // SIMULATION
	logic [ERROR_NUM_BITS-1:0] \cluster_in[error_bits]_IBUF ;
	logic [ERROR_NUM_BITS-1:0] \cluster_in[error_bits] ;
	for(I=0;I<ERROR_NUM_BITS;I++) begin
		IBUF \cluster_in[error_bits]_IBUF[I]_inst (.I(\cluster_in[error_bits] [I]), .O(\cluster_in[error_bits]_IBUF [I]));
		assign cluster_in_IBUF.error_bits [I] = \cluster_in[error_bits]_IBUF [I];
		assign \cluster_in[error_bits] [I] = cluster_in.error_bits[I];
	end
	
	logic [ENERGY_NUM_BITS-1:0] \cluster_in[ptt_x]_IBUF ;
	logic [ENERGY_NUM_BITS-1:0] \cluster_in[ptt_x] ;
	for(I=0;I<ENERGY_NUM_BITS;I++) begin
		IBUF \cluster_in[ptt_x]_IBUF[I]_inst (.I(\cluster_in[ptt_x] [I]), .O(\cluster_in[ptt_x]_IBUF [I]));
		assign cluster_in_IBUF.ptt_x [I] = \cluster_in[ptt_x]_IBUF [I];
		assign \cluster_in[ptt_x] [I] = cluster_in.ptt_x[I];
	end
	
	logic [ENERGY_NUM_BITS-1:0] \cluster_in[ptt_y]_IBUF ;
	logic [ENERGY_NUM_BITS-1:0] \cluster_in[ptt_y] ;
	for(I=0;I<ENERGY_NUM_BITS;I++) begin
		IBUF \cluster_in[ptt_y]_IBUF[I]_inst (.I(\cluster_in[ptt_y] [I]), .O(\cluster_in[ptt_y]_IBUF [I]));
		assign cluster_in_IBUF.ptt_y [I] = \cluster_in[ptt_y]_IBUF [I];
		assign \cluster_in[ptt_y] [I] = cluster_in.ptt_y[I];
	end
	
	logic [ENERGY_NUM_BITS-1:0] \cluster_in[ptt_z]_IBUF ;
	logic [ENERGY_NUM_BITS-1:0] \cluster_in[ptt_z] ;
	for(I=0;I<ENERGY_NUM_BITS;I++) begin
		IBUF \cluster_in[ptt_z]_IBUF[I]_inst (.I(\cluster_in[ptt_z] [I]), .O(\cluster_in[ptt_z]_IBUF [I]));
		assign cluster_in_IBUF.ptt_z [I] = \cluster_in[ptt_z]_IBUF [I];
		assign \cluster_in[ptt_z] [I] = cluster_in.ptt_z[I];
	end
	
	logic [PHI_NUM_BITS-1:0] \cluster_in[phi]_IBUF ;
	logic [PHI_NUM_BITS-1:0] \cluster_in[phi] ;
	for(I=0;I<PHI_NUM_BITS;I++) begin
		IBUF \cluster_in[phi]_IBUF[I]_inst (.I(\cluster_in[phi] [I]), .O(\cluster_in[phi]_IBUF [I]));
		assign cluster_in_IBUF.phi [I] = \cluster_in[phi]_IBUF [I];
		assign \cluster_in[phi] [I] = cluster_in.phi[I];
	end
	
	logic [ETA_NUM_BITS-1:0] \cluster_in[eta]_IBUF ;
	logic [ETA_NUM_BITS-1:0] \cluster_in[eta] ;
	for(I=0;I<ETA_NUM_BITS;I++) begin
		IBUF \cluster_in[eta]_IBUF[I]_inst (.I(\cluster_in[eta] [I]), .O(\cluster_in[eta]_IBUF [I]));
		assign cluster_in_IBUF.eta [I] = \cluster_in[eta]_IBUF [I];
		assign \cluster_in[eta] [I] = cluster_in.eta[I];
	end
	
	logic [ENERGY_NUM_BITS-1:0] \center[ptt]_IBUF ;
	logic [ENERGY_NUM_BITS-1:0] \center[ptt] ;
	for(I=0;I<ENERGY_NUM_BITS;I++) begin
		IBUF \center[ptt]_IBUF[I]_inst (.I(\center[ptt] [I]), .O(\center[ptt]_IBUF [I]));
		assign center_IBUF.ptt [I] = \center[ptt]_IBUF [I];
		assign \center[ptt] [I] = center.ptt[I];
	end
	
	logic [PHI_NUM_BITS-1:0] \center[phi]_IBUF ;
	logic [PHI_NUM_BITS-1:0] \center[phi] ;
	for(I=0;I<PHI_NUM_BITS;I++) begin
		IBUF \center[phi]_IBUF[I]_inst (.I(\center[phi] [I]), .O(\center[phi]_IBUF [I]));
		assign center_IBUF.phi [I] = \center[phi]_IBUF [I];
		assign \center[phi] [I] = center.phi[I];
	end
	
	logic [ETA_NUM_BITS-1:0] \center[eta]_IBUF ;
	logic [ETA_NUM_BITS-1:0] \center[eta] ;
	for(I=0;I<ETA_NUM_BITS;I++) begin
		IBUF \center[eta]_IBUF[I]_inst (.I(\center[eta] [I]), .O(\center[eta]_IBUF [I]));
		assign center_IBUF.eta [I] = \center[eta]_IBUF [I];
		assign \center[eta] [I] = center.eta[I];
	end
	
`endif // SIMULATION
	for(I=0;I<2;I++) begin
		IBUF \roi_indx_IBUF[I]_inst (.I(roi_indx[I]), .O(roi_indx_IBUF[I]));
	end
	IBUF center_valid_IBUF_inst (.I(center_valid), .O(center_valid_IBUF));
//	IBUF preproc_go_IBUF_inst (.I(preproc_go), .O(preproc_go_IBUF));
//	OBUF preproc_busy_OBUF_inst (.I(preproc_busy_OBUF), .O(preproc_busy));
//	OBUF preproc_rdy_OBUF_inst (.I(preproc_rdy_OBUF), .O(preproc_rdy));
//	OBUF preproc_done_OBUF_inst (.I(preproc_done_OBUF), .O(preproc_done));
	
`endif // FULL_BC
	
	for(I=0;I<WAIT_STATES_NUM_BITS;I++) begin
		IBUF \num_wait_states_IBUF[I]_inst (.I(num_wait_states[I]), .O(num_wait_states_IBUF[I]));
	end

//	IBUF report_go_IBUF_inst (.I(report_go), .O(report_go_IBUF));
	IBUF new_bunch_crossing_IBUF_inst (.I(new_bunch_crossing), .O(new_bunch_crossing_IBUF));
	IBUF end_bunch_crossing_IBUF_inst (.I(end_bunch_crossing), .O(end_bunch_crossing_IBUF));
	IBUF fsm_go_IBUF_inst (.I(fsm_go), .O(fsm_go_IBUF));
`endif // VIO
endmodule
