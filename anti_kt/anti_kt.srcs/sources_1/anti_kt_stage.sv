`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/27/2023 01:27:44 PM
// Design Name: 
// Module Name: anti_kt_stage
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "defines.vh"

module anti_kt_stage #(
	parameter THIS_STAGE = 0) (
	input clk,
	input reset,
	
	input stage_enable,
`ifdef REPORT_PROGRESS_2
	input [NUM_BUCKETS-1:0] bucket_capture,
`endif
	input roi_type roi_stage_in,
	input dr2matrix_type dr2matrix_in,
	output roi_type roi_stage_out,
    output cluster_type jet,
    output logic [($clog2(MAX_CLUSTERS))-1:0] cluster_num,
    output logic jet_valid
    );
    logic [($clog2(MAX_CLUSTERS))-1:0] N_to_disable;
    roi_type roi;
    dr2_up_type [MAX_CLUSTERS-1:0] minimum_column;
    dr2_pt2inv_up_type minimum_row;
    logic [($clog2(MAX_CLUSTERS))-1:0] column_num [MAX_CLUSTERS-1:0];
    dr2_column_type sep_list_minima;
    logic [($clog2(MAX_CLUSTERS))-1:0] row_num;
    logic [($clog2(MAX_CLUSTERS))-1:0] M_to_give_N_to ;
    logic N_is_a_jet;
    pt2inv_type pt2inv [MAX_CLUSTERS-1:0];
    dr2_pt2inv_type dr2 [MAX_CLUSTERS-1:0];
//	assign roi = roi_stage_in;

    // find the minimum in each row
    genvar ROW;
    for(ROW=0;ROW<MAX_CLUSTERS;ROW++) begin
    	bitonicsort32 #(.INPUT_TYPE(dr2matrix_row_type)) bs_column (
    		.reset(reset),
    		.dr2_row(dr2matrix_in.dr2_row[ROW]),
    		.disabled(roi_stage_in.disabled),
    		.minimum(minimum_column[ROW]),
    		.cluster_num(column_num[ROW])
    	);
		// by definition, the incoming pt2inv WILL ALWAYS BE CORRECT.
		// the incoming pt2inv for this stage must be corrected in update_roi from the previous stage
		// the outgoing pt2inv from this stage must be corrected in update_roi from this stage.
		assign #10 pt2inv[ROW] = roi_stage_in.roi_dr2.dr2_row[ROW].pt2inv;
		minima_mult dr2_mult (
			.A(minimum_column[ROW].dr2.deltar2),
			.B(pt2inv[ROW]),
			.P(dr2[ROW].deltar2)
		);

		assign #10 sep_list_minima.dr2[ROW].deltar2 = dr2[ROW].deltar2;
						
    end

    // now find the minimum of that
    bitonicsort32 #(.INPUT_TYPE(dr2_column_type),
    				.OUTPUT_TYPE(dr2_pt2inv_up_type)) bs_row (
    	.reset(reset),
    	.dr2_row(sep_list_minima),
    	.disabled(roi_stage_in.disabled),
    	.minimum(minimum_row),
    	.cluster_num(row_num)
    );

    // is it a jet?
    logic is_it_a_jet;
    assign  is_it_a_jet = (row_num == column_num[row_num]);
    assign  M_to_give_N_to = row_num;
    assign  N_to_disable = column_num[row_num];
   
    assign  N_is_a_jet = is_it_a_jet;  
    
    // if there are any clusters still in play, this
    // stage will have valid outputs for each roi
    logic disable_N;
	// TODO: disable this cluster if all its pt's are zero in testbench
    assign  disable_N = !(&(roi_stage_in.disabled));
	assign  jet_valid = disable_N & N_is_a_jet & stage_enable;
	assign  jet = roi_stage_in.clusters.cluster[N_to_disable];
	assign  cluster_num = N_to_disable;
	
	update_roi ur (
		.reset(reset),
		.roi_ur_in(roi_stage_in),
		.roi_ur_out(roi_stage_out),
		.disable_N(disable_N),
		.N_to_disable(N_to_disable),
		.jet(jet),	// remember this is jet N, not M
		.merge_count(roi_stage_in.roi_dr2.dr2_row[N_to_disable].merge_count), // also provide loser's merge_count
		.M_to_give_N_to(M_to_give_N_to),
		.N_is_a_jet(N_is_a_jet)
	);
	
`ifdef REPORT_PROGRESS_2
	always @(posedge clk or posedge reset) begin
		if(reset) begin
		end else if(bucket_capture[THIS_STAGE/STAGES_PER_BUCKET] & stage_enable) begin
			if(tb_anti_kt.progress_fd != 0) begin
				$fwrite(tb_anti_kt.progress_fd,"%t: stage %0d gives %2d (%3d,%3d),(%3d,%3d,%3d,%3d) to %2d (%3d,%3d),(%3d,%3d,%3d,%3d) -> (%3d,%3d,%3d,%3d) ROI%0d.%0d\n",
					$time,THIS_STAGE,
					N_to_disable,
					jet.phi,jet.eta,jet.ptt_x,jet.ptt_y,jet.ptt_z,
					roi_stage_in.roi_dr2.dr2_row[N_to_disable].merge_count,
					M_to_give_N_to,
					roi_stage_in.clusters.cluster[M_to_give_N_to].phi,
					roi_stage_in.clusters.cluster[M_to_give_N_to].eta,
					roi_stage_in.clusters.cluster[M_to_give_N_to].ptt_x,
					roi_stage_in.clusters.cluster[M_to_give_N_to].ptt_y,
					roi_stage_in.clusters.cluster[M_to_give_N_to].ptt_z,
					roi_stage_in.roi_dr2.dr2_row[M_to_give_N_to].merge_count,
					roi_stage_out.clusters.cluster[M_to_give_N_to].ptt_x,
					roi_stage_out.clusters.cluster[M_to_give_N_to].ptt_y,
					roi_stage_out.clusters.cluster[M_to_give_N_to].ptt_z,
					roi_stage_out.roi_dr2.dr2_row[M_to_give_N_to].merge_count,
					roi_stage_in.roi_num,roi_stage_in.iteration
				);
			end
			$write("%t: stage %0d gives %2d (%3d,%3d),(%3d,%3d,%3d,%3d) to %2d (%3d,%3d),(%3d,%3d,%3d,%3d) -> (%3d,%3d,%3d,%3d) ROI%0d.%0d\n",
				$time,THIS_STAGE,
				N_to_disable,
				jet.phi,jet.eta,jet.ptt_x,jet.ptt_y,jet.ptt_z,
				roi_stage_in.roi_dr2.dr2_row[N_to_disable].merge_count,
				M_to_give_N_to,
				roi_stage_in.clusters.cluster[M_to_give_N_to].phi,
				roi_stage_in.clusters.cluster[M_to_give_N_to].eta,
				roi_stage_in.clusters.cluster[M_to_give_N_to].ptt_x,
				roi_stage_in.clusters.cluster[M_to_give_N_to].ptt_y,
				roi_stage_in.clusters.cluster[M_to_give_N_to].ptt_z,
				roi_stage_in.roi_dr2.dr2_row[M_to_give_N_to].merge_count,
				roi_stage_out.clusters.cluster[M_to_give_N_to].ptt_x,
				roi_stage_out.clusters.cluster[M_to_give_N_to].ptt_y,
				roi_stage_out.clusters.cluster[M_to_give_N_to].ptt_z,
				roi_stage_out.roi_dr2.dr2_row[M_to_give_N_to].merge_count,
				roi_stage_in.roi_num,roi_stage_in.iteration
			);
		end
	end
`endif // REPORT_PROGRESS_2
endmodule
/*
`ifdef DEBUG_SIMULATION
	wire signed [3:0] minus_one;
	assign minus_one = -4'sd1;
	for(ROW=0;ROW<32;ROW++) begin
		always @(posedge clk or posedge reset) begin
			if(reset) begin
			end else begin
				if(bucket_capture & stage_enable) begin
					$display("%t: stage %0d row %2d chooses column %2d out of \
%3d,%3d,%3d,%3d, %3d,%3d,%3d,%3d, \
%3d,%3d,%3d,%3d, %3d,%3d,%3d,%3d, \
%3d,%3d,%3d,%3d, %3d,%3d,%3d,%3d, \
%3d,%3d,%3d,%3d, %3d,%3d,%3d,%3d with (%3d,%3d) -> %3d", //  %0d %3d %3d",
						$time,THIS_STAGE,ROW,column_num[ROW],
						(roi_stage_in.disabled[0] || roi_stage_in.disabled[ROW]) ? minus_one : dr2matrix_in.dr2_row[ROW].dr2[0].deltar2,
						(roi_stage_in.disabled[1] || roi_stage_in.disabled[ROW]) ? minus_one : dr2matrix_in.dr2_row[ROW].dr2[1].deltar2,
						(roi_stage_in.disabled[2] || roi_stage_in.disabled[ROW]) ? minus_one : dr2matrix_in.dr2_row[ROW].dr2[2].deltar2,
						(roi_stage_in.disabled[3] || roi_stage_in.disabled[ROW]) ? minus_one : dr2matrix_in.dr2_row[ROW].dr2[3].deltar2,
						(roi_stage_in.disabled[4] || roi_stage_in.disabled[ROW]) ? minus_one : dr2matrix_in.dr2_row[ROW].dr2[4].deltar2,
						(roi_stage_in.disabled[5] || roi_stage_in.disabled[ROW]) ? minus_one : dr2matrix_in.dr2_row[ROW].dr2[5].deltar2,
						(roi_stage_in.disabled[6] || roi_stage_in.disabled[ROW]) ? minus_one : dr2matrix_in.dr2_row[ROW].dr2[6].deltar2,
						(roi_stage_in.disabled[7] || roi_stage_in.disabled[ROW]) ? minus_one : dr2matrix_in.dr2_row[ROW].dr2[7].deltar2,
						(roi_stage_in.disabled[8] || roi_stage_in.disabled[ROW]) ? minus_one : dr2matrix_in.dr2_row[ROW].dr2[8].deltar2,
						(roi_stage_in.disabled[9] || roi_stage_in.disabled[ROW]) ? minus_one : dr2matrix_in.dr2_row[ROW].dr2[9].deltar2,
						(roi_stage_in.disabled[10] || roi_stage_in.disabled[ROW]) ? minus_one : dr2matrix_in.dr2_row[ROW].dr2[10].deltar2,
						(roi_stage_in.disabled[11] || roi_stage_in.disabled[ROW]) ? minus_one : dr2matrix_in.dr2_row[ROW].dr2[11].deltar2,
						(roi_stage_in.disabled[12] || roi_stage_in.disabled[ROW]) ? minus_one : dr2matrix_in.dr2_row[ROW].dr2[12].deltar2,
						(roi_stage_in.disabled[13] || roi_stage_in.disabled[ROW]) ? minus_one : dr2matrix_in.dr2_row[ROW].dr2[13].deltar2,
						(roi_stage_in.disabled[14] || roi_stage_in.disabled[ROW]) ? minus_one : dr2matrix_in.dr2_row[ROW].dr2[14].deltar2,
						(roi_stage_in.disabled[15] || roi_stage_in.disabled[ROW]) ? minus_one : dr2matrix_in.dr2_row[ROW].dr2[15].deltar2,
						(roi_stage_in.disabled[16] || roi_stage_in.disabled[ROW]) ? minus_one : dr2matrix_in.dr2_row[ROW].dr2[16].deltar2,
						(roi_stage_in.disabled[17] || roi_stage_in.disabled[ROW]) ? minus_one : dr2matrix_in.dr2_row[ROW].dr2[17].deltar2,
						(roi_stage_in.disabled[18] || roi_stage_in.disabled[ROW]) ? minus_one : dr2matrix_in.dr2_row[ROW].dr2[18].deltar2,
						(roi_stage_in.disabled[19] || roi_stage_in.disabled[ROW]) ? minus_one : dr2matrix_in.dr2_row[ROW].dr2[19].deltar2,
						(roi_stage_in.disabled[20] || roi_stage_in.disabled[ROW]) ? minus_one : dr2matrix_in.dr2_row[ROW].dr2[20].deltar2,
						(roi_stage_in.disabled[21] || roi_stage_in.disabled[ROW]) ? minus_one : dr2matrix_in.dr2_row[ROW].dr2[21].deltar2,
						(roi_stage_in.disabled[22] || roi_stage_in.disabled[ROW]) ? minus_one : dr2matrix_in.dr2_row[ROW].dr2[22].deltar2,
						(roi_stage_in.disabled[23] || roi_stage_in.disabled[ROW]) ? minus_one : dr2matrix_in.dr2_row[ROW].dr2[23].deltar2,
						(roi_stage_in.disabled[24] || roi_stage_in.disabled[ROW]) ? minus_one : dr2matrix_in.dr2_row[ROW].dr2[24].deltar2,
						(roi_stage_in.disabled[25] || roi_stage_in.disabled[ROW]) ? minus_one : dr2matrix_in.dr2_row[ROW].dr2[25].deltar2,
						(roi_stage_in.disabled[26] || roi_stage_in.disabled[ROW]) ? minus_one : dr2matrix_in.dr2_row[ROW].dr2[26].deltar2,
						(roi_stage_in.disabled[27] || roi_stage_in.disabled[ROW]) ? minus_one : dr2matrix_in.dr2_row[ROW].dr2[27].deltar2,
						(roi_stage_in.disabled[28] || roi_stage_in.disabled[ROW]) ? minus_one : dr2matrix_in.dr2_row[ROW].dr2[28].deltar2,
						(roi_stage_in.disabled[29] || roi_stage_in.disabled[ROW]) ? minus_one : dr2matrix_in.dr2_row[ROW].dr2[29].deltar2,
						(roi_stage_in.disabled[30] || roi_stage_in.disabled[ROW]) ? minus_one : dr2matrix_in.dr2_row[ROW].dr2[30].deltar2,
						(roi_stage_in.disabled[31] || roi_stage_in.disabled[ROW]) ? minus_one : dr2matrix_in.dr2_row[ROW].dr2[31].deltar2,
						roi_stage_in.clusters.cluster[ROW].ptt_x,roi_stage_in.clusters.cluster[ROW].ptt_y,pt2inv[ROW]);//,
				end
			end
		end
	end
	always @(posedge clk or posedge reset) begin
		if(reset) begin
		end else begin
			if(bucket_capture & stage_enable) begin
				$display("%t: stage %0d chooses row %2d from %2d out of \
%2d,%2d,%2d,%2d, %2d,%2d,%2d,%2d, \
%2d,%2d,%2d,%2d, %2d,%2d,%2d,%2d, \
%2d,%2d,%2d,%2d, %2d,%2d,%2d,%2d, \
%2d,%2d,%2d,%2d, %2d,%2d,%2d,%2d",
					$time,THIS_STAGE,row_num,column_num[row_num],
					(roi_stage_in.disabled[0]) ? minus_one : sep_list_minima.dr2[0].deltar2,
					(roi_stage_in.disabled[1]) ? minus_one : sep_list_minima.dr2[1].deltar2,
					(roi_stage_in.disabled[2]) ? minus_one : sep_list_minima.dr2[2].deltar2,
					(roi_stage_in.disabled[3]) ? minus_one : sep_list_minima.dr2[3].deltar2,
					(roi_stage_in.disabled[4]) ? minus_one : sep_list_minima.dr2[4].deltar2,
					(roi_stage_in.disabled[5]) ? minus_one : sep_list_minima.dr2[5].deltar2,
					(roi_stage_in.disabled[6]) ? minus_one : sep_list_minima.dr2[6].deltar2,
					(roi_stage_in.disabled[7]) ? minus_one : sep_list_minima.dr2[7].deltar2,
					(roi_stage_in.disabled[8]) ? minus_one : sep_list_minima.dr2[8].deltar2,
					(roi_stage_in.disabled[9]) ? minus_one : sep_list_minima.dr2[9].deltar2,
					(roi_stage_in.disabled[10]) ? minus_one : sep_list_minima.dr2[10].deltar2,
					(roi_stage_in.disabled[11]) ? minus_one : sep_list_minima.dr2[11].deltar2,
					(roi_stage_in.disabled[12]) ? minus_one : sep_list_minima.dr2[12].deltar2,
					(roi_stage_in.disabled[13]) ? minus_one : sep_list_minima.dr2[13].deltar2,
					(roi_stage_in.disabled[14]) ? minus_one : sep_list_minima.dr2[14].deltar2,
					(roi_stage_in.disabled[15]) ? minus_one : sep_list_minima.dr2[15].deltar2,
					(roi_stage_in.disabled[16]) ? minus_one : sep_list_minima.dr2[16].deltar2,
					(roi_stage_in.disabled[17]) ? minus_one : sep_list_minima.dr2[17].deltar2,
					(roi_stage_in.disabled[18]) ? minus_one : sep_list_minima.dr2[18].deltar2,
					(roi_stage_in.disabled[19]) ? minus_one : sep_list_minima.dr2[19].deltar2,
					(roi_stage_in.disabled[20]) ? minus_one : sep_list_minima.dr2[20].deltar2,
					(roi_stage_in.disabled[21]) ? minus_one : sep_list_minima.dr2[21].deltar2,
					(roi_stage_in.disabled[22]) ? minus_one : sep_list_minima.dr2[22].deltar2,
					(roi_stage_in.disabled[23]) ? minus_one : sep_list_minima.dr2[23].deltar2,
					(roi_stage_in.disabled[24]) ? minus_one : sep_list_minima.dr2[24].deltar2,
					(roi_stage_in.disabled[25]) ? minus_one : sep_list_minima.dr2[25].deltar2,
					(roi_stage_in.disabled[26]) ? minus_one : sep_list_minima.dr2[26].deltar2,
					(roi_stage_in.disabled[27]) ? minus_one : sep_list_minima.dr2[27].deltar2,
					(roi_stage_in.disabled[28]) ? minus_one : sep_list_minima.dr2[28].deltar2,
					(roi_stage_in.disabled[29]) ? minus_one : sep_list_minima.dr2[29].deltar2,
					(roi_stage_in.disabled[30]) ? minus_one : sep_list_minima.dr2[30].deltar2,
					(roi_stage_in.disabled[31]) ? minus_one : sep_list_minima.dr2[31].deltar2);
			end
		end
	end
`endif // DEBUG_SIMULATION
*/