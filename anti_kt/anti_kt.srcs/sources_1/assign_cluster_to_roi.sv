`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03/25/2024 10:08:30 AM
// Design Name: 
// Module Name: assign_cluster_to_roi
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "defines.vh"

module assign_cluster_to_roi(
	input clk,
	input reset,
	input cluster_type cluster_in,
	input cluster_in_valid,
	input new_bunch_crossing,
	input gblock_type center,
	input [($clog2(NUM_ROIS))-1:0] roi_indx,
	input center_valid,
	output [NUM_ROIS-1:0] phi_must_adjust,
	output [NUM_ROIS-1:0] roi_select
	);
	
	integer i;
	genvar ROI;
	
	// this is where we save the ROI center coordinates
	gblock_type roi_center [NUM_ROIS-1:0];
	// a ROI is either ready or ignored,or not initialized
	// combinatorially define the 4 boundaries of 1 or 3 rectangles
	
	logic [ETA_NUM_BITS-1:0] roi_upper_bound [NUM_ROIS-1:0];
	logic [ETA_NUM_BITS-1:0] roi_lower_bound [NUM_ROIS-1:0];
	logic [PHI_NUM_BITS-1:0] roi_left_bound [NUM_ROIS-1:0];
	logic [PHI_NUM_BITS-1:0] roi_right_bound [NUM_ROIS-1:0];
	
	logic [NUM_ROIS-1:0] roi_ready;
	logic [NUM_ROIS-1:0] roi_ignore;
	
	// all ROIs must be ready or ignored before clusters will be processes
	logic all_rois_ready;
	assign all_rois_ready = &(roi_ready | roi_ignore);
	
`ifndef ALL_ROIS_SORTED
	// wires for serial sort
	gblock_type roi_pass [NUM_ROIS-1:0];							// pass_thru from one stage to the next
	logic [($clog2(NUM_ROIS))-1:0] roi_pass_num [NUM_ROIS-1:0];		// "
	gblock_type roi_val [NUM_ROIS-1:0];								// current / final value for a stage
	logic [($clog2(NUM_ROIS))-1:0] roi_val_num [NUM_ROIS-1:0];		// "
		
	//instantiate the serial sort stages
	for(ROI=0;ROI<NUM_ROIS;ROI++) begin
		roi_serial_sort #(.OBJECT_TYPE(gblock_type)) ss (
			.clk(clk),
			.reset(reset),
			.new_bunch_crossing(new_bunch_crossing),
			.roi_in_valid(center_valid),
			.roi_in((ROI==0) ? center : roi_pass[ROI-1]),
			.roi_in_num((ROI==0) ? roi_indx : roi_pass_num[ROI-1]),
			.roi_val(roi_val[ROI]),
			.roi_val_num(roi_val_num[ROI]),
			.roi_pass(roi_pass[ROI]),
			.roi_pass_num(roi_pass_num[ROI])
		);
	end
`endif
    // if the roi center phi is less that 1 radius, adjust both it and the cluster phi
    // to effectively wrap
    
    logic [NUM_ROIS-1:0] roi_phi_must_adjust;
	assign phi_must_adjust = roi_phi_must_adjust;
    
    // phi has to wrap around 63/0
    logic [PHI_NUM_BITS-1:0] cluster_in_phi_effective [NUM_ROIS-1:0];
    
    for(ROI=0;ROI<NUM_ROIS;ROI++) begin
    	assign cluster_in_phi_effective[ROI] = (roi_phi_must_adjust[ROI]) ?
    		((cluster_in.phi + PHI_ADJUST_AMOUNT) % ROI_PHI_SIZE) : cluster_in.phi;
    end
	// roi_phi is 7 bits, thus 0-127
	// a roi at the left spans 0-19 with center at 9.5 or the 9/10 split
	// a roi at the right spans 108-127 with center at 117.5 or the 117/118 split
	// oops
	// a roi at the right spans 44-63 with center at 53.5 or the 53/44 split
	localparam MIN_LEFT = RECT_HORIZONTAL_OFFSET;
	localparam MAX_RIGHT = ROI_PHI_SIZE-RECT_HORIZONTAL_OFFSET-1; // 117
	// roi_eta is 8 bits, thus 0-255
	// a roi at the bottom spans 0-19 with center at 9.5 or the 9/10 split
	// a roi at the top spans 236-255 with center at 245.5 or the 245/246 split
	// oops
	// a roi at the top spans 78-97 with center at 87.5 or the 87/88 split
	localparam MIN_LOWER = RECT_VERTICAL_OFFSET;
	localparam MAX_UPPER = ROI_ETA_SIZE-RECT_VERTICAL_OFFSET-1; // 245
	
	for(ROI=0;ROI<NUM_ROIS;ROI++) begin
		assign roi_upper_bound[ROI] = (roi_center[ROI].eta + RECT_VERTICAL_OFFSET);
		assign roi_lower_bound[ROI] = (roi_center[ROI].eta - (RECT_VERTICAL_OFFSET+1));
		assign roi_right_bound[ROI] = (roi_center[ROI].phi + RECT_HORIZONTAL_OFFSET) % ROI_PHI_SIZE;
		assign roi_left_bound[ROI] = (roi_center[ROI].phi - (RECT_HORIZONTAL_OFFSET+1)) % ROI_PHI_SIZE;
	end

	// test against each edge
	logic  upper_ok [NUM_ROIS-1:0];
	logic lower_ok [NUM_ROIS-1:0];
	logic right_ok [NUM_ROIS-1:0];
	logic left_ok [NUM_ROIS-1:0];
	
	for(ROI=0;ROI<NUM_ROIS;ROI++) begin
		assign upper_ok[ROI] = (cluster_in.eta < roi_upper_bound[ROI]);
		assign lower_ok[ROI] = (cluster_in.eta > roi_lower_bound[ROI]);
		assign right_ok[ROI] = (cluster_in_phi_effective[ROI] < roi_right_bound[ROI]);
		assign left_ok[ROI] = (cluster_in_phi_effective[ROI] > roi_left_bound[ROI]);
	end
	
	// it's a candidate if it's entirely within the box
	logic [NUM_ROIS-1:0] roi_ok;
	for(ROI=0;ROI<NUM_ROIS;ROI++) begin
		assign roi_ok[ROI] = upper_ok[ROI] &
							lower_ok[ROI] &
							right_ok[ROI] &
							left_ok[ROI];
	end

	// pick candidate cluster with highest energy (i.e., lowest roi_num)
	for(ROI=0;ROI<NUM_ROIS;ROI++) begin
		if(ROI==0) begin
`ifdef ALL_ROIS_SORTED
			assign roi_select[ROI] = (all_rois_ready) ? (roi_ready[ROI] & roi_ok[ROI]) : 0;
`else
			assign roi_select[ROI] = (all_rois_ready) ? (roi_ready[roi_val_num[ROI]] & roi_ok[roi_val_num[ROI]]) : 0;
`endif
		end else begin
`ifdef ALL_ROIS_SORTED			
			assign roi_select[ROI] = (all_rois_ready) ? (roi_ready[ROI] & roi_ok[ROI] & !(|(roi_ok[ROI-1:0]))) : 0;
`else
			assign roi_select[ROI] = (all_rois_ready) ? (roi_ready[roi_val_num[ROI]] & roi_ok[roi_val_num[ROI]] & !(|(roi_select[ROI-1:0]))) : 0;
`endif
		end
	end
	
    // accumulate roi info
    always @(posedge clk or posedge reset) begin
        if(reset) begin
            for(i=0;i<NUM_ROIS;i=i+1) begin
//                roi[i].eta <= 0;
//                roi[i].phi <= 0;
				roi_center[i] <= 0;
                roi_ready[i] <= 0;
                roi_ignore[i] <= 0;
                roi_phi_must_adjust[i] <= 0;
            end
        end else begin
			if(new_bunch_crossing) begin
				for(i=0;i<NUM_ROIS;i=i+1) begin
//					roi[i].eta <= 0;
//					roi[i].phi <= 0;
					roi_center[i] <= 0;
					roi_ready[i] <= 0;
					roi_ignore[i] <= 0;
					roi_phi_must_adjust[i] <= 0;
				end
			end else if(center_valid) begin
				// ignore any roi less than 1 radius from eta edge
				roi_center[roi_indx].eta <= center.eta;
				if(center.eta < MIN_LOWER) begin
					roi_ignore[roi_indx] <= 1;
					roi_ready[roi_indx] <= 0;
				end else if(center.eta > MAX_UPPER) begin
					roi_ignore[roi_indx] <= 1;
					roi_ready[roi_indx] <= 0;
				end else if(center.phi > 63) begin
					roi_ignore[roi_indx] <= 1;
					roi_ready[roi_indx] <= 0;
				end else begin
					roi_ignore[roi_indx] <= 0;
					roi_ready[roi_indx] <= 1;
				end
				
				// any roi less than 1 radius from phi edge gets bumped -- so does cluster phi
				if(center.phi < ROI_PHI_SIZE) begin
					if(center.phi < MIN_LEFT) begin
						roi_phi_must_adjust[roi_indx] <= 1;
						roi_center[roi_indx].phi <= center.phi + PHI_ADJUST_AMOUNT;
					end else if(center.phi > MAX_RIGHT) begin
						roi_phi_must_adjust[roi_indx] <= 1;
						roi_center[roi_indx].phi <= center.phi + PHI_ADJUST_AMOUNT;
					end else begin
						roi_phi_must_adjust[roi_indx] <= 0;
						roi_center[roi_indx].phi <= center.phi;
					end
				end
				roi_center[roi_indx].ptt <= center.ptt;
			end
		end
    end            
endmodule
