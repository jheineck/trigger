`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/21/2023 03:41:44 PM
// Design Name: 
// Module Name: bitonicsort32
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "defines.vh"

`ifndef BITONIC_SORT_OP
`define BITONIC_SORT_OP <
`endif

module bitonicsort32 #(parameter type INPUT_TYPE = dr2_row_type,
						parameter type OUTPUT_TYPE = dr2_up_type) (
	input reset,
	input INPUT_TYPE dr2_row,
	input [MAX_CLUSTERS-1:0] disabled,
    output OUTPUT_TYPE minimum,
    output [($clog2(MAX_CLUSTERS))-1:0] cluster_num
    );
    OUTPUT_TYPE [MAX_CLUSTERS-1:0] dr2_N;
//    const logic [DR2_NUM_BITS-1:0] dr2_max = ~0;
    genvar I;
    generate
	    for(I=0;I<MAX_CLUSTERS;I++) begin
//			assign  dr2_N[I].dr2 = dr2_row.dr2[I];
//	    	assign  dr2_N[I].dr2.deltar2 = (disabled[I]) ? dr2_max : dr2_row.dr2[I].deltar2;
	    	assign  dr2_N[I].dr2.deltar2 = (disabled[I]) ? ~0 : dr2_row.dr2[I].deltar2;
	    end
	endgenerate
    
    OUTPUT_TYPE min_0_1, min_2_3, min_4_5, min_6_7, min_8_9, min_10_11, min_12_13,
    			min_14_15, min_16_17, min_18_19, min_20_21, min_22_23, min_24_25,
    			min_26_27, min_28_29, min_30_31;
    OUTPUT_TYPE min_1_3, min_5_7, min_9_11, min_13_15, min_17_19, min_21_23,
    			min_25_27, min_29_31;
    OUTPUT_TYPE min_3_7, min_11_15, min_19_23, min_27_31;
    OUTPUT_TYPE min_7_15, min_23_31;
    OUTPUT_TYPE min_15_31;
    
    logic [($clog2(MAX_CLUSTERS))-1:0] cnum_0_1, cnum_2_3, cnum_4_5, cnum_6_7, cnum_8_9,
    			cnum_10_11, cnum_12_13, cnum_14_15, cnum_16_17, cnum_18_19, cnum_20_21,
    			cnum_22_23, cnum_24_25, cnum_26_27, cnum_28_29, cnum_30_31,
    			cnum_1_3, cnum_5_7, cnum_9_11, cnum_13_15, cnum_17_19, cnum_21_23,
    			cnum_25_27, cnum_29_31,
    			cnum_3_7, cnum_11_15, cnum_19_23, cnum_27_31,
    			cnum_7_15, cnum_23_31, cnum_15_31;
generate
if(MAX_CLUSTERS > 1) begin    			
    logic comp_0_vs_1;
    assign  comp_0_vs_1 = (dr2_N[0].dr2.deltar2 `BITONIC_SORT_OP dr2_N[1].dr2.deltar2);
	assign  min_0_1 = (comp_0_vs_1) ? dr2_N[0] : dr2_N[1];
		assign  cnum_0_1 = (comp_0_vs_1) ? 0 : 1;
end else begin
	assign  min_0_1 = dr2_N[0];
		assign  cnum_0_1 = 0;
end
if(MAX_CLUSTERS > 2) begin
if(MAX_CLUSTERS > 3) begin
	logic comp_2_vs_3;
	assign  comp_2_vs_3 = (dr2_N[2].dr2.deltar2 `BITONIC_SORT_OP dr2_N[3].dr2.deltar2);
	assign  min_2_3 = (comp_2_vs_3) ? dr2_N[2] : dr2_N[3];
		assign  cnum_2_3 = (comp_2_vs_3) ? 2 : 3;
end else begin
	assign  min_2_3 = dr2_N[2];
		assign  cnum_2_3 = 2;
end
if(MAX_CLUSTERS > 4) begin
if(MAX_CLUSTERS > 5) begin
	logic comp_4_vs_5;
	assign  comp_4_vs_5 = (dr2_N[4].dr2.deltar2 `BITONIC_SORT_OP dr2_N[5].dr2.deltar2);
	assign  min_4_5 = (comp_4_vs_5) ? dr2_N[4] : dr2_N[5];
		assign  cnum_4_5 = (comp_4_vs_5) ? 4 : 5;
end else begin
	assign  min_4_5 = dr2_N[4];
		assign  cnum_4_5 = 4;
end
if(MAX_CLUSTERS > 6) begin
if(MAX_CLUSTERS > 7) begin
	logic comp_6_vs_7;
	assign  comp_6_vs_7 = (dr2_N[6].dr2.deltar2 `BITONIC_SORT_OP dr2_N[7].dr2.deltar2);
	assign  min_6_7 = (comp_6_vs_7) ? dr2_N[6] : dr2_N[7];
		assign  cnum_6_7 = (comp_6_vs_7) ? 6 : 7;
end else begin
	assign  min_6_7 = dr2_N[6];
		assign  cnum_6_7 = 6;
end
if(MAX_CLUSTERS > 8) begin
if(MAX_CLUSTERS > 9) begin
	logic comp_8_vs_9;
	assign  comp_8_vs_9 = (dr2_N[8].dr2.deltar2 `BITONIC_SORT_OP dr2_N[9].dr2.deltar2);
	assign  min_8_9 = (comp_8_vs_9) ? dr2_N[8] : dr2_N[9];
		assign  cnum_8_9 = (comp_8_vs_9) ? 8 : 9;
end else begin
	assign  min_8_9 = dr2_N[8];
		assign  cnum_8_9 = 8;
end
if(MAX_CLUSTERS > 10) begin
if(MAX_CLUSTERS > 11) begin
	logic comp_10_vs_11;
	assign  comp_10_vs_11 = (dr2_N[10].dr2.deltar2 `BITONIC_SORT_OP dr2_N[11].dr2.deltar2);
	assign  min_10_11 = (comp_10_vs_11) ? dr2_N[10] : dr2_N[11];
		assign  cnum_10_11 = (comp_10_vs_11) ? 10 : 11;
end else begin
	assign  min_10_11 = dr2_N[10];
		assign  cnum_10_11 = 10;
end
if(MAX_CLUSTERS > 12) begin
if(MAX_CLUSTERS > 13) begin
	logic comp_12_vs_13;
	assign  comp_12_vs_13 = (dr2_N[12].dr2.deltar2 `BITONIC_SORT_OP dr2_N[13].dr2.deltar2);
	assign  min_12_13 = (comp_12_vs_13) ? dr2_N[12] : dr2_N[13];
		assign  cnum_12_13 = (comp_12_vs_13) ? 12 : 13;
end else begin
	assign  min_12_13 = dr2_N[12];
		assign  cnum_12_13 = 12;
end
if(MAX_CLUSTERS > 14) begin
if(MAX_CLUSTERS > 15) begin
	logic comp_14_vs_15;
	assign  comp_14_vs_15 = (dr2_N[14].dr2.deltar2 `BITONIC_SORT_OP dr2_N[15].dr2.deltar2);
	assign  min_14_15 = (comp_14_vs_15) ? dr2_N[14] : dr2_N[15];
		assign  cnum_14_15 = (comp_14_vs_15) ? 14 : 15;
end else begin
	assign  min_14_15 = dr2_N[14];
		assign  cnum_14_15 = 14;
end
if(MAX_CLUSTERS > 16) begin
if(MAX_CLUSTERS > 17) begin
	logic comp_16_vs_17;
	assign  comp_16_vs_17 = (dr2_N[16].dr2.deltar2 `BITONIC_SORT_OP dr2_N[17].dr2.deltar2);
	assign  min_16_17 = (comp_16_vs_17) ? dr2_N[16] : dr2_N[17];
		assign  cnum_16_17 = (comp_16_vs_17) ? 16 : 17;
end else begin
	assign  min_16_17 = dr2_N[16];
		assign  cnum_16_17 = 16;
end
if(MAX_CLUSTERS > 18) begin
if(MAX_CLUSTERS > 19) begin
	logic comp_18_vs_19;
	assign  comp_18_vs_19 = (dr2_N[18].dr2.deltar2 `BITONIC_SORT_OP dr2_N[19].dr2.deltar2);
	assign  min_18_19 = (comp_18_vs_19) ? dr2_N[18] : dr2_N[19];
		assign  cnum_18_19 = (comp_18_vs_19) ? 18 : 19;
end else begin
	assign  min_18_19 = dr2_N[18];
		assign  cnum_18_19 = 18;
end
if(MAX_CLUSTERS > 20) begin
if(MAX_CLUSTERS > 21) begin
	logic comp_20_vs_21;
	assign  comp_20_vs_21 = (dr2_N[20].dr2.deltar2 `BITONIC_SORT_OP dr2_N[21].dr2.deltar2);
	assign  min_20_21 = (comp_20_vs_21) ? dr2_N[20] : dr2_N[21];
		assign  cnum_20_21 = (comp_20_vs_21) ? 20 : 21;
end else begin
	assign  min_20_21 = dr2_N[20];
		assign  cnum_20_21 = 20;
end
if(MAX_CLUSTERS > 22) begin
if(MAX_CLUSTERS > 23) begin
	logic comp_22_vs_23;
	assign  comp_22_vs_23 = (dr2_N[22].dr2.deltar2 `BITONIC_SORT_OP dr2_N[23].dr2.deltar2);
	assign  min_22_23 = (comp_22_vs_23) ? dr2_N[22] : dr2_N[23];
		assign  cnum_22_23 = (comp_22_vs_23) ? 22 : 23;
end else begin
	assign  min_22_23 = dr2_N[22];
		assign  cnum_22_23 = 22;
end
if(MAX_CLUSTERS > 24) begin
if(MAX_CLUSTERS > 25) begin
	logic comp_24_vs_25;
	assign  comp_24_vs_25 = (dr2_N[24].dr2.deltar2 `BITONIC_SORT_OP dr2_N[25].dr2.deltar2);
	assign  min_24_25 = (comp_24_vs_25) ? dr2_N[24] : dr2_N[25];
		assign  cnum_24_25 = (comp_24_vs_25) ? 24 : 25;
end else begin
	assign  min_24_25 = dr2_N[24];
		assign  cnum_24_25 =  24;
end
if(MAX_CLUSTERS > 26) begin
if(MAX_CLUSTERS > 27) begin
	logic comp_26_vs_27;
	assign  comp_26_vs_27 = (dr2_N[26].dr2.deltar2 `BITONIC_SORT_OP dr2_N[27].dr2.deltar2);
	assign  min_26_27 = (comp_26_vs_27) ? dr2_N[26] : dr2_N[27];
		assign  cnum_26_27 = (comp_26_vs_27) ? 26 : 27;
end else begin
	assign  min_26_27 = dr2_N[26];
		assign  cnum_26_27 = 26;
end
if(MAX_CLUSTERS > 28) begin
if(MAX_CLUSTERS > 29) begin
	logic comp_28_vs_29;
	assign  comp_28_vs_29 = (dr2_N[28].dr2.deltar2 `BITONIC_SORT_OP dr2_N[29].dr2.deltar2);
	assign  min_28_29 = (comp_28_vs_29) ? dr2_N[28] : dr2_N[29];
		assign  cnum_28_29 = (comp_28_vs_29) ? 28 : 29;
end else begin
	assign  min_28_29 = dr2_N[28];
		assign  cnum_28_29 = 28;
end
if(MAX_CLUSTERS > 30) begin
if(MAX_CLUSTERS > 31) begin
	logic comp_30_vs_31;
	assign  comp_30_vs_31 = (dr2_N[30].dr2.deltar2 `BITONIC_SORT_OP dr2_N[31].dr2.deltar2);
	assign  min_30_31 = (comp_30_vs_31) ? dr2_N[30] : dr2_N[31];
		assign  cnum_30_31 = (comp_30_vs_31) ? 30 : 31;
end else begin
	assign  min_30_31 = dr2_N[30];
		assign  cnum_30_31 = 30;
end
end
end
end
end
end
end
end
end
end
end
end
end
end
end
end

if(MAX_CLUSTERS > 2) begin
	logic comp_1_vs_3;
	assign  comp_1_vs_3 = (min_0_1.dr2.deltar2 `BITONIC_SORT_OP min_2_3.dr2.deltar2);
	assign  min_1_3 = (comp_1_vs_3) ? min_0_1 : min_2_3;
		assign  cnum_1_3 = (comp_1_vs_3) ? cnum_0_1 : cnum_2_3;
end else begin
	assign  min_1_3 = min_0_1;
		assign  cnum_1_3 = cnum_0_1;
end
if(MAX_CLUSTERS > 4) begin
if(MAX_CLUSTERS > 6) begin
	logic comp_5_vs_7;
	assign  comp_5_vs_7 = (min_4_5.dr2.deltar2 `BITONIC_SORT_OP min_6_7.dr2.deltar2);
	assign  min_5_7 = (comp_5_vs_7) ? min_4_5 : min_6_7;
		assign  cnum_5_7 = (comp_5_vs_7) ? cnum_4_5 : cnum_6_7;
end else begin
	assign  min_5_7 = min_4_5;
		assign  cnum_5_7 = cnum_4_5;
end
if(MAX_CLUSTERS > 8) begin
if(MAX_CLUSTERS > 10) begin
	logic comp_9_vs_11;
	assign  comp_9_vs_11 = (min_8_9.dr2.deltar2 `BITONIC_SORT_OP min_10_11.dr2.deltar2);
	assign  min_9_11 = (comp_9_vs_11) ? min_8_9 : min_10_11;
		assign  cnum_9_11 = (comp_9_vs_11) ? cnum_8_9 : cnum_10_11;
end else begin
	assign  min_9_11 = min_8_9;
		assign  cnum_9_11 = cnum_8_9;
end
if(MAX_CLUSTERS > 12) begin
if(MAX_CLUSTERS > 14) begin
	logic comp_13_vs_15;
	assign  comp_13_vs_15 = (min_12_13.dr2.deltar2 `BITONIC_SORT_OP min_14_15.dr2.deltar2);
	assign  min_13_15 = (comp_13_vs_15) ? min_12_13 : min_14_15;
		assign  cnum_13_15 = (comp_13_vs_15) ? cnum_12_13 : cnum_14_15;
end else begin
	assign  min_13_15 = min_12_13;
		assign  cnum_13_15 = cnum_12_13;
end
if(MAX_CLUSTERS > 16) begin
if(MAX_CLUSTERS > 18) begin
	logic comp_17_vs_19;
	assign  comp_17_vs_19 = (min_16_17.dr2.deltar2 `BITONIC_SORT_OP min_18_19.dr2.deltar2);
	assign  min_17_19 = (comp_17_vs_19) ? min_16_17 : min_18_19;
		assign  cnum_17_19 = (comp_17_vs_19) ? cnum_16_17 : cnum_18_19;
end else begin
	assign  min_17_19 = min_16_17;
		assign  cnum_17_19 = cnum_16_17;
end
if(MAX_CLUSTERS > 20) begin
if(MAX_CLUSTERS > 22) begin
	logic comp_21_vs_23;
	assign  comp_21_vs_23 = (min_20_21.dr2.deltar2 `BITONIC_SORT_OP min_22_23.dr2.deltar2);
	assign  min_21_23 = (comp_21_vs_23) ? min_20_21 : min_22_23;
		assign  cnum_21_23 = (comp_21_vs_23) ? cnum_20_21 : cnum_22_23;
end else begin
	assign  min_21_23 = min_20_21;
		assign  cnum_21_23 = cnum_20_21;
end
if(MAX_CLUSTERS > 24) begin
if(MAX_CLUSTERS > 26) begin
	logic comp_25_vs_27;
	assign  comp_25_vs_27 = (min_24_25.dr2.deltar2 `BITONIC_SORT_OP min_26_27.dr2.deltar2);
	assign  min_25_27 = (comp_25_vs_27) ? min_24_25 : min_26_27;
		assign  cnum_25_27 = (comp_25_vs_27) ? cnum_24_25 : cnum_26_27;
end else begin
	assign  min_25_27 = min_24_25;
		assign  cnum_25_27 = cnum_24_25;
end
if(MAX_CLUSTERS > 28) begin
if(MAX_CLUSTERS > 30) begin
	logic comp_29_vs_31;
	assign  comp_29_vs_31 = (min_28_29.dr2.deltar2 `BITONIC_SORT_OP min_30_31.dr2.deltar2);
	assign  min_29_31 = (comp_29_vs_31) ? min_28_29 : min_30_31;
		assign  cnum_29_31 = (comp_29_vs_31) ? cnum_28_29 : cnum_30_31;
end else begin
	assign  min_29_31 = min_28_29;
		assign  cnum_29_31 = cnum_28_29;
end
end
end
end
end
end
end
end


if(MAX_CLUSTERS > 4) begin
	logic comp_3_vs_7;
	assign  comp_3_vs_7 = (min_1_3.dr2.deltar2 `BITONIC_SORT_OP min_5_7.dr2.deltar2);
	assign  min_3_7 = (comp_3_vs_7) ? min_1_3 : min_5_7;
		assign  cnum_3_7 = (comp_3_vs_7) ? cnum_1_3 : cnum_5_7;
end else begin
	assign  min_3_7 = min_1_3;
		assign  cnum_3_7 = cnum_1_3;
end
if(MAX_CLUSTERS > 8) begin
if(MAX_CLUSTERS > 12) begin
	logic comp_11_vs_15;
	assign  comp_11_vs_15 = (min_9_11.dr2.deltar2 `BITONIC_SORT_OP min_13_15.dr2.deltar2);
	assign  min_11_15 = (comp_11_vs_15) ? min_9_11 : min_13_15;
		assign  cnum_11_15 = (comp_11_vs_15) ? cnum_9_11 : cnum_13_15;
end else begin
	assign  min_11_15 = min_9_11;
		assign  cnum_11_15 =  cnum_9_11;
end
if(MAX_CLUSTERS > 16) begin
if(MAX_CLUSTERS > 20) begin
	logic comp_19_vs_23;
	assign  comp_19_vs_23 = (min_17_19.dr2.deltar2 `BITONIC_SORT_OP min_21_23.dr2.deltar2);
	assign  min_19_23 = (comp_19_vs_23) ? min_17_19 : min_21_23;
		assign  cnum_19_23 = (comp_19_vs_23) ? cnum_17_19 : cnum_21_23;
end else begin
	assign  min_19_23 = min_17_19;
		assign  cnum_19_23 = cnum_17_19;
end
if(MAX_CLUSTERS > 24) begin
if(MAX_CLUSTERS > 28) begin
	logic comp_27_vs_31;
	assign  comp_27_vs_31 = (min_25_27.dr2.deltar2 `BITONIC_SORT_OP min_29_31.dr2.deltar2);
	assign  min_27_31 = (comp_27_vs_31) ? min_25_27 : min_29_31;
		assign  cnum_27_31 = (comp_27_vs_31) ? cnum_25_27 : cnum_29_31;
end else begin
	assign  min_27_31 = min_25_27;
		assign  cnum_27_31 = cnum_25_27;
end
end
end
end

if(MAX_CLUSTERS > 8) begin
	logic comp_7_vs_15;
	assign  comp_7_vs_15 = (min_3_7.dr2.deltar2 `BITONIC_SORT_OP min_11_15.dr2.deltar2);
	assign  min_7_15 = (comp_7_vs_15) ? min_3_7 : min_11_15;
		assign  cnum_7_15 = (comp_7_vs_15) ? cnum_3_7 : cnum_11_15;
end else begin
	assign  min_7_15 = min_3_7;
		assign  cnum_7_15 = cnum_3_7;
end
if(MAX_CLUSTERS > 24) begin
	logic comp_23_vs_31;
	assign  comp_23_vs_31 = (min_19_23.dr2.deltar2 `BITONIC_SORT_OP min_27_31.dr2.deltar2);
	assign  min_23_31 = (comp_23_vs_31) ? min_19_23 : min_27_31;
		assign  cnum_23_31 = (comp_23_vs_31) ? cnum_19_23 : cnum_27_31;
end else begin
	assign  min_23_31 = min_19_23;
		assign  cnum_23_31 = cnum_19_23;
end
	
if(MAX_CLUSTERS > 16) begin
	logic comp_15_vs_31;
	assign  comp_15_vs_31 = (min_7_15.dr2.deltar2 `BITONIC_SORT_OP min_23_31.dr2.deltar2);
	assign  min_15_31 = (comp_15_vs_31) ? min_7_15 : min_23_31;
		assign  cnum_15_31 = (comp_15_vs_31) ? cnum_7_15 : cnum_23_31;
end else begin
	assign  min_15_31 = min_7_15;
		assign  cnum_15_31 = cnum_7_15;
end

	assign  minimum = min_15_31;
	assign  cluster_num = cnum_15_31;
endgenerate
endmodule
