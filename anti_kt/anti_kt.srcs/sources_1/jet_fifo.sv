`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12/11/2023 04:51:21 PM
// Design Name: 
// Module Name: jet_fifo
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "defines.vh"

module jet_fifo (
	input clk,
	input reset,
	input clear,
	input push_enable,
	input pop_enable,
	input [($clog2(NUM_ROIS))-1:0] roi_num,
	input [($clog2(MAX_CLUSTERS))-1:0] cluster_num,
	input cluster_type jet,
	output [($clog2(NUM_ROIS))-1:0] o_roi_num,
	output [($clog2(MAX_CLUSTERS))-1:0] o_cluster_num,
	output cluster_type o_jet,
//	output [($clog2(MAX_CLUSTERS))-1:0] num_stored
	output empty
);
	reg [($clog2(MAX_CLUSTERS))-1:0] write_addr, read_addr;
	typedef struct packed {
		reg [($clog2(NUM_ROIS))-1:0] roi_num;
		reg [($clog2(MAX_CLUSTERS))-1:0] cluster_num;
		cluster_type jet;
	} fifo_data_type;
	
	fifo_data_type [((MAX_CLUSTERS*NUM_ROIS)/(NUM_BUCKETS*STAGES_PER_BUCKET))-1:0] fifo_ram;
	fifo_data_type write_datum, read_datum;
	assign write_datum.roi_num = roi_num;
	assign write_datum.cluster_num = cluster_num;
	assign write_datum.jet = jet;
	
//	assign read_datum = fifo_ram[read_addr];
	assign o_roi_num = read_datum.roi_num;
	assign o_cluster_num = read_datum.cluster_num;
	assign o_jet = read_datum.jet;
	
//	assign num_stored = write_addr;
	assign empty = !(read_addr != write_addr);
	
	integer i;
	always @(posedge clk or posedge reset) begin
		if(reset) begin
			write_addr <= 0;
			for(i=0;i<((MAX_CLUSTERS*NUM_ROIS)/(NUM_BUCKETS*STAGES_PER_BUCKET));i++) begin
				fifo_ram[i] <= 0;
			end
		end else if(clear) begin
			write_addr <= 0;
			for(i=0;i<((MAX_CLUSTERS*NUM_ROIS)/(NUM_BUCKETS*STAGES_PER_BUCKET));i++) begin
				fifo_ram[i] <= 0;
			end
		end else begin
			if(push_enable) begin
				fifo_ram[write_addr] <= write_datum;
				write_addr <= write_addr + 1;
			end
		end
	end
	always @(posedge clk or posedge reset) begin
		if(reset) begin
			read_addr <= 0;
			read_datum <= 0;
		end else if(clear) begin
			read_addr <= 0;
			read_datum <= 0;
		end else begin
			read_datum <= fifo_ram[read_addr];
			if(pop_enable) begin
				if(read_addr != write_addr) begin
					read_addr <= read_addr + 1;
				end
			end
		end
	end

endmodule
