`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12/05/2023 02:44:07 PM
// Design Name: 
// Module Name: anti_kt
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "defines.vh"

module DR2_MAC (
	input clk,
	input sclr,
	input ce,
	input sel,
	input [7:0] A, D,
	output reg [57:0] P
	);

	localparam NUM_FF_STAGES = 5;
	reg [7:0] Aff[NUM_FF_STAGES-1:0];
	reg [7:0] Dff[NUM_FF_STAGES-1:0];
	integer i;
	always @(posedge clk) begin
		if(sclr) begin
			P <= 0;
			for(i=0;i<NUM_FF_STAGES;i++) begin
				Aff[i] <= 0; Dff[i] <= 0;
			end
		end else begin
			if(ce) begin
				for(i=0;i<NUM_FF_STAGES-1;i++) begin
					Aff[i+1] <= Aff[i]; Aff[0] <= A;
					Dff[i+1] <= Dff[i]; Dff[0] <= D;
				end
				P <= (sel) ? (Dff[NUM_FF_STAGES-1]-Aff[NUM_FF_STAGES-1])*(Dff[NUM_FF_STAGES-1]-Aff[NUM_FF_STAGES-1])+P : (Dff[NUM_FF_STAGES-1]-Aff[NUM_FF_STAGES-1])*(Dff[NUM_FF_STAGES-1]-Aff[NUM_FF_STAGES-1]);
			end
		end
	end
endmodule
module PT2_MAC (
	input clk,
	input sclr,
	input ce,
	input sel,
	input [ENERGY_NUM_BITS-1:0] A, D,
	output reg [57:0] P
	);

	localparam NUM_FF_STAGES = 5;
	reg [ENERGY_NUM_BITS-1:0] Aff[NUM_FF_STAGES-1:0];
	reg [ENERGY_NUM_BITS-1:0] Dff[NUM_FF_STAGES-1:0];
	integer i;
	always @(posedge clk) begin
		if(sclr) begin
			P <= 0;
			for(i=0;i<NUM_FF_STAGES;i++) begin
				Aff[i] <= 0; Dff[i] <= 0;
			end
		end else begin
			if(ce) begin
				for(i=0;i<NUM_FF_STAGES-1;i++) begin
					Aff[i+1] <= Aff[i]; Aff[0] <= A;
					Dff[i+1] <= Dff[i]; Dff[0] <= D;
				end
				P <= (sel) ? (Dff[NUM_FF_STAGES-1]-Aff[NUM_FF_STAGES-1])*(Dff[NUM_FF_STAGES-1]-Aff[NUM_FF_STAGES-1])+P :
							(Dff[NUM_FF_STAGES-1]-Aff[NUM_FF_STAGES-1])*(Dff[NUM_FF_STAGES-1]-Aff[NUM_FF_STAGES-1]);
			end
		end
	end
endmodule