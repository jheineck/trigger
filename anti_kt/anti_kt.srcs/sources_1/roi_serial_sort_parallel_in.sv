`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/31/2023 01:56:04 PM
// Design Name: 
// Module Name: serial_sort
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: input is compared against current value
// 				greater stays, or becomes, new current value
//				lesser passes through to next stage
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "defines.vh"

module roi_serial_sort #(
	parameter integer NUM_STAGES = 4,
	parameter type OBJECT_TYPE = gblock_type) (
    input clk,
    input reset,
    input new_bunch_crossing,		// new bunch crossing, clear roi data
    input roi_in_valid,
    input OBJECT_TYPE roi_pass_in,						// candidate ROI
    input [($clog2(NUM_STAGES))-1:0] roi_pass_in_num,		// "
	
	input OBJECT_TYPE roi_top_in,
	input [($clog2(NUM_STAGES))-1:0] roi_top_in_num,
	input caught_in,
	output caught_out,
	
    output OBJECT_TYPE roi_val,					// value at this stage
    output reg [($clog2(NUM_STAGES))-1:0] roi_val_num,	// "
    output OBJECT_TYPE roi_pass_out,						// pass-through to next stage
    output [($clog2(NUM_STAGES))-1:0] roi_pass_out_num
    );
	OBJECT_TYPE roi_in;
	wire [($clog2(NUM_STAGES))-1:0] roi_select_num;
	logic caught;
	
    OBJECT_TYPE roi_select;						// temporary new value for this stage
    wire [($clog2(NUM_STAGES))-1:0] roi_select_num;
	
	assign caught_out = caught_in | caught;
	assign roi_in = (caught_in) ? roi_pass_in : roi_top_in;
	assign roi_in_num = (caught_in) ? roi_pass_in_num : roi_top_in_num;
    
    // choose which of input or current stays or passes through
    assign roi_select = (roi_in.ptt > roi_val.ptt ) ?
    	roi_in : roi_val;
    assign roi_select_num = (roi_in.ptt > roi_val.ptt) ?
    	roi_in_num : roi_val_num;
    assign roi_pass_out = (roi_in.ptt > roi_val.ptt) ?
    	roi_val : roi_in;
    assign roi_pass_out_num = (roi_in.ptt > roi_val.ptt) ?
    	roi_val_num : roi_in_num;

	// update current value    	
    always @(posedge clk or posedge reset) begin
    	if(reset) begin
    		roi_val <= '{default:0};
    		roi_val_num <= 0;
    	end else begin
    		if(new_bunch_crossing) begin
    			roi_val <= '{default:0};
    			roi_val_num <= 0;
    		end else begin
    			if(roi_in_valid) begin
					roi_val <= roi_select;
					roi_val_num <= roi_select_num;
				end
    		end
    	end
    end
endmodule
