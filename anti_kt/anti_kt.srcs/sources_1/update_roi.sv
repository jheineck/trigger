`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/28/2023 01:04:22 PM
// Design Name: 
// Module Name: update_roi
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.1 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "defines.vh"

module update_roi(
	input reset,
	input roi_type roi_ur_in,
	output roi_type roi_ur_out,
	input disable_N,
	input [($clog2(MAX_CLUSTERS))-1:0] N_to_disable,
	input [($clog2(MAX_CLUSTERS))-1:0] M_to_give_N_to,
	input cluster_type jet,
	input [($clog2(MAX_CLUSTERS)):0] merge_count,
	input N_is_a_jet
);
	assign #10 roi_ur_out.iteration = roi_ur_in.iteration + 1;
		// as long as we're here, might as well compute the new pt2inv, too
	logic [ENERGY_NUM_BITS-1:0] jet_M_ptt_x, jet_M_ptt_y;
	logic [(2*ENERGY_NUM_BITS):0] ptx2, pty2;
	pt2inv_type new_pt2inv;
	
	// not that these are taken from the accumulated ptt in the OUTPUT roi
	// which are computed below
	assign jet_M_ptt_x = roi_ur_out.clusters.cluster[M_to_give_N_to].ptt_x;
	assign jet_M_ptt_y = roi_ur_out.clusters.cluster[M_to_give_N_to].ptt_y;
	
	pt2inv_mult ptx2_mult (
		.A(jet_M_ptt_x),
		.B(jet_M_ptt_x),
		.P(ptx2)
	);
	
	pt2inv_mult pty2_mult (
		.A(jet_M_ptt_y),
		.B(jet_M_ptt_y),
		.P(pty2)
	);
	
	assign new_pt2inv = (~(ptx2+pty2));
	assign  roi_ur_out.roi_num = roi_ur_in.roi_num;
	
	genvar I, J;

	for(I=0;I<MAX_CLUSTERS;I++) begin
		assign #10  roi_ur_out.disabled[I] = ((N_to_disable == I) & (disable_N)) ? disable_N : roi_ur_in.disabled[I];
		
		assign #10  roi_ur_out.is_a_jet[I] = ((N_to_disable == I) & (disable_N)) ? N_is_a_jet : roi_ur_in.is_a_jet[I];
					
		assign #10  roi_ur_out.clusters.cluster[I].ptt_x = (disable_N & (!N_is_a_jet) & (M_to_give_N_to == I)) ?
				roi_ur_in.clusters.cluster[I].ptt_x + 
				jet.ptt_x :
				roi_ur_in.clusters.cluster[I].ptt_x;

		assign #10  roi_ur_out.clusters.cluster[I].ptt_y = (disable_N & (!N_is_a_jet) & (M_to_give_N_to == I)) ?
				roi_ur_in.clusters.cluster[I].ptt_y + 
				jet.ptt_y :
				roi_ur_in.clusters.cluster[I].ptt_y;
				
		assign #10  roi_ur_out.clusters.cluster[I].ptt_z = (disable_N & (!N_is_a_jet) & (M_to_give_N_to == I)) ?
				roi_ur_in.clusters.cluster[I].ptt_z + 
				jet.ptt_z :
				roi_ur_in.clusters.cluster[I].ptt_z;
				
		// let the obsolete value of this pt2inv flow through, even though it won't be used in the next iteration
		assign #10 roi_ur_out.roi_dr2.dr2_row[I].pt2inv = (disable_N & (!N_is_a_jet) &
				(M_to_give_N_to == I)) ? new_pt2inv : roi_ur_in.roi_dr2.dr2_row[I].pt2inv;
		assign #10 roi_ur_out.roi_dr2.dr2_row[I].merge_count = (disable_N & (!N_is_a_jet) &
				(M_to_give_N_to == I)) ?
					(roi_ur_in.roi_dr2.dr2_row[I].merge_count + merge_count) :
					roi_ur_in.roi_dr2.dr2_row[I].merge_count;
				
		// everything else
		assign  roi_ur_out.clusters.cluster[I].phi = roi_ur_in.clusters.cluster[I].phi;
		assign  roi_ur_out.clusters.cluster[I].eta = roi_ur_in.clusters.cluster[I].eta;
		assign  roi_ur_out.clusters.cluster[I].error_bits = roi_ur_in.clusters.cluster[I].error_bits;
//		for(J=0;J<MAX_CLUSTERS;J++) begin
//			assign  roi_ur_out.roi_dr2.dr2_row[I].dr2[J].deltar2 = roi_ur_in.roi_dr2.dr2_row[I].dr2[J].deltar2;
//		end
	end
endmodule
