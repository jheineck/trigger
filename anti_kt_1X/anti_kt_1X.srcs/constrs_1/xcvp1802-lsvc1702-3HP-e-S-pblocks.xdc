create_pblock pblock_akt
resize_pblock [get_pblocks pblock_akt] -add {SLR0}
resize_pblock [get_pblocks pblock_akt] -add {SLR1}
add_cells_to_pblock [get_pblocks pblock_akt] [get_cells -quiet [list akt]]
add_cells_to_pblock [get_pblocks pblock_akt] [get_cells -quiet * -filter {NAME =~ "*BUF*"}]