create_clock -period 3.125 -name clk -waveform {0.000 1.5625} [get_ports -filter { NAME =~  "*clk*" && DIRECTION == "IN" }]

set min_input_delay 0.6
set max_input_delay 1.0

set_input_delay -clock [get_clocks clk] -min -add_delay $min_input_delay [get_ports reset]
set_input_delay -clock [get_clocks clk] -max -add_delay $max_input_delay [get_ports reset]

set_input_delay -clock [get_clocks clk] -min -add_delay $min_input_delay [get_ports fsm_go]
set_input_delay -clock [get_clocks clk] -max -add_delay $max_input_delay [get_ports fsm_go]
#set_input_delay -clock [get_clocks clk] -min -add_delay $min_input_delay [get_ports report_go]
#set_input_delay -clock [get_clocks clk] -max -add_delay $max_input_delay [get_ports report_go]
set_input_delay -clock [get_clocks clk] -min -add_delay $min_input_delay [get_ports {num_wait_states[*]}]
set_input_delay -clock [get_clocks clk] -max -add_delay $max_input_delay [get_ports {num_wait_states[*]}]
#set_input_delay -clock [get_clocks clk] -min -add_delay $min_input_delay [get_ports preproc_go]
#set_input_delay -clock [get_clocks clk] -max -add_delay $max_input_delay [get_ports preproc_go]
set_input_delay -clock [get_clocks clk] -min -add_delay $min_input_delay [get_ports center[*]]
set_input_delay -clock [get_clocks clk] -max -add_delay $max_input_delay [get_ports center[*]]
set_input_delay -clock [get_clocks clk] -min -add_delay $min_input_delay [get_ports cluster_in[*]]
set_input_delay -clock [get_clocks clk] -max -add_delay $max_input_delay [get_ports cluster_in[*]]
set_input_delay -clock [get_clocks clk] -min -add_delay $min_input_delay [get_ports roi_indx[*]]
set_input_delay -clock [get_clocks clk] -max -add_delay $max_input_delay [get_ports roi_indx[*]]


#set_input_delay -clock [get_clocks clk] -min -add_delay $min_input_delay [get_ports q]
#set_input_delay -clock [get_clocks clk] -max -add_delay $max_input_delay [get_ports q]
#set_input_delay -clock [get_clocks clk] -min -add_delay $min_input_delay [get_ports q[*]]
#set_input_delay -clock [get_clocks clk] -max -add_delay $max_input_delay [get_ports q[*]]

# we subtract out the clock input buffer delay and data output buffer delay

set max_output_delay -5.0
set min_output_delay -4.6

#set_output_delay -clock [get_clocks clk] -min -add_delay $min_output_delay [get_ports preproc_busy]
#set_output_delay -clock [get_clocks clk] -max -add_delay $max_output_delay [get_ports preproc_busy]
#set_output_delay -clock [get_clocks clk] -min -add_delay $min_output_delay [get_ports preproc_done]
#set_output_delay -clock [get_clocks clk] -max -add_delay $max_output_delay [get_ports preproc_done]
set_output_delay -clock [get_clocks clk] -min -add_delay $min_output_delay [get_ports fsm_busy]
set_output_delay -clock [get_clocks clk] -max -add_delay $max_output_delay [get_ports fsm_busy]
set_output_delay -clock [get_clocks clk] -min -add_delay $min_output_delay [get_ports fsm_done]
set_output_delay -clock [get_clocks clk] -max -add_delay $max_output_delay [get_ports fsm_done]
#set_output_delay -clock [get_clocks clk] -min -add_delay $min_output_delay [get_ports report_busy]
#set_output_delay -clock [get_clocks clk] -max -add_delay $max_output_delay [get_ports report_busy]
#set_output_delay -clock [get_clocks clk] -min -add_delay $min_output_delay [get_ports report_done]
#set_output_delay -clock [get_clocks clk] -max -add_delay $max_output_delay [get_ports report_done]
set_output_delay -clock [get_clocks clk] -min -add_delay $min_output_delay [get_ports {report_jet[*]}]
set_output_delay -clock [get_clocks clk] -max -add_delay $max_output_delay [get_ports {report_jet[*]}]
set_output_delay -clock [get_clocks clk] -min -add_delay $min_output_delay [get_ports report_jet_valid]
set_output_delay -clock [get_clocks clk] -max -add_delay $max_output_delay [get_ports report_jet_valid]
set_output_delay -clock [get_clocks clk] -min -add_delay $min_output_delay [get_ports {report_roi_num[*]}]
set_output_delay -clock [get_clocks clk] -max -add_delay $max_output_delay [get_ports {report_roi_num[*]}]
set_output_delay -clock [get_clocks clk] -min -add_delay $min_output_delay [get_ports {report_cluster_num[*]}]
set_output_delay -clock [get_clocks clk] -max -add_delay $max_output_delay [get_ports {report_cluster_num[*]}]

# hold delay about 9*STAGES_PER_BUCKET
# setup delay hold_delay+1
# 4 stages/bucket
#set hold_delay 37
#set setup_delay 38
#2 stages/bucket
set hold_delay 19
set setup_delay 20

#set _xlnx_shared_i0 [get_pins {akt/genblk2[*].genblk1[*].mf/fifo_ram_reg*/*} -filter {NAME =~ "*/D"}]
#set_multicycle_path -setup -from [get_clocks clk] -to $_xlnx_shared_i0 $setup_delay
#set_multicycle_path -hold -from [get_clocks clk] -to $_xlnx_shared_i0 $hold_delay

set _xlnx_shared_i1 [get_pins {akt/akb/genblk1[0].bs/br/Q_reg[*][*]*/C}]
#set _xlnx_shared_i2 [get_pins {akt/genblk1[0].genblk1[*].*/*} -filter {NAME =~ "*/CE"}]
#set_multicycle_path -setup -from $_xlnx_shared_i1 -to $_xlnx_shared_i2 $setup_delay
#set _xlnx_shared_i3 [get_pins {akt/genblk1[0].genblk1[*].*/*} -filter {NAME =~ "*/D"}]
#set_multicycle_path -setup -from $_xlnx_shared_i1 -to $_xlnx_shared_i3 $setup_delay
#set_multicycle_path -hold -from $_xlnx_shared_i1 -to $_xlnx_shared_i2 $hold_delay
#set_multicycle_path -hold -from $_xlnx_shared_i1 -to $_xlnx_shared_i3 $hold_delay

set _xlnx_shared_i4 [get_pins {akt/akb/genblk1[1].bs/br/Q_reg[*][*]*/C}]
#set _xlnx_shared_i5 [get_pins {akt/genblk1[1].genblk1[*].*/*} -filter {NAME =~ "*/CE"}]
#set_multicycle_path -setup -from $_xlnx_shared_i4 -to $_xlnx_shared_i5 $setup_delay
#set _xlnx_shared_i6 [get_pins {akt/genblk1[1].genblk1[*].*/*} -filter {NAME =~ "*/D"}]
#set_multicycle_path -setup -from $_xlnx_shared_i4 -to $_xlnx_shared_i6 $setup_delay
#set_multicycle_path -hold -from $_xlnx_shared_i4 -to $_xlnx_shared_i5 $hold_delay
#set_multicycle_path -hold -from $_xlnx_shared_i4 -to $_xlnx_shared_i6 $hold_delay

set _xlnx_shared_i7 [get_pins {akt/akb/genblk1[1].bs/br/Q_reg[*][*]*/*} -filter {NAME =~ "*/CE"}]
set_multicycle_path -setup -from $_xlnx_shared_i1 -to $_xlnx_shared_i7 $setup_delay
set _xlnx_shared_i8 [get_pins {akt/akb/genblk1[1].bs/br/Q_reg[*][*]*/*} -filter {NAME =~ "*/D"}]
set_multicycle_path -setup -from $_xlnx_shared_i1 -to $_xlnx_shared_i8 $setup_delay
set_multicycle_path -hold -from $_xlnx_shared_i1 -to $_xlnx_shared_i7 $hold_delay
set_multicycle_path -hold -from $_xlnx_shared_i1 -to $_xlnx_shared_i8 $hold_delay

set _xlnx_shared_i9 [get_pins {akt/akb/genblk1[0].bs/br/Q_reg[*][*]*/*} -filter {NAME =~ "*/CE"}]
set_multicycle_path -setup -from $_xlnx_shared_i1 -to $_xlnx_shared_i9 $setup_delay
set _xlnx_shared_i10 [get_pins {akt/akb/genblk1[0].bs/br/Q_reg[*][*]*/*} -filter {NAME =~ "*/D"}]
set_multicycle_path -setup -from $_xlnx_shared_i1 -to $_xlnx_shared_i10 $setup_delay
set_multicycle_path -hold -from $_xlnx_shared_i1 -to $_xlnx_shared_i9 $hold_delay
set_multicycle_path -hold -from $_xlnx_shared_i1 -to $_xlnx_shared_i10 $hold_delay

set_multicycle_path -setup -from $_xlnx_shared_i4 -to $_xlnx_shared_i7 $setup_delay
set_multicycle_path -setup -from $_xlnx_shared_i4 -to $_xlnx_shared_i8 $setup_delay
set_multicycle_path -hold -from $_xlnx_shared_i4 -to $_xlnx_shared_i7 $hold_delay
set_multicycle_path -hold -from $_xlnx_shared_i4 -to $_xlnx_shared_i8 $hold_delay

set_false_path -from [get_ports reset] -to [all_registers]

set dr2_setup_delay 10
set dr2_hold_delay 9

#set _xlnx_shared_i11 [get_pins {akt/f_roi_num_reg[*]*/*} -filter {NAME =~ "*/C"}]
#set _xlnx_shared_i18 [get_pins {akt/column_reg[*]*/*} -filter {NAME =~ "*/C"}]
#set _xlnx_shared_i20 [get_pins {akt/row_reg[*]*/*} -filter {NAME =~ "*/C"}]
#set _xlnx_shared_i13 [get_pins {akt/bc_in_reg[roi][*][roi_dr2][dr2_row][*][dr2][*][deltar2][*]/D}]

#set_multicycle_path -setup -from $_xlnx_shared_i11 -to $_xlnx_shared_i13 $dr2_setup_delay
#set_multicycle_path -setup -from $_xlnx_shared_i18 -to $_xlnx_shared_i13 $dr2_setup_delay
#set_multicycle_path -setup -from $_xlnx_shared_i20 -to $_xlnx_shared_i13 $dr2_setup_delay
#set_multicycle_path -hold -from $_xlnx_shared_i11 -to $_xlnx_shared_i13 $dr2_hold_delay
#set_multicycle_path -hold -from $_xlnx_shared_i18 -to $_xlnx_shared_i13 $dr2_hold_delay
#set_multicycle_path -hold -from $_xlnx_shared_i20 -to $_xlnx_shared_i13 $dr2_hold_delay




#set _xlnx_shared_i14 [get_pins {akt/bc_in_reg[roi][*][clusters][cluster][*][phi][*]*/*} -filter {NAME =~ "*/C"}]
#set _xlnx_shared_i15 [get_pins {akt/bc_in_reg[roi][*][clusters][cluster][*][eta][*]*/*} -filter {NAME =~ "*/C"}]
#set_multicycle_path -setup -from $_xlnx_shared_i14 -to $_xlnx_shared_i13 $dr2_setup_delay
#set_multicycle_path -setup -from $_xlnx_shared_i15 -to $_xlnx_shared_i13 $dr2_setup_delay
#set_multicycle_path -hold -from $_xlnx_shared_i14 -to $_xlnx_shared_i13 $dr2_hold_delay
#set_multicycle_path -hold -from $_xlnx_shared_i15 -to $_xlnx_shared_i13 $dr2_hold_delay
