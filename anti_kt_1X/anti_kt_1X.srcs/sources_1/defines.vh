`ifndef DEFINES_DEFINED
`define DEFINES_DEFINED

`define ALL_DATASETS
//`define FULL_ROIS
`ifdef FULL_ROIS
`define LOAD_BC_PARTS
`endif
`define FULL_BC
// SORT_CLUSTERS if not using all 32
//`define SORT_CLUSTERS
//`define BITONIC_SORT_OP <=
`define ALL_ROIS_SORTED
`ifdef SIMULATION
`define REPORT_PROGRESS
//`define REPORT_PROGRESS_2
`endif

parameter R2PAR = 16,
WAIT_STATES_PER_STAGE = 9,
MAX_CLUSTERS = 32, //  20, or 32
STAGES_PER_BUCKET = 4, // 2 or 5, or 4
NUM_BUCKETS = 2,
NUM_CHANNELS = 1,
//ITERATIONS_PER_BUCKET = 3, // 5 or 2, or 4
ITERATION_LOOPS = 4, // usually same as ITERATIONS_PER_BUCKET
NUM_WAIT_STATES = WAIT_STATES_PER_STAGE*STAGES_PER_BUCKET,
NUM_ROIS = 4,
DR2_NUM_BITS = 14,
PT2_NUM_BITS = 14,
ERROR_NUM_BITS = 5,
ENERGY_NUM_BITS = 12,
ETA_NUM_BITS = 8,
PHI_NUM_BITS = 7,
PT2INV_NUM_BITS = 25,
WAIT_STATES_NUM_BITS = 6,
MAC_WAIT_STATES = 4,
ROI_ETA_SIZE = 98,
ROI_PHI_SIZE = 64,
GBLOCK_ETA_SIZE = 49,
GBLOCK_PHI_SIZE = 32,
BC_IO_NUM_BITS = 32,
PHI_ADJUST_AMOUNT = 20,
RECT_VERTICAL_OFFSET = 10,
RECT_HORIZONTAL_OFFSET = 10,
MERGE_THRESHOLD = 4;

typedef struct packed {
	logic [4:0] error_bits;
	logic [ENERGY_NUM_BITS-1:0] ptt_x;
	logic [ENERGY_NUM_BITS-1:0] ptt_y;
	logic [ENERGY_NUM_BITS-1:0] ptt_z;
	logic [PHI_NUM_BITS-1:0] phi;
	logic [ETA_NUM_BITS-1:0] eta;
} cluster_type;

typedef struct packed {
	logic [4:0] error_bits;
	logic [ENERGY_NUM_BITS-1:0] ptt;
	logic [ENERGY_NUM_BITS-1:0] ptt_x;
	logic [ENERGY_NUM_BITS-1:0] ptt_y;
	logic [ENERGY_NUM_BITS-1:0] ptt_z;
	logic [PHI_NUM_BITS-1:0] phi;
	logic [ETA_NUM_BITS-1:0] eta;
} cluster_ptt_type;

typedef struct packed {
	logic [ENERGY_NUM_BITS:0] ptt;
	logic [PHI_NUM_BITS-1:0] phi;
	logic [ETA_NUM_BITS-1:0] eta;
} gblock_type;

typedef struct packed {
	logic [PHI_NUM_BITS-1:0] phi;
	logic [ETA_NUM_BITS-1:0] eta;
} center_type;

typedef struct packed {
	logic [($clog2(MAX_CLUSTERS))-1:0] cluster_num;
} cluster_num_type;
typedef struct packed {
	cluster_num_type [MAX_CLUSTERS-1:0] cluster_nums;
} cluster_nums_type;

typedef struct packed {
	cluster_type [MAX_CLUSTERS-1:0] cluster; 
} roi_cluster_type;

typedef struct packed {
	logic [DR2_NUM_BITS-1:0] deltar2;
} dr2_type;

typedef struct packed {
	logic [PT2INV_NUM_BITS-1:0] pt2inv;
} pt2inv_type;

typedef struct packed {
	logic [(PT2INV_NUM_BITS+DR2_NUM_BITS)-1:0] deltar2;
} dr2_pt2inv_type;
typedef struct packed {
	dr2_pt2inv_type dr2;
} dr2_pt2inv_up_type;

typedef struct packed {
//	dr2_type [MAX_CLUSTERS-1:0] dr2;
	pt2inv_type pt2inv;
	logic [($clog2(MAX_CLUSTERS)):0] merge_count;
} dr2_row_type;
typedef struct packed {
	dr2_type [MAX_CLUSTERS-1:0] dr2;
} dr2matrix_row_type;

typedef struct packed {
	dr2_type dr2;
} dr2_up_type;

typedef struct packed {
	dr2_pt2inv_type [MAX_CLUSTERS-1:0] dr2;
} dr2_column_type;

typedef struct packed {
	dr2_row_type [MAX_CLUSTERS-1:0] dr2_row;
} roi_dr2_type;
typedef struct packed {
	dr2matrix_row_type [MAX_CLUSTERS-1:0] dr2_row;
} dr2matrix_type;

typedef struct packed {
	roi_cluster_type clusters;
	roi_dr2_type roi_dr2;
	logic [MAX_CLUSTERS-1:0] disabled;
	logic [MAX_CLUSTERS-1:0] is_a_jet;
	logic [1:0] roi_num;
	logic [($clog2(MAX_CLUSTERS))-1:0] iteration;
} roi_type;
typedef struct packed {
	roi_type [NUM_ROIS-1:0] roi;
} bc_type;

parameter integer ROI_SIZE = $bits(roi_type);
parameter integer BC_SIZE = $bits(bc_type);
parameter integer CLUSTER_SIZE = $bits(cluster_type);

`endif
