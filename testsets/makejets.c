#include <stdio.h>
#include <stdlib.h>
#define JETS

int main(int argc, char **argv) {
	int ptt, eta, phi;
	ptt = 25;
	int num_tts = 500;
	int num_seeds = 30;
	if(argc > 1) {
		num_tts = atoi(argv[1]);
	}
	if(argc > 2) {
		num_seeds = atoi(argv[2]);
	}
	num_tts = (num_tts - 81);

	for (phi=32;phi<33;phi+=5) {
		for (eta=5;eta<81;eta+=5) {
			if(num_tts == 0) break;
			printf("15,%d,%d ",phi+2,eta+0); num_tts--; if(num_tts == 0) break;
			printf("15,%d,%d ",phi+1,eta+1); num_tts--; if(num_tts == 0) break;
			printf("15,%d,%d ",phi+0,eta+2); num_tts--; if(num_tts == 0) break;
			printf("15,%d,%d ",phi-1,eta+1); num_tts--; if(num_tts == 0) break;
			printf("15,%d,%d ",phi-2,eta+0); num_tts--; if(num_tts == 0) break;
			printf("15,%d,%d ",phi-1,eta-1); num_tts--; if(num_tts == 0) break;
			printf("15,%d,%d ",phi+0,eta-2); num_tts--; if(num_tts == 0) break;
			printf("15,%d,%d ",phi+1,eta-1); num_tts--; if(num_tts == 0) break;
			
			printf("10,%d,%d ",phi+2,eta+1); num_tts--; if(num_tts == 0) break;
			printf("10,%d,%d ",phi+2,eta-1); num_tts--; if(num_tts == 0) break;
			printf("10,%d,%d ",phi+1,eta+2); num_tts--; if(num_tts == 0) break;
//			printf("10,%d,%d ",phi-1,eta+2); num_tts--; if(num_tts == 0) break;
//			printf("10,%d,%d ",phi-2,eta+1); num_tts--; if(num_tts == 0) break;
//			printf("10,%d,%d ",phi-2,eta-1); num_tts--; if(num_tts == 0) break;
//			printf("10,%d,%d ",phi+1,eta-2); num_tts--; if(num_tts == 0) break;
//			printf("10,%d,%d ",phi-1,eta-2); num_tts--; if(num_tts == 0) break;

//			printf("(%d,%d) ",phi,eta);			
		}
	}
	ptt = 50;
	for (phi=32;phi<33;phi+=5) {
		for (eta=5;eta<81;eta+=5) {
			printf("%d,%d,%d ",ptt-1,phi-1,eta); num_tts--;
			printf("%d,%d,%d ",ptt-2,phi,eta-1); num_tts--;
			printf("%d,%d,%d ",ptt-3,phi+1,eta); num_tts--;
//			printf("%d,%d,%d ",ptt-4,phi,eta+1); num_tts--;
			printf("%d,%d,%d ",19,phi,eta+1); num_tts--;
			ptt += 10;
		}
	}
	ptt = 600;
	for (phi=32;phi<33;phi+=5) {
		for (eta=5;eta<81;eta+=5) {
			printf("%d,%d,%d ",ptt,phi,eta);
			ptt--;
		}
	}
	printf("20,32,94 ");
	printf("\n");
	return 0;
}
