#!/bin/tcsh
@ firsti = 6
@ maxi = 20
@ num_events = 10000

@ event = $firsti
while ($event < $maxi)
	echo event $event
	mkdir topotower_$event
	mktobs $event
	cp tobs.cpp topotower_$event
	cd topotower_$event
	gcc -o tobs tobs.cpp
	tobs $num_events
	cd ..
	@ event = $event + 1
end
