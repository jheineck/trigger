#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char**argv) {
	int event_num = 1;
	int line_num;
	char delimiter[] = " \n\r";
	char *token;
	int num_topotowers;
	int num_expected;
	int i;
	char event_fn[128];
	
	if(argc > 1) {
		event_num = atoi(argv[1]);
	}
	FILE *fd_to;
	FILE *fd_re;
	FILE *fd_ev;
	
	const int event_size = 4096;
	const int results_size = 512;
	
	char event[event_size];
	char results[results_size];
	
	fd_to = fopen("topotower_all.txt","r");
	if(fd_to == 0) return 1;
	fd_re = fopen("results.txt","r");
	if(fd_re == 0) {
		fclose(fd_to);
		return 2;
	}
	sprintf(event_fn,"topotower_%d/event.h",event_num);
	fd_ev = fopen(event_fn,"w");
	if(fd_ev == 0) {
		fclose(fd_to);
		fclose(fd_re);
		return 3;
	}
	for(line_num=1;line_num<event_num;line_num++) {
		fgets(event,event_size,fd_to);
		fgets(results,results_size,fd_re);
	}
	fgets(event,event_size,fd_to);
	fgets(results,results_size,fd_re);
	
	fclose(fd_to);
	fclose(fd_re);
	
	fprintf(fd_ev,"\tconst char *expect[] = {");
	num_expected = 0;
	token = strtok(results,delimiter);
	while(token) {
		if(token == NULL) break;
		if(*token == 10) break;
		num_expected++;
		fprintf(fd_ev,"\"%s,0\"",token);
		token = strtok(NULL,delimiter);
		if(token == NULL) break;
		if(*token != 10) {
			fprintf(fd_ev,",");
		}
	}
	fprintf(fd_ev,"};\n");
	
	fprintf(fd_ev,"\tconst char *tobs[] = {");
	num_topotowers = 0;
	token = strtok(event,delimiter);
	while(token) {
		if(token == NULL) break;
		if(*token == 10) break;
		num_topotowers++;
		fprintf(fd_ev,"\"%s\"",token);
		token = strtok(NULL,delimiter);
		if(token == NULL) break;
		if(*token != '\n') {
			fprintf(fd_ev,",");
		}
	}
	fprintf(fd_ev,"};\n");
	
	fprintf(fd_ev,"\tint arr[] = {");
	for(i=0;i<num_topotowers;i++) {
		fprintf(fd_ev,"%d",i);
		if(i==(num_topotowers-1)) {
			fprintf(fd_ev,"};\n");
		} else {
			fprintf(fd_ev,",");
		}
	}
	fprintf(fd_ev,"\tconst int num_expected = %d;\n",num_expected);
	fprintf(fd_ev,"\tconst int num_topotowers = %d;\n",num_topotowers);
	
	fclose(fd_ev);
}
