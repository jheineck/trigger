block 0 jet 0 expect '{flags:2,ptt:223,phi:62,eta:26}
block 0 jet 1 expect '{flags:2,ptt:89,phi:58,eta:55}
block 0 jet 2 expect '{flags:2,ptt:29,phi:7,eta:19}
block 0 jet 3 expect '{flags:2,ptt:66,phi:3,eta:61}
 3018 block 0 stage 0, replace qreg '{empty:1'b1,gblock:'{flags:0,ptt:0,phi:0,eta:0}} with preg '{flags:0,ptt:29,phi:7,eta:19}
 3018 block 0 stage 0, bumping '{flags:0,ptt:29,phi:7,eta:19}
 3022 block 0 stage 0, replace qreg '{empty:1'b0,gblock:'{flags:0,ptt:29,phi:7,eta:19}} with preg '{flags:0,ptt:47,phi:61,eta:26}
 3022 block 0 stage 0, bumping '{flags:0,ptt:47,phi:61,eta:26}
 3027 block 0 stage 0, replace qreg '{empty:1'b0,gblock:'{flags:0,ptt:47,phi:61,eta:26}} with preg '{flags:0,ptt:141,phi:62,eta:26}
 3027 block 0 stage 1, replace qreg '{empty:1'b1,gblock:'{flags:0,ptt:0,phi:0,eta:0}} with preg '{flags:0,ptt:29,phi:7,eta:19}
03027 block 0, rolloff '{flags:0,ptt:15,phi:60,eta:27} low Et
 3027 block 0 stage 1, bumping '{flags:0,ptt:29,phi:7,eta:19}
 3031 block 0 stage 1, replace qreg '{empty:1'b0,gblock:'{flags:0,ptt:29,phi:7,eta:19}} with preg '{flags:0,ptt:47,phi:61,eta:26}
 3031 block 0 stage 0, bumping '{flags:0,ptt:20,phi:0,eta:29}
 3035 block 0 stage 2, replace qreg '{empty:1'b1,gblock:'{flags:0,ptt:0,phi:0,eta:0}} with preg '{flags:0,ptt:29,phi:7,eta:19}
03035 block 0, rolloff '{flags:0,ptt:9,phi:53,eta:29} low Et
 3035 block 0 stage 1, bumping '{flags:0,ptt:20,phi:0,eta:29}
03039 block 0, rolloff '{flags:0,ptt:14,phi:54,eta:29} low Et
 3039 block 0 stage 2, bumping '{flags:0,ptt:20,phi:0,eta:29}
03043 block 0, rolloff '{flags:0,ptt:9,phi:5,eta:33} low Et
 3047 block 0 stage 3, replace qreg '{empty:1'b1,gblock:'{flags:0,ptt:0,phi:0,eta:0}} with preg '{flags:0,ptt:20,phi:0,eta:29}
 3047 block 0 stage 0, bumping '{flags:0,ptt:45,phi:8,eta:35}
03052 block 0, rolloff '{flags:0,ptt:17,phi:58,eta:35} low Et
 3052 block 0 stage 1, bumping '{flags:0,ptt:45,phi:8,eta:35}
 3056 block 0 stage 0, bumping '{flags:0,ptt:31,phi:55,eta:36}
 3056 block 0 stage 2, bumping '{flags:0,ptt:29,phi:7,eta:19}
 3060 block 0 stage 2, replace qreg '{empty:1'b0,gblock:'{flags:0,ptt:29,phi:7,eta:19}} with preg '{flags:0,ptt:45,phi:8,eta:35}
 3060 block 0 stage 0, bumping '{flags:0,ptt:38,phi:54,eta:37}
 3060 block 0 stage 1, bumping '{flags:0,ptt:31,phi:55,eta:36}
 3060 block 0 stage 3, bumping '{flags:0,ptt:20,phi:0,eta:29}
 3064 block 0 stage 3, replace qreg '{empty:1'b0,gblock:'{flags:0,ptt:20,phi:0,eta:29}} with preg '{flags:0,ptt:29,phi:7,eta:19}
03064 block 0, rolloff '{flags:0,ptt:16,phi:62,eta:37} low Et
 3064 block 0 stage 1, bumping '{flags:0,ptt:38,phi:54,eta:37}
 3064 block 0 stage 2, bumping '{flags:0,ptt:31,phi:55,eta:36}
 3068 block 0 stage 4, replace qreg '{empty:1'b1,gblock:'{flags:0,ptt:0,phi:0,eta:0}} with preg '{flags:0,ptt:20,phi:0,eta:29}
 3068 block 0 stage 0, bumping '{flags:0,ptt:50,phi:54,eta:38}
 3068 block 0 stage 2, bumping '{flags:0,ptt:38,phi:54,eta:37}
 3068 block 0 stage 3, bumping '{flags:0,ptt:29,phi:7,eta:19}
 3072 block 0 stage 3, replace qreg '{empty:1'b0,gblock:'{flags:0,ptt:29,phi:7,eta:19}} with preg '{flags:0,ptt:31,phi:55,eta:36}
 3072 block 0 stage 0, bumping '{flags:0,ptt:129,phi:55,eta:38}
 3072 block 0 stage 1, bumping '{flags:0,ptt:47,phi:61,eta:26}
 3072 block 0 stage 3, bumping '{flags:0,ptt:31,phi:55,eta:36}
 3072 block 0 stage 4, bumping '{flags:0,ptt:20,phi:0,eta:29}
 3077 block 0 stage 1, replace qreg '{empty:1'b0,gblock:'{flags:0,ptt:47,phi:61,eta:26}} with preg '{flags:0,ptt:50,phi:54,eta:38}
 3077 block 0 stage 3, replace qreg '{empty:1'b0,gblock:'{flags:0,ptt:31,phi:55,eta:36}} with preg '{flags:0,ptt:38,phi:54,eta:37}
 3077 block 0 stage 4, replace qreg '{empty:1'b0,gblock:'{flags:0,ptt:20,phi:0,eta:29}} with preg '{flags:0,ptt:29,phi:7,eta:19}
03077 block 0, rolloff '{flags:0,ptt:13,phi:57,eta:39} low Et
 3077 block 0 stage 1, bumping '{flags:0,ptt:50,phi:54,eta:38}
 3077 block 0 stage 2, bumping '{flags:0,ptt:45,phi:8,eta:35}
 3077 block 0 stage 4, bumping '{flags:0,ptt:29,phi:7,eta:19}
 3081 block 0 stage 1, replace qreg '{empty:1'b0,gblock:'{flags:0,ptt:50,phi:54,eta:38}} with preg '{flags:0,ptt:129,phi:55,eta:38}
 3081 block 0 stage 2, replace qreg '{empty:1'b0,gblock:'{flags:0,ptt:45,phi:8,eta:35}} with preg '{flags:0,ptt:47,phi:61,eta:26}
 3081 block 0 stage 4, replace qreg '{empty:1'b0,gblock:'{flags:0,ptt:29,phi:7,eta:19}} with preg '{flags:0,ptt:31,phi:55,eta:36}
 3081 block 0 stage 5, replace qreg '{empty:1'b1,gblock:'{flags:0,ptt:0,phi:0,eta:0}} with preg '{flags:0,ptt:20,phi:0,eta:29}
03081 block 0, rolloff '{flags:0,ptt:15,phi:60,eta:40} low Et
 3081 block 0 stage 2, bumping '{flags:0,ptt:47,phi:61,eta:26}
 3081 block 0 stage 3, bumping '{flags:0,ptt:38,phi:54,eta:37}
 3081 block 0 stage 5, bumping '{flags:0,ptt:20,phi:0,eta:29}
 3085 block 0 stage 2, replace qreg '{empty:1'b0,gblock:'{flags:0,ptt:47,phi:61,eta:26}} with preg '{flags:0,ptt:50,phi:54,eta:38}
 3085 block 0 stage 3, replace qreg '{empty:1'b0,gblock:'{flags:0,ptt:38,phi:54,eta:37}} with preg '{flags:0,ptt:45,phi:8,eta:35}
 3085 block 0 stage 5, replace qreg '{empty:1'b0,gblock:'{flags:0,ptt:20,phi:0,eta:29}} with preg '{flags:0,ptt:29,phi:7,eta:19}
03085 block 0, rolloff '{flags:0,ptt:9,phi:58,eta:41} low Et
 3085 block 0 stage 3, bumping '{flags:0,ptt:45,phi:8,eta:35}
 3085 block 0 stage 4, bumping '{flags:0,ptt:31,phi:55,eta:36}
 3089 block 0 stage 3, replace qreg '{empty:1'b0,gblock:'{flags:0,ptt:45,phi:8,eta:35}} with preg '{flags:0,ptt:47,phi:61,eta:26}
 3089 block 0 stage 4, replace qreg '{empty:1'b0,gblock:'{flags:0,ptt:31,phi:55,eta:36}} with preg '{flags:0,ptt:38,phi:54,eta:37}
 3089 block 0 stage 6, replace qreg '{empty:1'b1,gblock:'{flags:0,ptt:0,phi:0,eta:0}} with preg '{flags:0,ptt:20,phi:0,eta:29}
03089 block 0, rolloff '{flags:0,ptt:11,phi:60,eta:42} low Et
 3089 block 0 stage 4, bumping '{flags:0,ptt:38,phi:54,eta:37}
 3089 block 0 stage 5, bumping '{flags:0,ptt:29,phi:7,eta:19}
 3093 block 0 stage 4, replace qreg '{empty:1'b0,gblock:'{flags:0,ptt:38,phi:54,eta:37}} with preg '{flags:0,ptt:45,phi:8,eta:35}
 3093 block 0 stage 5, replace qreg '{empty:1'b0,gblock:'{flags:0,ptt:29,phi:7,eta:19}} with preg '{flags:0,ptt:31,phi:55,eta:36}
03093 block 0, rolloff '{flags:0,ptt:13,phi:1,eta:46} low Et
 3093 block 0 stage 5, bumping '{flags:0,ptt:31,phi:55,eta:36}
 3093 block 0 stage 6, bumping '{flags:0,ptt:20,phi:0,eta:29}
 3097 block 0 stage 5, replace qreg '{empty:1'b0,gblock:'{flags:0,ptt:31,phi:55,eta:36}} with preg '{flags:0,ptt:38,phi:54,eta:37}
 3097 block 0 stage 6, replace qreg '{empty:1'b0,gblock:'{flags:0,ptt:20,phi:0,eta:29}} with preg '{flags:0,ptt:29,phi:7,eta:19}
03097 block 0, rolloff '{flags:0,ptt:8,phi:0,eta:49} low Et
 3097 block 0 stage 6, bumping '{flags:0,ptt:29,phi:7,eta:19}
 3102 block 0 stage 6, replace qreg '{empty:1'b0,gblock:'{flags:0,ptt:29,phi:7,eta:19}} with preg '{flags:0,ptt:31,phi:55,eta:36}
 3102 block 0 stage 7, replace qreg '{empty:1'b1,gblock:'{flags:0,ptt:0,phi:0,eta:0}} with preg '{flags:0,ptt:20,phi:0,eta:29}
03102 block 0, rolloff '{flags:0,ptt:8,phi:11,eta:49} low Et
 3102 block 0 stage 7, bumping '{flags:0,ptt:20,phi:0,eta:29}
 3106 block 0 stage 7, replace qreg '{empty:1'b0,gblock:'{flags:0,ptt:20,phi:0,eta:29}} with preg '{flags:0,ptt:29,phi:7,eta:19}
 3106 block 0 stage 0, bumping '{flags:0,ptt:81,phi:55,eta:49}
 3110 block 0 stage 8, replace qreg '{empty:1'b1,gblock:'{flags:0,ptt:0,phi:0,eta:0}} with preg '{flags:0,ptt:20,phi:0,eta:29}
 3110 block 0 stage 0, bumping '{flags:0,ptt:29,phi:9,eta:52}
 3110 block 0 stage 1, bumping '{flags:0,ptt:81,phi:55,eta:49}
 3114 block 0 stage 0, bumping '{flags:0,ptt:29,phi:59,eta:53}
 3114 block 0 stage 1, bumping '{flags:0,ptt:29,phi:9,eta:52}
 3114 block 0 stage 2, bumping '{flags:0,ptt:50,phi:54,eta:38}
 3118 block 0 stage 2, replace qreg '{empty:1'b0,gblock:'{flags:0,ptt:50,phi:54,eta:38}} with preg '{flags:0,ptt:81,phi:55,eta:49}
03118 block 0, rolloff '{flags:0,ptt:8,phi:4,eta:54} low Et
 3118 block 0 stage 1, bumping '{flags:0,ptt:29,phi:59,eta:53}
 3118 block 0 stage 2, bumping '{flags:0,ptt:29,phi:9,eta:52}
 3118 block 0 stage 3, bumping '{flags:0,ptt:47,phi:61,eta:26}
 3122 block 0 stage 3, replace qreg '{empty:1'b0,gblock:'{flags:0,ptt:47,phi:61,eta:26}} with preg '{flags:0,ptt:50,phi:54,eta:38}
03122 block 0, rolloff '{flags:0,ptt:12,phi:54,eta:54} low Et
 3122 block 0 stage 2, bumping '{flags:0,ptt:29,phi:59,eta:53}
 3122 block 0 stage 3, bumping '{flags:0,ptt:29,phi:9,eta:52}
 3122 block 0 stage 4, bumping '{flags:0,ptt:45,phi:8,eta:35}
 3127 block 0 stage 4, replace qreg '{empty:1'b0,gblock:'{flags:0,ptt:45,phi:8,eta:35}} with preg '{flags:0,ptt:47,phi:61,eta:26}
 3127 block 0 stage 0, bumping '{flags:0,ptt:28,phi:58,eta:54}
 3127 block 0 stage 3, bumping '{flags:0,ptt:29,phi:59,eta:53}
 3127 block 0 stage 4, bumping '{flags:0,ptt:29,phi:9,eta:52}
 3127 block 0 stage 5, bumping '{flags:0,ptt:38,phi:54,eta:37}
 3131 block 0 stage 5, replace qreg '{empty:1'b0,gblock:'{flags:0,ptt:38,phi:54,eta:37}} with preg '{flags:0,ptt:45,phi:8,eta:35}
 3131 block 0 stage 0, bumping '{flags:0,ptt:32,phi:58,eta:55}
 3131 block 0 stage 1, bumping '{flags:0,ptt:28,phi:58,eta:54}
 3131 block 0 stage 4, bumping '{flags:0,ptt:29,phi:59,eta:53}
 3131 block 0 stage 5, bumping '{flags:0,ptt:29,phi:9,eta:52}
 3131 block 0 stage 6, bumping '{flags:0,ptt:31,phi:55,eta:36}
 3135 block 0 stage 6, replace qreg '{empty:1'b0,gblock:'{flags:0,ptt:31,phi:55,eta:36}} with preg '{flags:0,ptt:38,phi:54,eta:37}
03135 block 0, rolloff '{flags:0,ptt:10,phi:63,eta:57} low Et
 3135 block 0 stage 1, bumping '{flags:0,ptt:32,phi:58,eta:55}
 3135 block 0 stage 2, bumping '{flags:0,ptt:28,phi:58,eta:54}
 3135 block 0 stage 5, bumping '{flags:0,ptt:29,phi:59,eta:53}
 3135 block 0 stage 6, bumping '{flags:0,ptt:29,phi:9,eta:52}
 3135 block 0 stage 7, bumping '{flags:0,ptt:29,phi:7,eta:19}
 3139 block 0 stage 7, replace qreg '{empty:1'b0,gblock:'{flags:0,ptt:29,phi:7,eta:19}} with preg '{flags:0,ptt:31,phi:55,eta:36}
03139 block 0, rolloff '{flags:0,ptt:9,phi:7,eta:59} low Et
 3139 block 0 stage 2, bumping '{flags:0,ptt:32,phi:58,eta:55}
 3139 block 0 stage 3, bumping '{flags:0,ptt:28,phi:58,eta:54}
 3139 block 0 stage 6, bumping '{flags:0,ptt:29,phi:59,eta:53}
 3139 block 0 stage 7, bumping '{flags:0,ptt:29,phi:9,eta:52}
 3139 block 0 stage 8, bumping '{flags:0,ptt:20,phi:0,eta:29}
 3143 block 0 stage 8, replace qreg '{empty:1'b0,gblock:'{flags:0,ptt:20,phi:0,eta:29}} with preg '{flags:0,ptt:29,phi:7,eta:19}
03143 block 0, rolloff '{flags:0,ptt:17,phi:57,eta:59} low Et
 3143 block 0 stage 3, bumping '{flags:0,ptt:32,phi:58,eta:55}
 3143 block 0 stage 4, bumping '{flags:0,ptt:28,phi:58,eta:54}
 3143 block 0 stage 7, bumping '{flags:0,ptt:29,phi:59,eta:53}
 3143 block 0 stage 8, bumping '{flags:0,ptt:29,phi:9,eta:52}
 3147 block 0 stage 9, replace qreg '{empty:1'b1,gblock:'{flags:0,ptt:0,phi:0,eta:0}} with preg '{flags:0,ptt:20,phi:0,eta:29}
03147 block 0, rolloff '{flags:0,ptt:12,phi:60,eta:59} low Et
 3147 block 0 stage 4, bumping '{flags:0,ptt:32,phi:58,eta:55}
 3147 block 0 stage 5, bumping '{flags:0,ptt:28,phi:58,eta:54}
 3147 block 0 stage 8, bumping '{flags:0,ptt:29,phi:59,eta:53}
 3147 block 0 stage 9, bumping '{flags:0,ptt:20,phi:0,eta:29}
 3152 block 0 stage 9, replace qreg '{empty:1'b0,gblock:'{flags:0,ptt:20,phi:0,eta:29}} with preg '{flags:0,ptt:29,phi:9,eta:52}
 3152 block 0 stage 0, bumping '{flags:0,ptt:34,phi:55,eta:60}
 3152 block 0 stage 5, bumping '{flags:0,ptt:32,phi:58,eta:55}
 3152 block 0 stage 6, bumping '{flags:0,ptt:28,phi:58,eta:54}
 3152 block 0 stage 9, bumping '{flags:0,ptt:29,phi:59,eta:53}
 3156 block 0 stage 10, replace qreg '{empty:1'b1,gblock:'{flags:0,ptt:0,phi:0,eta:0}} with preg '{flags:0,ptt:20,phi:0,eta:29}
 3156 block 0 stage 0, bumping '{flags:0,ptt:21,phi:3,eta:61}
 3156 block 0 stage 1, bumping '{flags:0,ptt:34,phi:55,eta:60}
 3156 block 0 stage 6, bumping '{flags:0,ptt:32,phi:58,eta:55}
 3156 block 0 stage 7, bumping '{flags:0,ptt:28,phi:58,eta:54}
 3156 block 0 stage 10, bumping '{flags:0,ptt:20,phi:0,eta:29}
 3160 block 0 stage 10, replace qreg '{empty:1'b0,gblock:'{flags:0,ptt:20,phi:0,eta:29}} with preg '{flags:0,ptt:29,phi:59,eta:53}
03160 block 0, rolloff '{flags:0,ptt:10,phi:3,eta:62} low Et
 3160 block 0 stage 1, bumping '{flags:0,ptt:21,phi:3,eta:61}
 3160 block 0 stage 2, bumping '{flags:0,ptt:34,phi:55,eta:60}
 3160 block 0 stage 7, bumping '{flags:0,ptt:31,phi:55,eta:36}
 3160 block 0 stage 8, bumping '{flags:0,ptt:28,phi:58,eta:54}
 3164 block 0 stage 7, replace qreg '{empty:1'b0,gblock:'{flags:0,ptt:31,phi:55,eta:36}} with preg '{flags:0,ptt:32,phi:58,eta:55}
 3164 block 0 stage 11, replace qreg '{empty:1'b1,gblock:'{flags:0,ptt:0,phi:0,eta:0}} with preg '{flags:0,ptt:20,phi:0,eta:29}
03164 block 0, rolloff '{flags:0,ptt:19,phi:8,eta:62} low Et
 3164 block 0 stage 2, bumping '{flags:0,ptt:21,phi:3,eta:61}
 3164 block 0 stage 3, bumping '{flags:0,ptt:34,phi:55,eta:60}
 3164 block 0 stage 8, bumping '{flags:0,ptt:29,phi:7,eta:19}
 3164 block 0 stage 9, bumping '{flags:0,ptt:28,phi:58,eta:54}
 3168 block 0 stage 8, replace qreg '{empty:1'b0,gblock:'{flags:0,ptt:29,phi:7,eta:19}} with preg '{flags:0,ptt:31,phi:55,eta:36}
03168 block 0, rolloff '{flags:0,ptt:14,phi:52,eta:62} low Et
 3168 block 0 stage 3, bumping '{flags:0,ptt:21,phi:3,eta:61}
 3168 block 0 stage 4, bumping '{flags:0,ptt:34,phi:55,eta:60}
 3168 block 0 stage 9, bumping '{flags:0,ptt:29,phi:7,eta:19}
 3168 block 0 stage 10, bumping '{flags:0,ptt:28,phi:58,eta:54}
03172 block 0, rolloff '{flags:0,ptt:8,phi:1,eta:63} low Et
 3172 block 0 stage 4, bumping '{flags:0,ptt:21,phi:3,eta:61}
 3172 block 0 stage 5, bumping '{flags:0,ptt:34,phi:55,eta:60}
 3172 block 0 stage 10, bumping '{flags:0,ptt:29,phi:7,eta:19}
 3172 block 0 stage 11, bumping '{flags:0,ptt:20,phi:0,eta:29}
 3177 block 0 stage 11, replace qreg '{empty:1'b0,gblock:'{flags:0,ptt:20,phi:0,eta:29}} with preg '{flags:0,ptt:28,phi:58,eta:54}
03177 block 0, rolloff '{flags:0,ptt:12,phi:3,eta:63} low Et
 3177 block 0 stage 5, bumping '{flags:0,ptt:21,phi:3,eta:61}
 3177 block 0 stage 6, bumping '{flags:0,ptt:34,phi:55,eta:60}
 3177 block 0 stage 11, bumping '{flags:0,ptt:28,phi:58,eta:54}
 3181 block 0 stage 11, replace qreg '{empty:1'b0,gblock:'{flags:0,ptt:28,phi:58,eta:54}} with preg '{flags:0,ptt:29,phi:7,eta:19}
 3181 block 0 stage 12, replace qreg '{empty:1'b1,gblock:'{flags:0,ptt:0,phi:0,eta:0}} with preg '{flags:0,ptt:20,phi:0,eta:29}
03181 block 0, rolloff '{flags:0,ptt:12,phi:0,eta:64} low Et
 3181 block 0 stage 6, bumping '{flags:0,ptt:21,phi:3,eta:61}
 3181 block 0 stage 7, bumping '{flags:0,ptt:32,phi:58,eta:55}
 3181 block 0 stage 12, bumping '{flags:0,ptt:20,phi:0,eta:29}
 3185 block 0 stage 7, replace qreg '{empty:1'b0,gblock:'{flags:0,ptt:32,phi:58,eta:55}} with preg '{flags:0,ptt:34,phi:55,eta:60}
 3185 block 0 stage 12, replace qreg '{empty:1'b0,gblock:'{flags:0,ptt:20,phi:0,eta:29}} with preg '{flags:0,ptt:28,phi:58,eta:54}
03185 block 0, rolloff '{flags:0,ptt:15,phi:1,eta:64} low Et
 3185 block 0 stage 7, bumping '{flags:0,ptt:21,phi:3,eta:61}
 3185 block 0 stage 8, bumping '{flags:0,ptt:31,phi:55,eta:36}
 3189 block 0 stage 8, replace qreg '{empty:1'b0,gblock:'{flags:0,ptt:31,phi:55,eta:36}} with preg '{flags:0,ptt:32,phi:58,eta:55}
 3189 block 0 stage 13, replace qreg '{empty:1'b1,gblock:'{flags:0,ptt:0,phi:0,eta:0}} with preg '{flags:0,ptt:20,phi:0,eta:29}
03189 block 0, rolloff '{flags:0,ptt:13,phi:5,eta:67} low Et
 3189 block 0 stage 8, bumping '{flags:0,ptt:21,phi:3,eta:61}
 3189 block 0 stage 9, bumping '{flags:0,ptt:29,phi:9,eta:52}
 3193 block 0 stage 9, replace qreg '{empty:1'b0,gblock:'{flags:0,ptt:29,phi:9,eta:52}} with preg '{flags:0,ptt:31,phi:55,eta:36}
03193 block 0, rolloff '{flags:0,ptt:11,phi:10,eta:67} low Et
 3193 block 0 stage 9, bumping '{flags:0,ptt:21,phi:3,eta:61}
 3193 block 0 stage 10, bumping '{flags:0,ptt:29,phi:9,eta:52}
03197 block 0, rolloff '{flags:0,ptt:8,phi:59,eta:67} low Et
 3197 block 0 stage 10, bumping '{flags:0,ptt:21,phi:3,eta:61}
 3197 block 0 stage 11, bumping '{flags:0,ptt:29,phi:9,eta:52}
03202 block 0, rolloff '{flags:0,ptt:11,phi:61,eta:67} low Et
 3202 block 0 stage 11, bumping '{flags:0,ptt:21,phi:3,eta:61}
 3202 block 0 stage 12, bumping '{flags:0,ptt:28,phi:58,eta:54}
 3206 block 0 stage 12, replace qreg '{empty:1'b0,gblock:'{flags:0,ptt:28,phi:58,eta:54}} with preg '{flags:0,ptt:29,phi:9,eta:52}
03206 block 0, rolloff '{flags:0,ptt:8,phi:54,eta:79} low Et
 3206 block 0 stage 12, bumping '{flags:0,ptt:21,phi:3,eta:61}
 3206 block 0 stage 13, bumping '{flags:0,ptt:20,phi:0,eta:29}
 3210 block 0 stage 13, replace qreg '{empty:1'b0,gblock:'{flags:0,ptt:20,phi:0,eta:29}} with preg '{flags:0,ptt:28,phi:58,eta:54}
 3210 block 0 stage 13, bumping '{flags:0,ptt:21,phi:3,eta:61}
 3214 block 0 stage 14, replace qreg '{empty:1'b1,gblock:'{flags:0,ptt:0,phi:0,eta:0}} with preg '{flags:0,ptt:20,phi:0,eta:29}
 3214 block 0 stage 14, bumping '{flags:0,ptt:20,phi:0,eta:29}
 3218 block 0 stage 14, replace qreg '{empty:1'b0,gblock:'{flags:0,ptt:20,phi:0,eta:29}} with preg '{flags:0,ptt:21,phi:3,eta:61}
03218 block 0, rollout '{flags:0,ptt:20,phi:0,eta:29}
 3435 block 0 stage 0, storing '{flags:2,ptt:141,phi:62,eta:26}
 3439 block 0 stage 1, storing '{flags:0,ptt:129,phi:55,eta:38}
 3443 block 0 stage 2, storing '{flags:0,ptt:81,phi:55,eta:49}
 3447 block 0 stage 1, adding '{flags:0,ptt:50,phi:54,eta:38} to '{flags:0,ptt:129,phi:55,eta:38} = 179
 3452 block 0 stage 0, adding '{flags:2,ptt:47,phi:61,eta:26} to '{flags:2,ptt:141,phi:62,eta:26} = 188
 3456 block 0 stage 3, storing '{flags:0,ptt:45,phi:8,eta:35}
 3460 block 0 stage 1, adding '{flags:0,ptt:38,phi:54,eta:37} to '{flags:0,ptt:179,phi:55,eta:38} = 217
 3464 block 0 stage 4, storing '{flags:0,ptt:34,phi:55,eta:60}
 3468 block 0 stage 5, storing '{flags:2,ptt:32,phi:58,eta:55}
 3472 block 0 stage 1, adding '{flags:0,ptt:31,phi:55,eta:36} to '{flags:0,ptt:217,phi:55,eta:38} = 248
 3477 block 0 stage 5, adding '{flags:2,ptt:29,phi:59,eta:53} to '{flags:2,ptt:32,phi:58,eta:55} = 61
 3481 block 0 stage 6, storing '{flags:2,ptt:29,phi:7,eta:19}
 3485 block 0 stage 7, storing '{flags:0,ptt:29,phi:9,eta:52}
 3489 block 0 stage 5, adding '{flags:2,ptt:28,phi:58,eta:54} to '{flags:2,ptt:61,phi:58,eta:55} = 89
 3493 block 0 stage 8, storing '{flags:2,ptt:21,phi:3,eta:61}
 3497 block 0 stage 0, adding '{flags:2,ptt:20,phi:0,eta:29} to '{flags:2,ptt:188,phi:62,eta:26} = 208
 3502 block 0 stage 0, adding '{flags:3,ptt:15,phi:60,eta:27} to '{flags:2,ptt:208,phi:62,eta:26} = 223
 3506 block 0, nobody wants '{flags:1,ptt:9,phi:53,eta:29}
 3510 block 0, nobody wants '{flags:1,ptt:14,phi:54,eta:29}
 3514 block 0 stage 3, adding '{flags:3,ptt:9,phi:5,eta:33} to '{flags:0,ptt:45,phi:8,eta:35} = 54
 3518 block 0, nobody wants '{flags:1,ptt:17,phi:58,eta:35}
 3522 block 0, nobody wants '{flags:1,ptt:16,phi:62,eta:37}
 3527 block 0 stage 1, adding '{flags:3,ptt:13,phi:57,eta:39} to '{flags:0,ptt:248,phi:55,eta:38} = 261
 3531 block 0, nobody wants '{flags:1,ptt:15,phi:60,eta:40}
 3535 block 0, nobody wants '{flags:1,ptt:9,phi:58,eta:41}
 3539 block 0, nobody wants '{flags:1,ptt:11,phi:60,eta:42}
 3543 block 0, nobody wants '{flags:1,ptt:13,phi:1,eta:46}
 3547 block 0, nobody wants '{flags:1,ptt:8,phi:0,eta:49}
 3552 block 0 stage 7, adding '{flags:1,ptt:8,phi:11,eta:49} to '{flags:0,ptt:29,phi:9,eta:52} = 37
 3556 block 0, nobody wants '{flags:1,ptt:8,phi:4,eta:54}
 3560 block 0, nobody wants '{flags:1,ptt:12,phi:54,eta:54}
 3564 block 0, nobody wants '{flags:1,ptt:10,phi:63,eta:57}
 3568 block 0, nobody wants '{flags:1,ptt:9,phi:7,eta:59}
 3572 block 0 stage 4, adding '{flags:3,ptt:17,phi:57,eta:59} to '{flags:0,ptt:34,phi:55,eta:60} = 51
 3577 block 0, nobody wants '{flags:1,ptt:12,phi:60,eta:59}
 3581 block 0 stage 8, adding '{flags:3,ptt:10,phi:3,eta:62} to '{flags:2,ptt:21,phi:3,eta:61} = 31
 3585 block 0, nobody wants '{flags:1,ptt:19,phi:8,eta:62}
 3589 block 0 stage 4, adding '{flags:1,ptt:14,phi:52,eta:62} to '{flags:0,ptt:51,phi:55,eta:60} = 65
 3593 block 0 stage 8, adding '{flags:3,ptt:8,phi:1,eta:63} to '{flags:2,ptt:31,phi:3,eta:61} = 39
 3597 block 0 stage 8, adding '{flags:3,ptt:12,phi:3,eta:63} to '{flags:2,ptt:39,phi:3,eta:61} = 51
 3602 block 0, nobody wants '{flags:1,ptt:12,phi:0,eta:64}
 3606 block 0 stage 8, adding '{flags:3,ptt:15,phi:1,eta:64} to '{flags:2,ptt:51,phi:3,eta:61} = 66
 3610 block 0, nobody wants '{flags:1,ptt:13,phi:5,eta:67}
 3614 block 0, nobody wants '{flags:1,ptt:11,phi:10,eta:67}
 3618 block 0, nobody wants '{flags:1,ptt:8,phi:59,eta:67}
 3622 block 0, nobody wants '{flags:1,ptt:11,phi:61,eta:67}
 3627 block 0, nobody wants '{flags:1,ptt:8,phi:54,eta:79}
block 0, event 1
block 0, '{flags:2,ptt:223,phi:62,eta:26}
block 0, '{flags:2,ptt:89,phi:58,eta:55}
block 0, '{flags:2,ptt:29,phi:7,eta:19}
block 0, '{flags:2,ptt:66,phi:3,eta:61}
block 0, event 1, elapsed_time 654
08992 block 0, 31 rolloffs, 1 rollouts
block 0 jet 0 expect '{flags:2,ptt:223,phi:62,eta:26}
block 0 jet 1 expect '{flags:2,ptt:89,phi:58,eta:55}
block 0 jet 2 expect '{flags:2,ptt:29,phi:7,eta:19}
block 0 jet 3 expect '{flags:2,ptt:66,phi:3,eta:61}
