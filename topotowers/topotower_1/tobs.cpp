#include <stdio.h>
#include <stdlib.h>

#include "event.h"

int rand_comparison(const void *a, const void *b)
{
    (void)a; (void)b;

    return rand() % 2 ? +1 : -1;
}

void shuffle(void *base, size_t nmemb, size_t size)
{
    qsort(base, nmemb, size, rand_comparison);
}

int main (int argc, char **argv) {
	FILE *fd_out;
	int num_events = 10;
	if(argc > 1) {
		num_events = atoi(argv[1]);
	}
	
	fd_out = fopen("topotower.txt","w");
	srand(0);
	
	int i,j;
	for(i=0;i<num_expected;i++) {
		fprintf(fd_out,"%s ",expect[i]);
	}
	for(i=0;i<num_topotowers;i++) {
		fprintf(fd_out,"%s ",tobs[arr[i]]);
	}
	fprintf(fd_out,"\n");
	for(j=1;j<num_events;j++) {
		shuffle(arr,num_topotowers,sizeof(int));
		for(i=0;i<num_expected;i++) {
			fprintf(fd_out,"%s ",expect[i]);
		}
		for(i=0;i<num_topotowers;i++) {
			fprintf(fd_out,"%s ",tobs[arr[i]]);
		}
		fprintf(fd_out,"\n");
	}
	fclose(fd_out);
}
