#include <stdio.h>
#include <stdlib.h>

int rand_comparison(const void *a, const void *b)
{
    (void)a; (void)b;

    return rand() % 2 ? +1 : -1;
}

void shuffle(void *base, size_t nmemb, size_t size)
{
    qsort(base, nmemb, size, rand_comparison);
}

int main (int argc, char **argv) {
	int i,j;
	FILE *fd_out;
	int num_events = 10;
	if(argc > 1) {
		num_events = atoi(argv[1]);
	}
	int num_topotowers = 3;
	if(argc > 2) {
		num_topotowers = atoi(argv[2]);
	}
	int arr[100];
	int fact = 1;
	for(j=0;j<num_topotowers;j++) {
		arr[j] = j;
		fact = fact * (j + 1);
	}
	printf("total %d\n",fact);
	fd_out = fopen("tout.txt","w");
	srand(0);
	
	for(i=0;i<num_topotowers;i++) {
		fprintf(fd_out,"%d ",arr[i]);
	}
	fprintf(fd_out,"\n");
	for(j=1;j<num_events;j++) {
		shuffle(arr,num_topotowers,sizeof(int));
		for(i=0;i<num_topotowers;i++) {
			fprintf(fd_out,"%d ",arr[i]);
		}
		fprintf(fd_out,"\n");
	}
	fclose(fd_out);
}
