#include <stdio.h>
#include <stdlib.h>

#include "event.h"

int rand_comparison(const void *a, const void *b)
{
    (void)a; (void)b;

    return rand() % 2 ? +1 : -1;
}

void shuffle(void *base, size_t nmemb, size_t size)
{
    qsort(base, nmemb, size, rand_comparison);
}

int main (int argc, char **argv) {
	int num_events = 10;
	if(argc > 1) {
		num_events = atoi(argv[1]);
	}
//	const char *tobs[15] = {"1,50,0","1,50,0","77,50,72","75,50,92","81,50,52","39,44,20","40,44,10",
//	"64,50,95","66,50,75","70,50,55","74,50,90","76,50,70","80,50,50","99,50,20","100,50,10"};
//	int arr[15] = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14};
//	const char *exp[7] = {"100,50,10,0","99,50,20,0","231,50,52,0","219,50,72,0",
//	"213,50,92,0","40,44,10,0","39,44,20,0"};
	
	srand(0);
	
	int i,j;
	for(i=0;i<num_expected;i++) {
		printf("%s ",expect[i]);
	}
	for(i=0;i<num_topotowers;i++) {
		printf("%s ",tobs[arr[i]]);
	}
	printf("\n");
	for(j=1;j<num_events;j++) {
		shuffle(arr,num_topotowers,sizeof(int));
		for(i=0;i<num_expected;i++) {
			printf("%s ",expect[i]);
		}
		for(i=0;i<num_topotowers;i++) {
			printf("%s ",tobs[arr[i]]);
		}
		printf("\n");
	}
}
