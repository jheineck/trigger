`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06/10/2024 12:17:39 PM
// Design Name: 
// Module Name: generate_gblocks
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "defines.vh"

module generate_gblocks;
	integer i,j,k;
	gblock_type gblock_mem [MAX_GBLOCKS-1:0];
	integer dataset, dummyset;
	string sdataset;
	string prefix;
	string prefix2;
	gblock_type temp,ddin;
	integer fd;
	
	initial begin
		gblock_mem = '{default:0};
		temp = '{ptt:500,phi:PHI_SIZE-1,eta:ETA_SIZE-1};
		
        for(dataset=1;dataset<7;dataset++) begin
			dummyset = dataset;
			$display("using dataset %0d",dataset);
			sdataset.itoa(dataset);
			#1;
			$display({"Using Dataset_",sdataset});
			#1;
`ifdef RIVIERA
`ifdef POST_SYNTHESIS
			prefix = "../../../../../../";
`else
			prefix = "../../../../../";
`endif
`else
			prefix = "../../../../../";
`endif
			prefix2 = "gblocks/Datasets/Dataset_";
			$random(dummyset);
			fd = $fopen({prefix,prefix2,sdataset,"/gblocks.txt"},"w");
			
			for(i=0;i<MAX_GBLOCKS;i++) begin
				#1;
				ddin = '{ptt:$urandom_range(temp.ptt,0),phi:$urandom_range(temp.phi,0),eta:$urandom_range(temp.eta,0)};
				gblock_mem[i] = ddin;
				$fdisplay(fd,"%0d,%0d,%0d",ddin.ptt,ddin.phi,ddin.eta);
			end
			$writememh({prefix,prefix2,sdataset,"/gblocks.mem"},gblock_mem);
			$fclose(fd);
			$stop;
		end
	end
		
endmodule
