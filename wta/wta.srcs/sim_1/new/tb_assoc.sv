`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07/02/2024 09:34:41 AM
// Design Name: 
// Module Name: tb_assoc
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "defines.vh"

module tb_assoc;

	task doit(
		input phi_type phi0,
		input eta_type eta0,
		input phi_type phi1,
		input eta_type eta1);
		logic localish;
		begin
			localish = assoc(phi0,eta0,phi1,eta1);
			$display("(%0d,%0d) : (%0d,%0d) is %0d",phi0,eta0,phi1,eta1,localish);
		end
	endtask
	
	phi_type phi0, phi1;
	eta_type eta0, eta1;
	initial begin
		eta0 = 10; eta1 = 10;
		for(phi0=58;phi0<68;phi0++) begin
//			for(eta0=0;eta0<5;eta0++) begin
				for(phi1=58;phi1<68;phi1++) begin
//					for(eta1=0;eta1<9;eta1++) begin
						doit(phi0%64,eta0,phi1%64,eta1);
//					end
				end
//			end
		end
	end
endmodule
