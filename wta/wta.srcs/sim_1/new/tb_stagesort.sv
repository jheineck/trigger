`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06/24/2024 12:10:00 PM
// Design Name: 
// Module Name: tb_stagesort
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "defines.vh"

module tb_stagesort;

	parameter type OBJECT_TYPE = gblock_type;
	
	typedef struct packed {
		logic empty;
		OBJECT_TYPE gblock;
	} qtype;
	
	typedef struct packed {
		logic active;
		phi_type phi;
		eta_type eta;
	} ctype;
	
	parameter type QTYPE = qtype;
	parameter type CTYPE = ctype;
	
	logic reset;
	logic clk;
	logic load;
	logic unload;
	logic [NUM_STAGES-1:0] clear;
	logic [NUM_STAGES-1:0] accum;
	logic new_event;
	
	OBJECT_TYPE D [NUM_STAGES-1:0];
	OBJECT_TYPE P [NUM_STAGES-1:0];
	QTYPE Q [NUM_STAGES-1:0], Qnext [NUM_STAGES-1:0];
	CTYPE Cin [NUM_STAGES-1:0], Cout [NUM_STAGES-1:0];
	QTYPE expected_Q [NUM_STAGES-1:0];
	
	CTYPE creg [NUM_STAGES-1:0];
	OBJECT_TYPE preg [NUM_STAGES-1:0];
	logic exact_p [NUM_STAGES-1:0];
	logic exact_c [NUM_STAGES-1:0];
	qtype qreg [NUM_STAGES-1:0];

	OBJECT_TYPE resort_ddin [NUM_STAGES-1:0];
	logic [NUM_STAGES-1:0] busy;
	logic [NUM_STAGES-1:0] redundant;
	logic [NUM_STAGES-1:0] redundant_p;
	logic [NUM_STAGES-1:0] redundant_q;

	logic anybusy;
	integer testnum;
	logic business;
	always @(posedge clk or posedge reset) begin
		if(reset) begin
			business <= 0;
		end else begin
			business <= (|busy);
		end
	end
	assign anybusy = business | (|busy);
/*	
	assign anybusy = (|busy);
*/
	
	integer i;
	always @(posedge clk or posedge reset) begin
		if(reset) begin
		end else begin
			for(i=0;i<NUM_STAGES;i++) begin
				if(redundant[i]) begin
					$display("%05d test %0d, stage %0d: push off %p",$time,testnum,i,Q[i]);
				end
			end
		end
	end
	always @(posedge clk or posedge reset) begin
		if(reset) begin
		end else begin
			if(P[NUM_STAGES-1].ptt != 0) begin
				$display("%05d test %0d: roll off %p",$time,testnum,P[NUM_STAGES-1]);
			end
		end
	end
	
	genvar STAGE;
	generate
		for(STAGE=0;STAGE<NUM_STAGES;STAGE++) begin : stages
			stagesort #(.STAGE(STAGE),
						.OBJECT_TYPE(OBJECT_TYPE),
						.QTYPE(QTYPE),
						.CTYPE(CTYPE)) ss0 (
				.clk(clk),
				.reset(reset),
				.load(load),
				.unload(unload),
				.clear(clear[STAGE]),
				.accum(accum[STAGE]),
				.new_event(new_event),
				
				.D(D[STAGE]),
				.P(P[STAGE]),
				.Q(Q[STAGE]),
				.Qnext(Qnext[STAGE]),
				.resort_ddin(resort_ddin[STAGE]),
				
				.Cin(Cin[STAGE]),
				.Cout(Cout[STAGE]),
				
				.busy(busy[STAGE]),
				.redundant(redundant[STAGE])
			);
			if(STAGE != NUM_STAGES-1) begin
				assign Qnext[STAGE] = Q[STAGE+1];
			end
			if(STAGE != 0) begin
				assign D[STAGE] = P[STAGE-1];
				assign Cin[STAGE] = Cout[STAGE-1];
			end
			assign creg[STAGE] = stages[STAGE].ss0.creg;
			assign qreg[STAGE] = stages[STAGE].ss0.qreg;
			assign preg[STAGE] = stages[STAGE].ss0.preg;
			assign exact_c[STAGE] = stages[STAGE].ss0.exact_c;
			assign exact_p[STAGE] = stages[STAGE].ss0.exact_p;
			assign redundant_q[STAGE] = stages[STAGE].ss0.redundant_q;
			assign redundant_p[STAGE] = stages[STAGE].ss0.redundant_p;
		end
	endgenerate
/*
	assign creg[0] = {\\stages[0].ss0}/creg;
	assign creg[1] = {\\stages[1].ss0}/creg;
	assign creg[2] = {\\stages[2].ss0}/creg;
	assign preg[0] = {\\stages[0].ss0}/preg;
	assign preg[1] = {\\stages[1].ss0}/preg;
	assign preg[2] = {\\stages[2].ss0}/preg;
*/
	initial begin
		clk = 0;
	end
	
	always #5 clk = ~clk;
	
	integer wait_states;
	
	initial begin
		reset = 1;
		load = 0;
		unload = 0;
		clear = '{default:0};
		accum = '{default:0};
		new_event = 0;
		testnum = 0;
		wait_states = 4;
		
		D[0] = '{default:0};
		Cin[0] = '{default:0};
		Qnext[NUM_STAGES-1] = '{empty:1'b0,default:0};
		
		repeat (5) @(posedge clk);
		@(posedge clk) reset <= 0;
		#200;
		
		testnum = 1;
		$write("\ntest 1, should be no roll off\n");
		// load 3, verify they wind up in order
		@(posedge clk) new_event <= 1;
		@(posedge clk) new_event <= 0;
		@(posedge clk) load <= 1;
		@(posedge clk) D[0] <= '{500,30,30};
		@(posedge clk) D[0] <= '{600,40,40};
		@(posedge clk) D[0] <= '{550,50,50};
		@(posedge clk) D[0] <= '{default:0};
		wait(anybusy);
		wait(!anybusy);
		expected_Q = '{qtype'{0,gblock_type'{500,30,30}}, qtype'{0,gblock_type'{550,50,50}}, qtype'{0,gblock_type'{600,40,40}}};
		if(Q != expected_Q) begin
			$write("ERROR, expected %p\n       found    %p\n",expected_Q, Q);
		end
		
		testnum = 2;
		// roll a new one off the bottom; roll off 490
		$write("\ntest 2, should roll off 490\n");
		@(posedge clk) D[0] <= '{490,60,60};
		@(posedge clk) D[0] <= '{default:0};
		wait(anybusy);
		wait(!anybusy);
		expected_Q = '{qtype'{0,gblock_type'{500,30,30}}, qtype'{0,gblock_type'{550,50,50}}, qtype'{0,gblock_type'{600,40,40}}};
		if(Q != expected_Q) begin
			$write("ERROR, expected %p\n       found    %p\n",expected_Q, Q);
		end
		
		testnum = 3;
		// push a new one on the top; roll off 500
		$write("\ntest 3, should roll off 500\n");
		@(posedge clk) D[0] <= '{640,70,70};
		@(posedge clk) D[0] <= '{default:0};
		wait(anybusy);
		wait(!anybusy);
		expected_Q = '{qtype'{0,gblock_type'{550,50,50}}, qtype'{0,gblock_type'{600,40,40}}, qtype'{0,gblock_type'{640,70,70}}};
		if(Q != expected_Q) begin
			$write("ERROR, expected %p\n       found    %p\n",expected_Q, Q);
		end
		
		testnum = 4;
		// replace just the one on top; no roll off
		$write("\ntest 4, should be no roll off\n");
		@(posedge clk) D[0] <= '{650,70,70};
		@(posedge clk) D[0] <= '{default:0};
		wait(anybusy);
		wait(!anybusy);
		expected_Q = '{qtype'{0,gblock_type'{550,50,50}}, qtype'{0,gblock_type'{600,40,40}}, qtype'{0,gblock_type'{650,70,70}}};
		if(Q != expected_Q) begin
			$write("ERROR, expected %p\n       found    %p\n",expected_Q, Q);
		end
		
		testnum = 5;
		// replace just the middle one; no roll off
		$write("\ntest 5, should be no roll off\n");
		@(posedge clk) D[0] <= '{610,40,40};
		@(posedge clk) D[0] <= '{default:0};
		wait(anybusy);
		wait(!anybusy);
		expected_Q = '{qtype'{0,gblock_type'{550,50,50}}, qtype'{0,gblock_type'{610,40,40}}, qtype'{0,gblock_type'{650,70,70}}};
		if(Q != expected_Q) begin
			$write("ERROR, expected %p\n       found    %p\n",expected_Q, Q);
		end
		
		testnum = 6;
		// replace the middle one with highest ptt
		$write("\ntest 6, should be no roll off\n");
		@(posedge clk) D[0] <= '{660,40,40};
		@(posedge clk) D[0] <= '{default:0};
		wait(anybusy);
		wait(!anybusy);
		expected_Q = '{qtype'{0,gblock_type'{550,50,50}}, qtype'{0,gblock_type'{650,70,70}}, qtype'{0,gblock_type'{660,40,40}}};
		if(Q != expected_Q) begin
			$write("ERROR, expected %p\n       found    %p\n",expected_Q, Q);
		end
		
		testnum = 7;
		// push out the middle seed with one that takes its place
		$write("\ntest 7, should be no roll off, should push out 650\n");
		@(posedge clk) D[0] <= '{655,71,71};
		@(posedge clk) D[0] <= '{default:0};
		wait(anybusy);
		wait(!anybusy);
		expected_Q = '{qtype'{0,gblock_type'{550,50,50}}, qtype'{0,gblock_type'{655,71,71}}, qtype'{0,gblock_type'{660,40,40}}};
		if(Q != expected_Q) begin
			$write("ERROR, expected %p\n       found    %p\n",expected_Q, Q);
		end
		
		@(posedge clk) new_event <= 1;
		@(posedge clk) new_event <= 0;
		
		testnum = 8;
		$write("\ntest 8, push off 600, 601, 602, 603, 604, 605\n");
		// bunch of redundant, ascending ptt
		@(posedge clk) D[0] <= '{600,60,60};
		@(posedge clk) D[0] <= '{601,61,60};
		@(posedge clk) D[0] <= '{602,62,60};
		@(posedge clk) D[0] <= '{603,63,60};
		@(posedge clk) D[0] <= '{604,64,60};
		@(posedge clk) D[0] <= '{605,65,60};
		@(posedge clk) D[0] <= '{606,66,60};
		@(posedge clk) D[0] <= '{default:0};
		wait(anybusy);
		wait(!anybusy);
		expected_Q = '{qtype'{1,gblock_type'{0,0,0}}, qtype'{1,gblock_type'{0,0,0}}, qtype'{0,gblock_type'{606,66,60}}};
		if(Q != expected_Q) begin
			$write("ERROR, expected %p\n       found    %p\n",expected_Q, Q);
		end
		
		@(posedge clk) new_event <= 1;
		@(posedge clk) new_event <= 0;
		
		
		testnum = 9;
		// bunch of redundant, descending ptt
		$write("\ntest 9, push off 605, 604, 603, 602, 600\n");
		@(posedge clk) D[0] <= '{606,66,60}; // sticks at stage 0
		@(posedge clk) D[0] <= '{605,65,60}; // gets pushed out from stage 0
		@(posedge clk) D[0] <= '{604,64,60}; // gets pushed out from stage 0
		@(posedge clk) D[0] <= '{603,63,60}; // gets pushed out from stage 0
		@(posedge clk) D[0] <= '{602,62,60}; // gets pushed out from stage 0
		@(posedge clk) D[0] <= '{601,61,60}; // sticks at stage 1
		@(posedge clk) D[0] <= '{600,60,60}; // gets pushed out from stage 1
		@(posedge clk) D[0] <= '{default:0};
		wait(anybusy);
		wait(!anybusy);
		expected_Q = '{qtype'{1,gblock_type'{0,0,0}}, qtype'{0,gblock_type'{601,61,60}}, qtype'{0,gblock_type'{606,66,60}}};
		if(Q != expected_Q) begin
			$write("ERROR, expected %p\n       found    %p\n",expected_Q, Q);
		end
		
		testnum = 10;
		// roll off a bunch of constituents
		$write("\ntest 10, roll off\n");
		@(posedge clk) D[0] <= '{406,66,60}; // sticks at stage 0
		@(posedge clk) D[0] <= '{405,65,60}; // gets pushed out from stage 0
		@(posedge clk) D[0] <= '{404,64,60}; // gets pushed out from stage 0
		@(posedge clk) D[0] <= '{403,63,60}; // gets pushed out from stage 0
		@(posedge clk) D[0] <= '{402,62,60}; // gets pushed out from stage 0
		@(posedge clk) D[0] <= '{401,61,60}; // sticks at stage 1
		@(posedge clk) D[0] <= '{400,60,60}; // gets pushed out from stage 1
		@(posedge clk) D[0] <= '{default:0};
		wait(anybusy);
		wait(!anybusy);
		expected_Q = '{qtype'{1,gblock_type'{0,0,0}}, qtype'{0,gblock_type'{601,61,60}}, qtype'{0,gblock_type'{606,66,60}}};
		if(Q != expected_Q) begin
			$write("ERROR, expected %p\n       found    %p\n",expected_Q, Q);
		end
		testnum = 0;
		$display("end of tests");
		

		
		
		
		@(posedge clk) load <= 0;
		
		
		repeat (5) @(posedge clk);
		
		$stop;
		$finish;
	end
		
endmodule
