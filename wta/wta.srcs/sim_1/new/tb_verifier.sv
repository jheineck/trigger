`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 08/07/2024 11:49:20 AM
// Design Name: 
// Module Name: tb_verifier
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "defines.vh"

module tb_verifier;
	logic clk;
	logic reset;
	logic new_event, end_event;
	logic [NUM_BLOCKS-1:0] fsm_done;
	gblock_type ddin[NUM_BLOCKS-1:0];
	logic [NUM_BLOCKS-1:0] ddin_valid;
	
	verifier vf0 (
		.clk(clk),
		.reset(reset),
		.new_event(new_event),
		.end_event(end_event),
		.fsm_done(fsm_done),
		.ddin(ddin),
		.ddin_valid(ddin_valid)
	);
	
	localparam real CLOCK = 500.0 / CLOCK_FREQUENCY;
	initial begin
		clk = 0;
	end
	always #CLOCK clk = ~clk;
	
	// bunch crossing clock is independent of system clk
	localparam integer BC_CLOCK = BC_PERIOD / 2; // 1.2 us
	logic bc_clock;
	initial begin
		bc_clock = 0;
	end
	
	// generate bunch crossing start and end
	logic new_bunch, end_bunch;
	always #BC_CLOCK bc_clock = ~bc_clock;
	always @(posedge bc_clock or posedge reset) begin
		if(reset) begin
			end_bunch <= 0;
			new_bunch <= 0;
		end else begin
			new_bunch <= 1;
			#((2*CLOCK)+1) new_bunch <= 0;
			#(BC_PERIOD-20);
			end_bunch <= 1;
			#((2*CLOCK)+1) end_bunch <= 0;
		end
	end
	// synchronize bunch crossing start and end to system clk
	always @(posedge clk or posedge reset) begin
		if(reset) begin
			new_event <= 0;
			end_event <= 0;
		end else begin
			if(!new_event) begin
				new_event <= new_bunch;
			end else begin
				new_event <= 0;
			end
			if(!end_event) begin
				end_event <= end_bunch;
			end else begin
				end_event <= 0;
			end
		end
	end
	
	initial begin
		reset = 1;
		new_event = 0;
		end_event = 0;
		fsm_done = 0;
		ddin = '{default:0};
		ddin_valid = 0;
		
		// get past Versal power up
		repeat (5) @(posedge clk);
		@(posedge clk) reset <= 0;
		#200;
		wait(new_event);
		
		@(posedge clk) begin
			ddin_valid[2] <= 1;
		end
		@(posedge clk) ddin[2] <= '{0,100,30,10};
		@(posedge clk) ddin[2] <= '{0,110,30,11}; // push off the first one
		@(posedge clk) ddin[2] <= '{0,120,30,20}; // bump down
		@(posedge clk) ddin[2] <= '{0,90,30,40}; // bottom
		@(posedge clk) ddin[2] <= '{0,80,30,45}; // bottom
		@(posedge clk) ddin[2] <= '{0,115,30,42}; // bump & push
		
		@(posedge clk) begin
			ddin[2] <= '{default:0};
			ddin_valid[2] <= 0;
		end
		
		@(posedge clk) fsm_done[2] <= 1;
		@(posedge clk) fsm_done[2] <= 0;
		wait(end_event);
		repeat (5) @(posedge clk);
		$stop;
		$finish;
	end
		
endmodule
