`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Justin Heinecke 
// 
// Create Date: 06/10/2024 10:30:04 AM
// Design Name: wta
// Module Name: tb_wta
// Project Name: wta
// Target Devices: 
// Tool Versions: 
// Description: reads files with events and/or topotowers, sends them to the implementation,
//   gets the result reported jets
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "defines.vh"

module tb_wta;
	reg clk;
	reg reset;
	
	genvar BLOCK;
	
	// a new bunch crossing occurs every 1.2us.
	reg new_event;
	// an end bunch crossing occurs 20n before the next new_event
	reg end_event;
	// command flag to wta to commence with a bunch crossing
	reg fsm_go;
	// wta fsm is either loading or unloading topotowers
	logic [NUM_BLOCKS-1:0]fsm_busy;
	// wta fsm has finished with loading and unloading topotowers
	logic [NUM_BLOCKS-1:0]fsm_done;
	
	// output fsm is still processing
	logic [NUM_BLOCKS-1:0] out_fsm_busy;
	// output fsm has produced its jet list and is don
	logic [NUM_BLOCKS-1:0] out_fsm_done;

	// topotower input to each parallel block
	gblock_type [NUM_BLOCKS-1:0] ddin;
	// ddin is valid for each parallel block
	reg [NUM_BLOCKS-1:0] ddin_valid;
	// a jet as reported by wta_out
	gblock_type [NUM_BLOCKS-1:0] ddout;
	// wta_out jet is valid
	logic [NUM_BLOCKS-1:0]ddout_valid;
`ifdef RESORTABLE
	// indicates that the wta_in module should resort the seed list after every ptt accumulation
	logic resort_seeds;
	assign resort_seeds = (RESORT_SEEDS) ? 1'b1 : 1'b0;
`endif
	integer i,j,k;
	// storage for topotowers, per block, as read from topotowers.txt file
	gblock_type gblock_mem [NUM_BLOCKS-1:0] [MAX_GBLOCKS-1:0];
	// temps
	ptt_type ptt;
	phi_type phi;
	eta_type eta;
	
	// all of the following are just to implement reading events
	integer exp;
	integer dataset, dummyset;
	string sdataset;
	string prefix;
	string prefix2;
	string prefix3;
	string filename;
	string tevent;
	string sevent;
	integer linelen, eventlen;
	integer start_index, end_index,dum;
	integer fd, fdo;
	integer event_num;
	integer num_gblocks[NUM_BLOCKS-1:0];
	integer fields_read;
	logic [8*80:1] str;
	integer command;
	integer setnum;
	integer linenum;
	logic one_is_good;
	logic kicked_off;
	byte first_char;
	logic [THREAD_ID_SIZE-1:0] thread_id;
	assign thread_id = event_num[THREAD_ID_SIZE-1:0];
	
	// bit field indicating which core block a topotower is in
	logic [NUM_BLOCKS-1:0] block;
	// bit field indicating which slice or slices a topotower is in
	logic [NUM_BLOCKS-1:0] slice;
	
	// instantiate input wta, output wta_out, and inter-APP/APU FIFO
	wrapper_parallel #(.NUM_SORT_STAGES(NUM_SORT_STAGES),
						.NUM_STAGES(NUM_STAGES),
						.NUM_BLOCKS(NUM_BLOCKS),
						.OBJECT_TYPE(gblock_type)) wp_0 (
		.clk(clk),
		.reset(reset),
	`ifdef RESORTABLE
		.resort_seeds(resort_seeds),
	`endif
		.ddin(ddin),
		.ddin_valid(ddin_valid),
		.ddout(ddout),
		.ddout_valid(ddout_valid),
		.new_event(new_event),
		.end_event(end_event),
		.fsm_go(fsm_go),
		.fsm_busy(fsm_busy),
		.fsm_done(fsm_done),
		.out_fsm_busy(out_fsm_busy),
		.out_fsm_done(out_fsm_done)
	);
	
	verifier #(.NUM_SORT_STAGES(NUM_SORT_STAGES),
						.NUM_STAGES(NUM_STAGES),
						.NUM_BLOCKS(NUM_BLOCKS),
						.OBJECT_TYPE(gblock_type)) vf0 (
		.clk(clk),
		.reset(reset),
		.fdo(fdo),
		.new_event(new_event),
		.end_event(end_event),
		.fsm_done(fsm_done),
		.ddin(ddin),
		.ddin_valid(ddin_valid)
	);
	
	// number of expected results, per block
	integer ner [NUM_BLOCKS-1:0];
	// expected results, per block
	gblock_type er [NUM_BLOCKS-1:0][NUM_STAGES-1:0];
	
	generate
		// report the results
		// check the results against expected results, per block
		for(BLOCK=0;BLOCK<NUM_BLOCKS;BLOCK++) begin : wta_block
			gblock_type expected_results [NUM_STAGES-1:0];
			integer num_expected_results;
			// point into the testbench for thses
			assign num_expected_results = ner[BLOCK];
			assign expected_results = er[BLOCK];
	//		integer tevent_num;
			
			// how many result jets for this BLOCK
			integer count;
			// how many of the result jets were incorrect for this BLOCK
			integer error_count;
			// to hold off any report on a bc until the first one
			logic printed;
			always @(posedge clk or posedge reset) begin
				if(reset) begin
					printed <= 0;
					count <= 0;
					error_count <= 0;
					er[BLOCK] = '{default:0};
					ner[BLOCK] = 0;
				end else begin
					if(new_event) begin
						count <= 0;
						printed <= 0;
						error_count <= 0;
//						expected_results = '{default:0};//'{default:gblock_type'{0,0,0,0}};
//						num_expected_results = 0;
//						er[BLOCK] = '{default:0};
//						ner[BLOCK] = 0;
					end else if(end_event) begin
						if(count != num_expected_results) begin
							if(fdo != 0) begin
								$fdisplay(fdo,"ERROR block %0d, expected %0d, received %0d",BLOCK,num_expected_results,count);
							end
							$display("ERROR block %0d, expected %0d, received %0d",BLOCK,num_expected_results,count);
						end
						if(error_count != 0) begin
							if(fdo != 0) begin
								$fdisplay(fdo,"ERROR block %0d, %0d errors",BLOCK,error_count);
							end
							$display("ERROR block %0d, %0d errors",BLOCK,error_count);
						end
					end else begin
						if(ddout_valid[BLOCK]) begin
							if(!printed) begin
								printed <= 1;
								if(event_num != 0) begin
									if(fdo != 0) begin
										$fdisplay(fdo,"block %0d, event %0d",BLOCK,event_num);
									end
									$display("block %0d, event %0d",BLOCK,event_num);
								end
							end
							count <= count + 1;
							if(fdo != 0) begin
								$fdisplay(fdo,"block %0d, %p",BLOCK,ddout[BLOCK]);
							end
							$display("block %0d, %p",BLOCK,ddout[BLOCK]);
							if(ddout[BLOCK] != expected_results[count]) begin
								if(fdo != 0) begin
									$fdisplay(fdo,"ERROR, block %0d, expected %p, found %p",BLOCK,expected_results[count],ddout[BLOCK]);
								end
								$display("ERROR, block %0d, expected %p, found %p",BLOCK,expected_results[count],ddout[BLOCK]);
								error_count <= error_count + 1;
							end
						end
					end
				end
			end
			
			// paragraph to capture elapsed time of processing for this BLOCK
			real start_time;
			real end_time;
			real elapsed_time;
			always @(posedge fsm_go or posedge reset) begin
				if(reset) begin
					start_time= 0;
					end_time = 0;
					elapsed_time <= 0;
				end else begin
					start_time = $time;
					wait(out_fsm_done[BLOCK]);
					end_time = $time;
					wait(!out_fsm_done[BLOCK]);
					elapsed_time = end_time - start_time;
					$display("block %0d, event %0d, elapsed_time %0d",BLOCK,event_num,elapsed_time);
					if(elapsed_time > BC_PERIOD) begin
						$display("block %0d, event %0d, OVERRUN",BLOCK,event_num);
					end
					if(fdo != 0) begin
						$fdisplay(fdo,"block %0d, event %0d, elapsed_time %0d",BLOCK,event_num,elapsed_time);
						if(elapsed_time > BC_PERIOD) begin
							$fdisplay(fdo,"block %0d, event %0d, OVERRUN",BLOCK,event_num);
						end
					end
				end
			end
		end
	endgenerate
	
	localparam real CLOCK = 500.0 / CLOCK_FREQUENCY;
	initial begin
		clk = 0;
	end
	always #CLOCK clk = ~clk;
	
	// bunch crossing clock is independent of system clk
	localparam integer BC_CLOCK = BC_PERIOD / 2; // 1.2 us
	logic bc_clock;
	initial begin
		bc_clock = 0;
	end
	
	// generate bunch crossing start and end
	logic new_bunch, end_bunch;
	always #BC_CLOCK bc_clock = ~bc_clock;
	always @(posedge bc_clock or posedge reset) begin
		if(reset) begin
			end_bunch <= 0;
			new_bunch <= 0;
		end else begin
			new_bunch <= 1;
			#((2*CLOCK)+1) new_bunch <= 0;
			#(BC_PERIOD-20);
			end_bunch <= 1;
			#((2*CLOCK)+1) end_bunch <= 0;
		end
	end
	// synchronize bunch crossing start and end to system clk
	always @(posedge clk or posedge reset) begin
		if(reset) begin
			new_event <= 0;
			end_event <= 0;
		end else begin
			if(!new_event) begin
				new_event <= new_bunch;
			end else begin
				new_event <= 0;
			end
			if(!end_event) begin
				end_event <= end_bunch;
			end else begin
				end_event <= 0;
			end
		end
	end
	
	const real tt = (600.0 / CLOCK);
	logic times_up;
	integer times_up_timer;
	always @(posedge clk or posedge reset) begin
		if(reset) begin
			times_up <= 0;
			times_up_timer <= 0;
		end else if(new_event & (times_up_timer == 0)) begin
			times_up_timer <= tt;
		end else if(times_up_timer == 2) begin
			times_up <= 1;
			times_up_timer <= times_up_timer - 1;
		end else if(times_up_timer == 1) begin
			times_up <= 0;
			times_up_timer <= 0;
		end else if(times_up_timer != 0) begin
			times_up_timer <= times_up_timer - 1;
		end
	end
	logic times_up2;
	integer times_up2_timer;
	always @(posedge clk or posedge reset) begin
		if(reset) begin
			times_up2 <= 0;
			times_up2_timer <= 0;
		end else if(out_fsm_busy[2] & (times_up2_timer == 0)) begin
			times_up2_timer <= tt;
		end else if(times_up2_timer == 2) begin
			times_up2 <= 1;
			times_up2_timer <= times_up2_timer - 1;
		end else if(times_up2_timer == 1) begin
			times_up2 <= 0;
			times_up2_timer <= 0;
		end else if(times_up2_timer != 0) begin
			times_up2_timer <= times_up2_timer - 1;
		end
	end
	
	initial begin
		reset = 1;
		fsm_go = 0;//'{default:0};
//		new_event = 0;//'{default:0};
//		end_event = 0;//'{default:0};
		fd = 0;
		ddin_valid = 0;//'{default:0};
		gblock_mem = '{default:0};
		num_gblocks = '{default:0};
		ddin = '{default:0};
		fd = 0;
		ptt = 0; phi = 0; eta = 0;
		fields_read = 0;
//		num_expected_results = 0;
//		expected_results = '{default:gblock_type'{0,0,0}};
		command = 2;
		setnum = 0;
		linenum = 0;
		event_num = 0;
		one_is_good = 0;
		kicked_off = 0;
		
		// get past Versal power up
		repeat (5) @(posedge clk);
		@(negedge clk) reset <= 0;
		#200;
		
		fd = 99999;
		fdo = 0;
		dataset = 0;
        while (fd != 0) begin // for(dataset=1;dataset<7;dataset++) begin
        		
`ifdef RIVIERA
`ifdef POST_SYNTHESIS
			prefix = "../../../../../../";
`else
			prefix = "../../../../../";
`endif
`else
			prefix = "../../../../../";
`endif
        	if(command == 0) begin
				prefix3 = "Testset";
				prefix2 = {"gblocks/",prefix3,"s/",prefix3,"_"};
				filename = "gblocks.txt";
			end else if(command == 1) begin
				prefix3 = "topotower";
				prefix2 = {prefix3,"s/",prefix3,"_"};
				filename = {prefix3,".txt"};//"topotower.txt";
			end else if(command == 2) begin
				prefix3 = "testset";
				prefix2 = {prefix3,"s/",prefix3,"_"};
				filename = {prefix3,".txt"};//"topotower.txt";
			end else begin
				prefix3 = "Dataset";
				prefix2 = {"jets/",prefix3,"s_32/",prefix3,"_"};
				filename = "gblocks.txt";
			end
			if(setnum == 0) begin
				dataset++;
			end else begin
				dataset = setnum;
			end
			sdataset.itoa(dataset);
			$display({"Using ",prefix3,"_",sdataset});
//			$readmemh({prefix,prefix2,sdataset,"/gblocks.mem"},gblock_mem);
			if((command == 1) || (command == 2)) begin // obsolete, it's always 1
				event_num = 0;
				fd = $fopen({prefix,prefix2,sdataset,"/",filename},"r");
				fdo = $fopen({prefix,prefix2,sdataset,"/",filename,".out"},"w");
				if(fd == 0) break;
//				num_expected_results = 0;
//				expected_results = '{default:0};
				wait(new_event);
				
				while (!$feof(fd)) begin
//					if(event_num == 102) break;  // early stop for testing
					
					// the first event proceeds ipso quicko; subsequent events need to wait
					if(kicked_off) begin
						if(fsm_busy != 0) begin
							wait(fsm_busy == 0);
						end
					end
					if(kicked_off) begin // || (fsm_go && (event_num == 0))) begin
						wait(new_event);
						@(posedge clk) begin
//							new_event <= 1;
							for(block=0;block<NUM_BLOCKS;block++) begin
								ddin[block] <= 0;
							end
						end
//						@(posedge clk) new_event <= 0;
					end
					kicked_off = 0;
					// force linenum to cause it to use just the single event at that line number
					if(linenum != 0) begin
						$fseek(fd,0,0);
						for(i=0;i<linenum;i++) begin
							linelen = $fgets(tevent,fd);
						end
						event_num = linenum;
					end
/*
					for(block=0;block<NUM_BLOCKS;block++) begin
						case(block)
							0: wta_block[0].tevent_num = event_num;
							1: wta_block[1].tevent_num = event_num;
							2: wta_block[2].tevent_num = event_num;
							3: wta_block[3].tevent_num = event_num;
//							4: wta_block[4].tevent_num = event_num;
//							5: wta_block[5].tevent_num = event_num;
//							6: wta_block[6].tevent_num = event_num;
//							7: wta_block[7].tevent_num = event_num;
						endcase
					end
*/
					linelen = $fgets(tevent, fd);
					if(linelen > 4) begin // at least one topotower
						num_gblocks = '{default:0};
						for(block=0;block<NUM_BLOCKS;block++) begin
							er[block] = '{default:0};
							ner[block] = 0;
						end
						
						// search for a topotower of ptt,phi,eta or ptt,phi,eta,exp where exp is an expected result topotower
						start_index = 0;
						while(start_index < linelen) begin
							end_index = start_index;
							while((end_index < linelen) && (tevent[end_index] != 32) && (tevent[end_index] != 10) && (tevent[end_index] != 13)) begin
								dum = tevent[end_index];
								end_index++;
							end
							sevent = tevent.substr(start_index,end_index);
							start_index = end_index+1;
							
							// see if it's a comment
							first_char = sevent.getc(0);
							if(((first_char >= "0")) && ((first_char <= "9"))) begin
								
								// see if it's an expected result
								fields_read = $sscanf(sevent,"%d,%d,%d,%d",ptt,phi,eta,exp);
								if(fields_read == 4) begin
		//							if( (!resort_seeds && (exp == 1)) ||
		//								(resort_seeds && (exp == 2)) ||
									if( (exp == 1) || (exp == 0)) begin
										block = core_num(phi);
		//								expected_results[num_expected_results] = '{ptt,phi,eta};
		//								num_expected_results++;
/*
										case(block)
											1: begin wta_block[0].expected_results[wta_block[0].num_expected_results] = '{0,ptt,phi,eta}; wta_block[0].num_expected_results++; end
											2: begin wta_block[1].expected_results[wta_block[1].num_expected_results] = '{0,ptt,phi,eta}; wta_block[1].num_expected_results++; end
											4: begin wta_block[2].expected_results[wta_block[2].num_expected_results] = '{0,ptt,phi,eta}; wta_block[2].num_expected_results++; end
											8: begin wta_block[3].expected_results[wta_block[3].num_expected_results] = '{0,ptt,phi,eta}; wta_block[3].num_expected_results++; end
		//									4: begin wta_block[4].expected_results[wta_block[4].num_expected_results] = '{0,ptt,phi,eta}; wta_block[4].num_expected_results++; end
		//									5: begin wta_block[5].expected_results[wta_block[5].num_expected_results] = '{0,ptt,phi,eta}; wta_block[5].num_expected_results++; end
		//									6: begin wta_block[6].expected_results[wta_block[6].num_expected_results] = '{0,ptt,phi,eta}; wta_block[6].num_expected_results++; end
		//									7: begin wta_block[7].expected_results[wta_block[7].num_expected_results] = '{0,ptt,phi,eta}; wta_block[7].num_expected_results++; end
										endcase
*/
										// technically, block could exceed number of blocks, but Verilog is ok with that
										er[$clog2(block)][ner[$clog2(block)]] = '{flags:2,ptt:ptt,phi:phi,eta:eta};
`ifdef SIMULATION_DEBUG4
$fdisplay(fdo,"block %0d jet %0d expect %p",$clog2(block),ner[$clog2(block)], er[$clog2(block)][ner[$clog2(block)]]);
$display("block %0d jet %0d expect %p",$clog2(block),ner[$clog2(block)], er[$clog2(block)][ner[$clog2(block)]]);
`endif
										ner[$clog2(block)]++;
									end
								end else if(fields_read == 3) begin
									// not an expected result.  Use it if it's legal
									if((ptt > 0) && (phi < PHI_SIZE) && (eta < ETA_SIZE)
`ifdef SEEDS_ONLY
										&& (ptt >=  SEED_PTT_THRESHOLD)
`endif
									) begin
										// which blocks is it within the overlap + core regions, bit field
										slice = slice_num(phi);
										for(block=0;block<NUM_BLOCKS;block++) begin
											if(slice[block]) begin
												// save it for that block / those blocks
												if(num_gblocks[block] < MAX_GBLOCKS) begin
													gblock_mem[block][num_gblocks[block]] = '{ptt:ptt,phi:phi,eta:eta,default:0};
													num_gblocks[block]++;
												end
											end
										end
										ptt = 0; phi = 0; eta = 0;
									end
								end else begin
									if(fields_read == 1) begin
										start_index = linelen;
									end else begin
										continue;
									end
								end
							end
		
	
	
	//						$sscanf(sevent,"%d,%d,%d ",ptt,phi,eta);
						end
						
						// don't use the event if there were no valid topotowers
						kicked_off = 0;
						for(j=0;j<NUM_BLOCKS;j++) begin
							if(num_gblocks[j] != 0) begin
								kicked_off = 1;
							end
						end
						if(kicked_off) begin
`ifdef SIMULATION_DEBUG
$display("end of event %0d",event_num);
if(fdo != 0) begin
	$fdisplay(fdo,"end of event %0d",event_num);
end
`endif

							@(posedge clk) begin
								fsm_go <= 1;
							end
							for(j=0;j<MAX_GBLOCKS;j++) begin
								one_is_good = 0;
								// in parallel, feed all blocks with their respective topotowers
								@(posedge clk) begin
									for(block=0;block<NUM_BLOCKS;block++) begin
										if(j < num_gblocks[block]) begin
											one_is_good = 1'b1;
											ddin_valid[block] <= 1;
											ddin[block] <= gblock_mem[block][j];
										end else begin
											ddin_valid[block] <= 0;
											ddin[block] <= '{default:0};
										end
									end
								end
// Do all MAX_GBLOCKS regardless
								if(!one_is_good) break;
// Do all MAX_GBLOCKS regardless
							end
							@(posedge clk) begin
								ddin <= '{default:0};
								ddin_valid <= 0;
								fsm_go <= 0;
							end
							event_num++;
						end
					end
					// go get another event
				// no more events
				end
				wait(fsm_busy == 0);
				@(posedge clk) reset <= 1;
				@(posedge clk) reset <= 0;
				
//					repeat (5) @(posedge clk);
				$fclose(fd);
				if(fdo != 0) begin
					$fclose(fdo);
					fdo = 0;
				end
			end else begin
/*
				for(block=0;block<NUM_BLOCKS;block++) begin
					fd = $fopen({prefix,prefix2,sdataset,"/",filename},"r");
					if(fd == 0) break;
					num_gblocks = 0;
					case(block)
						0: begin wta_block[0].expected_results = '{default:0}; wta_block[0].num_expected_results = 0; end
						1: begin wta_block[1].expected_results = '{default:0}; wta_block[1].num_expected_results = 0; end
						2: begin wta_block[2].expected_results = '{default:0}; wta_block[2].num_expected_results = 0; end
//						3: begin wta_block[3].expected_results = '{default:0}; wta_block[3].num_expected_results = 0; end
//						4: begin wta_block[4].expected_results = '{default:0}; wta_block[4].num_expected_results = 0; end
//						5: begin wta_block[5].expected_results = '{default:0}; wta_block[5].num_expected_results = 0; end
//						6: begin wta_block[6].expected_results = '{default:0}; wta_block[6].num_expected_results = 0; end
//						7: begin wta_block[7].expected_results = '{default:0}; wta_block[7].num_expected_results = 0; end
					endcase
					while (!$feof(fd)) begin
						fields_read = $fscanf(fd,"%d,%d,%d,%d",ptt,phi,eta,exp);
						if(fields_read == 4) begin
							if( (!resort_seeds && (exp == 1)) ||
								(resort_seeds && (exp == 2)) ||
								(exp == 0)) begin
//								expected_results[num_expected_results] = '{ptt,phi,eta};
//								num_expected_results++;
								case(block)
									0: begin wta_block[0].expected_results[wta_block[0].num_expected_results] = '{ptt,phi,eta}; wta_block[0].num_expected_results++; end
									1: begin wta_block[1].expected_results[wta_block[1].num_expected_results] = '{ptt,phi,eta}; wta_block[1].num_expected_results++; end
									2: begin wta_block[2].expected_results[wta_block[2].num_expected_results] = '{ptt,phi,eta}; wta_block[2].num_expected_results++; end
//									3: begin wta_block[3].expected_results[wta_block[3].num_expected_results] = '{ptt,phi,eta}; wta_block[3].num_expected_results++; end
//									4: begin wta_block[4].expected_results[wta_block[4].num_expected_results] = '{ptt,phi,eta}; wta_block[4].num_expected_results++; end
//									5: begin wta_block[5].expected_results[wta_block[5].num_expected_results] = '{ptt,phi,eta}; wta_block[5].num_expected_results++; end
//									6: begin wta_block[6].expected_results[wta_block[6].num_expected_results] = '{ptt,phi,eta}; wta_block[6].num_expected_results++; end
//									7: begin wta_block[7].expected_results[wta_block[7].num_expected_results] = '{ptt,phi,eta}; wta_block[7].num_expected_results++; end
								endcase
							end
						end else begin
							if(fields_read == 3) begin
								if((ptt > 0)&&(phi < PHI_SIZE)&&(eta < ETA_SIZE)) begin
									gblock_mem[num_gblocks] = '{ptt:ptt,phi:phi,eta:eta};
									num_gblocks++;
									ptt = 0; phi = 0; eta = 0;
								end
							end else begin
								$fgets(str, fd);
							end
						end
					end
					$fclose(fd);
					wait(!fsm_busy[block]);
					@(posedge clk) begin new_event[block] <= 1'b1; ddin <= 0; end
					@(posedge clk) new_event[block] <= 0;
					@(posedge clk) fsm_go[block] <= 1;
					wait(fsm_busy);
					
					if(num_gblocks > MAX_GBLOCKS) begin
						num_gblocks = MAX_GBLOCKS;
					end
					
					for(j=0;j<num_gblocks;j++) begin
		//			for(j=0;j<13;j++) begin
						@(posedge clk) begin
							ddin_valid[block] <= 1;
							ddin <= gblock_mem[j];
						end
					end
					@(posedge clk) begin
						ddin <= '{default:0};
						ddin_valid[block] <= 1'b0;
						fsm_go[block] <= 0;
						end
//					wait(fsm_done);
//					@(posedge clk) end_event <= 1;
//					@(posedge clk) end_event <= 0;
					repeat (5) @(posedge clk);
					$stop;
				end
*/
			end
		end
		$finish;
	end
endmodule
