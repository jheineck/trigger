`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: Indiana University Department of Physics
// Engineer: Justin Heinecke
// 
// Create Date: 08/07/2024 10:50:25 AM
// Design Name: wta
// Module Name: verifier
// Project Name: wta
// Target Devices: 
// Tool Versions: 
// Description: An attempt to do the wta algorithm by brute force in parallel with the FPGA
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "defines.vh"

module verifier #(parameter integer NUM_STAGES = 10,
			parameter integer NUM_SORT_STAGES = 15,
			parameter integer NUM_BLOCKS = 4,
			parameter type OBJECT_TYPE = gblock_type) (
	input logic clk,
	input logic reset,
	input integer fdo,
	input logic new_event,
	input logic end_event,
	input logic [NUM_BLOCKS-1:0] fsm_done,
	input gblock_type [NUM_BLOCKS-1:0] ddin,
	input logic [NUM_BLOCKS-1:0] ddin_valid
	);

	gblock_type Qsort [NUM_BLOCKS-1:0][NUM_SORT_STAGES-1:0];
	logic Qsort_empty [NUM_BLOCKS-1:0][NUM_SORT_STAGES-1:0];
	gblock_type Qaccum [NUM_BLOCKS-1:0][NUM_STAGES-1:0];
	logic Qaccum_empty [NUM_BLOCKS-1:0][NUM_STAGES-1:0];
	gblock_type clist [NUM_BLOCKS-1:0][MAX_CONSTIT-1:0];
	gblock_type olist [NUM_BLOCKS-1:0][MAX_CONSTIT-1:0];
	gblock_type temp1, temp2;
	integer num_seeds [NUM_BLOCKS-1:0];
//	integer num_overflow [NUM_BLOCKS-1:0];
	integer constit_num;
	logic [NUM_BLOCKS-1:0] this_core;
	logic taken;

	integer block, stage, got_it_stage, stage2, p_clist[NUM_BLOCKS-1:0], i, bump_it_stage,p_olist[NUM_BLOCKS-1:0];
	logic got_it;
	always @(posedge clk or posedge reset) begin
		if(reset) begin
			for(block=0;block<NUM_BLOCKS;block++) begin
				for(stage=0;stage<NUM_SORT_STAGES;stage++) begin
					Qsort[block][stage] <= '{default:0};
					Qsort_empty[block][stage] <= 0;
				end
				for(stage=0;stage<NUM_STAGES;stage++) begin
					Qaccum[block][stage] <= '{default:0};
					Qaccum_empty[block][stage] <= 0;
				end
				for(i=0;i<MAX_CONSTIT;i++) begin
					clist[block][i] <= 0;
					olist[block][i] <= 0;
				end
				p_clist[block] <= 0;
				p_olist[block] <= 0;
				num_seeds[block] <= 0;
//				num_overflow[block] <= 0;
			end
			got_it <= 0;
			constit_num <= 0;
			this_core <= 0;
			taken <= 0;
		end else if(new_event) begin
			for(block=0;block<NUM_BLOCKS;block++) begin
				for(stage=0;stage<NUM_SORT_STAGES;stage++) begin
					Qsort[block][stage] <= '{default:0};
					Qsort_empty[block][stage] <= 1;
				end
				for(stage=0;stage<NUM_STAGES;stage++) begin
					Qaccum[block][stage] <= '{default:0};
					Qaccum_empty[block][stage] <= 1;
				end
				for(i=0;i<MAX_CONSTIT;i++) begin
					clist[block][i] <= 0;
					olist[block][i] <= 0;
				end
				p_clist[block] <= 0;
				p_olist[block] <= 0;
				num_seeds[block] <= 0;
//				num_overflow[block] <= 0;
			end
			got_it <= 0;
			constit_num <= 0;
			this_core <= 0;
			taken <= 0;
			
			
			
		end else if(|fsm_done) begin
			// one of the blocks has finished; compute my version
			for(block=0;block<NUM_BLOCKS;block++) begin
				if(fsm_done[block]) begin
//if(block==2) $stop;
`ifdef SIMULATION_DEBUG4
$display("%0d block %0d fsm_done v",$time,block);
`endif
					this_core = 1<<block;
					num_seeds[block] = 0;
//					num_overflow[block] = 0;
					// grab just the top 10
					for(stage=0;stage<NUM_SORT_STAGES;stage++) begin
						if(!Qsort_empty[block][stage]) begin
							taken = 0;
							for(stage2=0;stage2<=num_seeds[block];stage2++) begin
								if(Qaccum_empty[block][stage2]) begin
`ifdef SIMULATION_DEBUG4
$display("%0d block %0d stage %0d from %0d gets %p v",$time,block,stage2,stage,Qsort[block][stage]);
`endif
									Qaccum[block][stage2] = Qsort[block][stage];
									Qaccum_empty[block][stage2] = 0;
									num_seeds[block] = num_seeds[block] + 1;
									taken = 1;
									break;
								end else if(assoc(Qsort[block][stage].phi,Qsort[block][stage].eta,
													Qaccum[block][stage2].phi,Qaccum[block][stage2].eta)) begin
`ifdef SIMULATION_DEBUG4
$display("%0d block %0d stage %0d adding %p from %0d to %p = %0d v",$time,block,stage2,
							Qsort[block][stage],stage,Qaccum[block][stage2],
							Qaccum[block][stage2].ptt + Qsort[block][stage].ptt);
`endif
									Qaccum[block][stage2].ptt = Qaccum[block][stage2].ptt + Qsort[block][stage].ptt;
									taken = 1;
									break;
								end
							end
							if(!taken) begin
`ifdef SIMULATION_DEBUG4
$display("%0d block %0d from %0d overflow %p v",$time,block,stage,Qsort[block][stage]);
`endif
							end
						end else begin
							break;
						end
					end
					
`ifdef SIMULATION_DEBUG4
$display("%0d block %0d copy complete process olist v",$time,block);
`endif
					// go through the overflow list like constituents
					// NO! DAMN IT! GO THROUGH IT LIKE SEEDS, BECAUSE THEY ARE!
					for(constit_num=0;constit_num<p_olist[block];constit_num++) begin
						taken = 0;
						for(stage=0;stage<num_seeds[block];stage++) begin
							if(assoc(olist[block][constit_num].phi,olist[block][constit_num].eta,Qaccum[block][stage].phi,Qaccum[block][stage].eta)) begin
`ifdef SIMULATION_DEBUG4
$display("%0d block %0d stage %0d adding %p to %p = %0d v",$time,block,stage,olist[block][constit_num],Qaccum[block][stage],
															Qaccum[block][stage].ptt + olist[block][constit_num].ptt);
`endif
								Qaccum[block][stage].ptt = Qaccum[block][stage].ptt + olist[block][constit_num].ptt;
								taken = 1;
								break;
							end
						end


// try this
						if(!taken) begin
							if(num_seeds[block] < NUM_STAGES) begin // there's an empty one
								Qaccum[block][num_seeds[block]] = olist[block][constit_num];
								Qaccum_empty[block][num_seeds[block]] = 0;
`ifdef SIMULATION_DEBUG4
$display("%0d block %0d stage %0d from %0d gets %p v",$time,block,num_seeds[block],constit_num,olist[block][constit_num]);
`endif
								num_seeds[block] = num_seeds[block] + 1;
								taken = 1;
							end
						end
// end try




`ifdef SIMULATION_DEBUG4
						if(!taken) begin
$display("%0d block %0d from %0d no takers %p v",$time,block,constit_num,olist[block][constit_num]);
						end
`endif
					end
					// go through the real constituent list
`ifdef SIMULATION_DEBUG4
$display("%0d block %0d olist complete process plist v",$time,block);
`endif
					for(constit_num=0;constit_num<p_clist[block];constit_num++) begin
						taken = 0;
						for(stage=0;stage<num_seeds[block];stage++) begin
							if(assoc(clist[block][constit_num].phi,clist[block][constit_num].eta,Qaccum[block][stage].phi,Qaccum[block][stage].eta)) begin
`ifdef SIMULATION_DEBUG4
$display("%0d block %0d stage %0d adding %p to %p = %0d v",$time,block,stage,clist[block][constit_num],Qaccum[block][stage],
			Qaccum[block][stage].ptt + clist[block][constit_num].ptt);
`endif
								Qaccum[block][stage].ptt = Qaccum[block][stage].ptt + clist[block][constit_num].ptt;
								taken = 1;
								break;
							end
						end
`ifdef SIMULATION_DEBUG4
						if(!taken) begin
$display("%0d block %0d from %0d no takers %p v",$time,block,constit_num,clist[block][constit_num]);
						end
`endif
					end
`ifdef SIMULATION_DEBUG4
					// report the results
$display("%0d block %0d plist complete report results v",$time,block);
					for(stage=0;stage<num_seeds[block];stage++) begin
//$display("block %0d stage %0d core_num %0b this_core %0b",block,stage,core_num(Qaccum[block][stage].phi),this_core);
						if(core_num(Qaccum[block][stage].phi) == this_core) begin
							$display("%0d block %0d jet %0d expect %p v",$time,block,stage,Qaccum[block][stage]);
							if(fdo != 0) begin
								$fdisplay(fdo,"%0d block %0d jet %0d expect %p v",$time,block,stage,Qaccum[block][stage]);
							end
						end
					end
`endif
				end
			end
			
			
			
		end else begin
			for(block=0;block<NUM_BLOCKS;block++) begin
				if(ddin_valid[block]) begin
//if(block==2) $stop;
					got_it = 0;
//					skip_the_rest = 0;
					if((ddin[block].ptt != 0) && (ddin[block].ptt < SEED_PTT_THRESHOLD)) begin
`ifdef SIMULATION_DEBUG4
$display("%0d block %0d rolloff %p low Et v",$time,block,ddin[block]);
`endif
						if(p_clist[block] < (MAX_CONSTIT-2)) begin
							clist[block][p_clist[block]] = ddin[block];
							p_clist[block] = p_clist[block] + 1;
						end
						got_it = 1;
//						skip_the_rest = 1;
					end else for(stage=0;stage<NUM_SORT_STAGES;stage++) begin
						if(got_it) begin
							break;
						end else begin
							if((Qsort_empty[block][stage]) || (ddin[block].ptt > Qsort[block][stage].ptt)) begin // || ((ddin[block].ptt != 0) && Qsort_empty[block][stage] && (ddin[block].ptt == Qsort[block][stage].ptt))) begin
`ifdef SIMULATION_DEBUG4
$display("%0d block %0d stage %0d gets %p v",$time,block,stage,ddin[block]);
`endif
								Qsort[block][stage] <= ddin[block];
								Qsort_empty[block][stage] <= 0;
								got_it = 1;
								got_it_stage = stage;
								bump_it_stage = stage;
								// bump the rest?
								if(!Qsort_empty[block][stage]) begin // not empty before this
									// it wasn't empty
									// bump down to the first empty or off the bottom
									for(stage2=got_it_stage;stage2<NUM_SORT_STAGES;stage2++) begin
										bump_it_stage = stage2;
										// if necessary roll off to clist
										if(stage2==NUM_SORT_STAGES-1) begin
`ifdef SIMULATION_DEBUG4
$display("%0d block %0d stage %0d rolloff %p v",$time,block,stage,Qsort[block][stage2]);
`endif
											if(p_olist[block] < (MAX_CONSTIT-2)) begin
												olist[block][p_olist[block]] = Qsort[block][stage2];
												p_olist[block] = p_olist[block] + 1;
											end
										end else begin // was_it_the_last_stage
											// bump it down
`ifdef SIMULATION_DEBUG4
$display("%0d block %0d stage %0d bumping %p to %0d v",$time,block,stage,Qsort[block][stage2],stage2+1);
`endif
											Qsort[block][stage2+1] <= Qsort[block][stage2];
											Qsort_empty[block][stage2+1] <= 0;
												// was the destination empty, if so, we're done
											if(Qsort_empty[block][stage2+1]) begin
												break;
											end  // if_was_it_empty
										end // roll_off_or_bump
									end // for_loop_through_lower_stages
								end else begin // is empty before this, nothing to do here
									break;
								end
							end // done with this one
						end // try the next one
					end
					if(got_it == 0) begin
						if(ddin[block].ptt != 0) begin
`ifdef SIMULATION_DEBUG4
$display("%0d block %0d stage %0d rolloff %p no takers v",$time,block,stage,ddin[block]);
`endif
							if(p_olist[block] < (MAX_CONSTIT-2)) begin
								olist[block][p_olist[block]] <= ddin[block];
								p_olist[block] <= p_olist[block] + 1;
							end
						end
					end
				end
			end
		end
	end
endmodule
