`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Justin Heinecke
// 
// Create Date: 06/07/2024 12:49:08 PM
// Design Name: wta
// Module Name: clist_fifo
// Project Name: wta
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "defines.vh"
//`define DISTRIBUTED_RAM
//`define OUTPUT_REGISTER
//`define USE_IP_CLIST

module clist_fifo #(parameter type OBJECT_TYPE = gblock_type,
					integer NUM_SLOTS = 100) (
	input logic clk,
	input logic reset,
	input logic clear,
	input logic [THREAD_ID_SIZE-1:0] thread_id_in,
	input logic [THREAD_ID_SIZE-1:0] thread_id_out,
	input logic push_enable,
	input logic pop_enable,
	input OBJECT_TYPE jet,
	output OBJECT_TYPE o_jet,
	output logic last_one,
	output logic empty
);

	logic [NUM_THREADS-1:0] push_enable_by_thread;
	logic [NUM_THREADS-1:0] pop_enable_by_thread;
	assign push_enable_by_thread = push_enable<<thread_id_in;
	assign pop_enable_by_thread = pop_enable<<thread_id_out;
	OBJECT_TYPE write_datum;
	assign write_datum = jet;
	logic [NUM_THREADS-1:0] clear_by_thread;
	assign clear_by_thread = clear<<thread_id_in;
	
	logic [NUM_THREADS-1:0] empty_by_thread;
	logic [NUM_THREADS-1:0] last_one_by_thread;
	OBJECT_TYPE read_datum_by_thread[NUM_THREADS-1:0];
	assign o_jet = read_datum_by_thread[thread_id_out];
	assign empty = empty_by_thread[thread_id_out];
	assign last_one = last_one_by_thread[thread_id_out];
	
	genvar THREAD;
	generate
	for(THREAD=0;THREAD<NUM_THREADS;THREAD++) begin : THREADNUM
	
`ifdef USE_IP_CLIST
	reg [8:0] write_addr, read_addr;
	logic [8:0] num_stored;
`else
	reg [($clog2(NUM_SLOTS))-1:0] write_addr, read_addr;
	logic [($clog2(NUM_SLOTS))-1:0] num_stored;
`endif
`ifdef DISTRIBUTED_RAM
	(* ram_style = "distributed" *) OBJECT_TYPE [NUM_SLOTS-1:0] fifo_ram;
`endif
	logic push_enable_effective;
	assign push_enable_effective = push_enable_by_thread[THREAD] & (write_addr != NUM_SLOTS);
	
`ifdef DISTRIBUTED_RAM
`ifdef OUTPUT_REGISTER
	always @(posedge clk) begin
`else
	always_comb begin
`endif // OUTPUT_REGISTER
		read_datum_by_thread[THREAD] <= fifo_ram[read_addr];
	end
`endif // DISTRIBUTED_RAM
//	assign o_jet = read_datum;
	
	assign empty_by_thread[THREAD] = !(read_addr != write_addr);
	
	integer i;
	integer highwater;
	always @(posedge clk or posedge reset) begin
		if(reset) begin
			if(highwater != 0) begin
				$display("%m highwater %0d",highwater);
			end
			highwater <= 0;
			write_addr <= 0;
//			fifo_ram <= '{default:0};
		end else if(clear_by_thread[THREAD]) begin
			if(write_addr > highwater) begin
				highwater <= write_addr;
			end
			write_addr <= 0;
//			fifo_ram <= '{default:0};
		end else begin
			if(push_enable_by_thread[THREAD]) begin
				if(push_enable_effective) begin // ~0) begin
`ifdef DISTRIBUTED_RAM
					fifo_ram[write_addr] <= write_datum;
`endif
					write_addr <= write_addr + 1;
				end else begin
					$display("%m OVERFLOW");
					$stop;
				end
			end
		end
	end
	always @(posedge clk or posedge reset) begin
		if(reset) begin
			read_addr <= 0;
		end else if(clear_by_thread[THREAD]) begin
			read_addr <= 0;
		end else begin
			if(pop_enable_by_thread[THREAD]) begin
				if(read_addr != write_addr) begin
					read_addr <= read_addr + 1;
				end
			end
		end
	end
	
	always @(posedge clk or posedge reset) begin
		if(reset) begin
			num_stored <= 0;
		end else begin
			if(push_enable_effective & !pop_enable_by_thread[THREAD]) begin // (num_stored != ~0)) begin
				num_stored <= num_stored + 1;
			end else if(!push_enable_effective & pop_enable_by_thread[THREAD] & (num_stored != 0)) begin
				num_stored <= num_stored - 1;
			end
		end
	end
	assign last_one_by_thread[THREAD] = (num_stored == 1);
	
`ifndef DISTRIBUTED_RAM
`ifdef USE_IP_CLIST
	clist clist_0 (
		.clk(clk),
		.wea(push_enable_effective),
		.addra(write_addr),
		.addrb(read_addr),
		.dia(write_datum),
		.dob(read_datum_by_thread[THREAD]),
		.enab(1'b1),
		.rstab(1'b0)
	);
`else
	simple_dual_one_clock #(.OBJECT_TYPE(OBJECT_TYPE),
							.NUM_SLOTS(NUM_SLOTS)) sdoc (
		.clk(clk),
		.wea(push_enable_effective),
		.addra(write_addr),
		.addrb(read_addr),
		.dia(write_datum),
		.dob(read_datum_by_thread[THREAD])
	);
`endif
`endif
	end
	endgenerate
endmodule

`ifndef DISTRIBUTED_RAM
`ifndef USE_IP_CLIST
module simple_dual_one_clock #(parameter type OBJECT_TYPE = gblock_type,
					integer NUM_SLOTS = 100) (
	input clk,wea,
	input [($clog2(NUM_SLOTS))-1:0] addra, addrb,
	input OBJECT_TYPE dia,
	output OBJECT_TYPE dob
	);
	
	(* ram_style = "block" *) OBJECT_TYPE ram [NUM_SLOTS-1:0];
	
	always @(posedge clk) begin
		if(wea) begin
			ram[addra] <= dia;
		end
	end
	
`ifdef OUTPUT_REGISTER
	always @(posedge clk) begin
`else
	always_comb begin
`endif
		dob <= ram[addrb];
	end
endmodule
`endif
`endif
