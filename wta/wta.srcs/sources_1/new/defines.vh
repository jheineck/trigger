`ifndef WTA_DEFINED
`include "gep_pkg.vh"
parameter NUM_STAGES = 10,
NUM_OVERFLOW = 40,
NUM_SORT_STAGES = NUM_STAGES+NUM_OVERFLOW,
NUM_BLOCKS = 4,
THREAD_ID_SIZE = 2,
NUM_THREADS = 2**THREAD_ID_SIZE;
parameter J = 15;
parameter ENERGY_NUM_BITS = 12,
ETA_NUM_BITS = 8,
PHI_NUM_BITS = 7,
FLAGS_NUM_BITS = 5,
PHI_SIZE = 64,
ETA_SIZE = 98,
OVERLAP_SIZE = 8, // 4, // 4 each is 8
//NUM_GBLOCKS = 200,
FLAGS_CONSTIT_BIT = 0,
FLAGS_CORE_BIT = 1,
RESORT_SEEDS = 0,
SEED_PTT_THRESHOLD = 20,
MAX_GBLOCKS = 166,
MAIN_CLIST_SIZE = 512,
STAGE_CLIST_SIZE = 16,
CLOCK_FREQUENCY = 240,
BC_PERIOD = 6000,
MAX_CONSTIT = MAX_GBLOCKS;//300;
//`define UNUSED_FORMAT_GBLOCKS;
//`define UNUSED_FORMAT_TOPOTOWERS
//`define RESORTABLE
//`define SEEDS_ONLY
parameter integer SEGMENT_SIZE = PHI_SIZE / NUM_BLOCKS,
				TOP_SEGMENT_LEFT = PHI_SIZE - (SEGMENT_SIZE/2),
				TOP_OVERLAP_LEFT = TOP_SEGMENT_LEFT - OVERLAP_SIZE,
				TOP_SEGMENT_RIGHT = (SEGMENT_SIZE/2) - 1,
				TOP_OVERLAP_RIGHT = TOP_SEGMENT_RIGHT + OVERLAP_SIZE;
	
`define COMMON_CLISTS
typedef logic [FLAGS_NUM_BITS-1:0] flags_type;
typedef logic [THREAD_ID_SIZE-1:0] thread_id_type;
typedef logic [PHI_NUM_BITS-1:0] phi_type;
typedef logic [ETA_NUM_BITS-1:0] eta_type;
typedef logic [ENERGY_NUM_BITS-1:0] ptt_type;
typedef struct packed {
//	thread_id_type thread_id;
	flags_type flags;
	ptt_type ptt;
	phi_type phi;
	eta_type eta;
} gblock_type;

typedef enum logic [1:0] {
	STAGESORT_IDLE,
	STAGESORT_LOAD,
	STAGESORT_UNLOAD,
	STAGESORT_CONSTITUENT
} e_stagesort_input_mode;
//typedef enum logic {
//	STAGESORT_SORT,
//	STAGESORT_RESORT
//} e_stagesort_sort_mode;

function logic assoc (phi_type phi0, eta_type eta0, phi_type phi1, eta_type eta1);
	phi_type phi_dr, phi_dr_abs, phi_dr_adj;
	eta_type eta_dr, eta_dr_abs;//, eta_dr_adj;
	logic eta_in_bounds_1, eta_in_bounds_2, eta_in_bounds_3, eta_in_bounds_4;
	logic eta_in_bounds_m1, eta_in_bounds_m2, eta_in_bounds_m3, eta_in_bounds_m4;
	logic eta_in_bounds_p1, eta_in_bounds_p2, eta_in_bounds_p3, eta_in_bounds_p4;
	begin
		phi_dr = phi0 - phi1;
		if(phi_dr[PHI_NUM_BITS-1]) begin
			phi_dr_abs = (~(phi_dr)) + 1;
		end else begin
			phi_dr_abs = phi_dr;
		end
		if(phi_dr_abs > (PHI_SIZE/2)) begin
			phi_dr_adj = phi_dr_abs - ((phi_dr_abs - (PHI_SIZE/2))<<1);
		end else begin
			phi_dr_adj = phi_dr_abs;
		end
		eta_dr = eta0 - eta1;
		if(eta_dr[ETA_NUM_BITS-1]) begin
			eta_dr_abs = (~(eta_dr)) + 1;
		end else begin
			eta_dr_abs = eta_dr;
		end
		if(eta0 < 4) begin
			eta_in_bounds_m4 = 0;
		end else begin
			eta_in_bounds_m4 = 1;
		end
		if(eta0 < 3) begin
			eta_in_bounds_m3 = 0;
		end else begin
			eta_in_bounds_m3 = 1;
		end
		if(eta0 < 2) begin
			eta_in_bounds_m2 = 0;
		end else begin
			eta_in_bounds_m2 = 1;
		end
		if(eta0 < 1) begin
			eta_in_bounds_m1 = 0;
		end else begin
			eta_in_bounds_m1 = 1;
		end
		if(eta0 > (ETA_SIZE-5)) begin
			eta_in_bounds_p4 = 0;
		end else begin
			eta_in_bounds_p4 = 1;
		end
		if(eta0 > (ETA_SIZE-4)) begin
			eta_in_bounds_p3 = 0;
		end else begin
			eta_in_bounds_p3 = 1;
		end
		if(eta0 > (ETA_SIZE-3)) begin
			eta_in_bounds_p2 = 0;
		end else begin
			eta_in_bounds_p2 = 1;
		end
		if(eta0 > (ETA_SIZE-2)) begin
			eta_in_bounds_p1 = 0;
		end else begin
			eta_in_bounds_p1 = 1;
		end
		if(eta1 > eta0) begin
			eta_in_bounds_1 = eta_in_bounds_p1;
			eta_in_bounds_2 = eta_in_bounds_p2;
			eta_in_bounds_3 = eta_in_bounds_p3;
			eta_in_bounds_4 = eta_in_bounds_p4;
		end else begin
			eta_in_bounds_1 = eta_in_bounds_m1;
			eta_in_bounds_2 = eta_in_bounds_m2;
			eta_in_bounds_3 = eta_in_bounds_m3;
			eta_in_bounds_4 = eta_in_bounds_m4;
		end
		if( ((eta_dr_abs == 0) && ((phi_dr_adj == 1)||(phi_dr_adj == 2)||(phi_dr_adj == 3)||(phi_dr_adj == 4))) ||
			(eta_in_bounds_1) && ((eta_dr_abs == 1) && ((phi_dr_adj == 0)||(phi_dr_adj == 1)||(phi_dr_adj == 2)||(phi_dr_adj == 3))) ||
			(eta_in_bounds_2) && ((eta_dr_abs == 2) && ((phi_dr_adj == 0)||(phi_dr_adj == 1)||(phi_dr_adj == 2)||(phi_dr_adj == 3))) ||
			(eta_in_bounds_3) && ((eta_dr_abs == 3) && ((phi_dr_adj == 0)||(phi_dr_adj == 1)||(phi_dr_adj == 2))) ||
			(eta_in_bounds_4) && ((eta_dr_abs == 4) && (phi_dr_adj == 0)) ) begin
			assoc = 1;
		end else begin
			assoc = 0;
		end
	end
endfunction
	function [NUM_BLOCKS-1:0] core_num (input phi_type rphi);
		integer rblock;
		logic [NUM_BLOCKS-1:0] rval;
		// bits 3:0 indicate it's in the core
		begin
			if((rphi >= TOP_SEGMENT_LEFT) || (rphi <= TOP_SEGMENT_RIGHT)) begin
				rval[0] = 1'b1;
`ifdef SIMULATION_DEBUG2
$display("%0d is in block 0",rphi);
`endif
			end else begin
				rval[0] = 1'b0;
			end
			for(rblock=0;rblock<NUM_BLOCKS-2;rblock++) begin
				if((rphi >= (TOP_SEGMENT_RIGHT + (SEGMENT_SIZE*rblock) + 1)) && (rphi <= (TOP_SEGMENT_RIGHT + (SEGMENT_SIZE*(rblock+1))))) begin
					rval[rblock+1] = 1'b1;
`ifdef SIMULATION_DEBUG2
$display("%0d is in block %0d",rphi,rblock+1);
`endif
				end else begin
					rval[rblock+1] = 1'b0;
				end
			end
			if(NUM_BLOCKS>2) begin
				rblock = NUM_BLOCKS-2;
				if((rphi >= (TOP_SEGMENT_RIGHT + (SEGMENT_SIZE*rblock) + 1)) && (rphi <= (TOP_SEGMENT_LEFT - 1))) begin
					rval[rblock+1] = 1'b1;
`ifdef SIMULATION_DEBUG2
$display("%0d is in block %0d",rphi,rblock+1);
`endif
				end else begin
					rval[rblock+1] = 1'b0;
				end
			end
			return rval;
		end
	endfunction
	function [NUM_BLOCKS-1:0] slice_num (input phi_type rphi);
		integer rblock;
		logic [NUM_BLOCKS-1:0] rval;
		// bits 3:0 indicate it's in the overlap + core
		begin
			rblock = 0;
			if((rphi >= TOP_OVERLAP_LEFT) || (rphi <= TOP_OVERLAP_RIGHT)) begin
				rval[rblock] = 1'b1;
`ifdef SIMULATION_DEBUG2
$display("%0d is in slice %0d",rphi,rblock);
`endif
			end else begin
				rval[rblock] = 1'b0;
			end
			for(rblock=0;rblock<NUM_BLOCKS-2;rblock++) begin
				if((rphi >= (TOP_SEGMENT_RIGHT + (SEGMENT_SIZE*rblock) - OVERLAP_SIZE + 1)) && (rphi <= (TOP_SEGMENT_RIGHT + (SEGMENT_SIZE*(rblock+1))) + OVERLAP_SIZE)) begin
					rval[rblock+1] = 1'b1;
`ifdef SIMULATION_DEBUG2
$display("%0d is in slice %0d",rphi,rblock+1);
`endif
				end else begin
					rval[rblock+1] = 1'b0;
				end
			end
			if(NUM_BLOCKS > 2) begin
				rblock = NUM_BLOCKS-2;
				if((rphi >= (TOP_SEGMENT_RIGHT + (SEGMENT_SIZE*rblock) - OVERLAP_SIZE + 1)) && (rphi <= (TOP_SEGMENT_LEFT - 1 + OVERLAP_SIZE))) begin
					rval[rblock+1] = 1'b1;
`ifdef SIMULATION_DEBUG2
$display("%0d is in slice %0d",rphi,rblock+1);
`endif
				end else begin
					rval[rblock+1] = 1'b0;
				end
			end
			return rval;
		end
	endfunction
`define WTA_DEFINED
`endif
