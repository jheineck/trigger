`ifndef GEP_PKG_DEFINED
`define GEP_PKG_DEFINED

parameter integer
   LAR_A            = 0,
   LAR_B            = 1,
   TILE             = 2,
   EFEX_EG          = 3,
   EFEX_TAU         = 4,
   JFEX             = 5,
   FFEX             = 6,
   GFEX             = 7,
   MUCTPI           = 8,
   ZDC              = 9,
   LAR_PREPROC1     = 10,
   LAR_PREPROC2     = 11,
   TILECAL_PREPROC  = 12,
   TOPOC_PU         = 13,
   TAU1             = 14,
   TAU2             = 15,
   EGAMMA1          = 16,
   EGAMMA2          = 17,
   ANTIKT_CONEJET1  = 18,
   CONE_JET2        = 19,
   JET_TAG          = 20,
   CLUSTER_MET      = 21,
   TOWER_MET        = 22,
   MET              = 23,
   HYPOTHESIS       = 24,
   TIP              = 25;

const integer SOURCES_LAR_PREPROC1[0:15] = '{
	default:LAR_A};
	
const integer SOURCES_LAR_PREPROC2[0:15] = '{
	default:LAR_B};
	
const integer SOURCES_TILECAL_PREPROC[0:3] = '{
//	TILE,TILE,TILE};
	default:TILE};
	
parameter integer SOURCES_TOPOC_PU[0:2] = {
	TILECAL_PREPROC, LAR_PREPROC1, LAR_PREPROC2};
parameter integer SOURCES_TAU1[0:3] = {
	TILECAL_PREPROC, EFEX_TAU, EFEX_TAU, EFEX_TAU};
parameter integer SOURCES_TAU2[0:1] = {
	TAU1, TOPOC_PU};
parameter integer SOURCES_EGAMMA1[0:3] = {
	LAR_PREPROC1, EFEX_EG, EFEX_EG, EFEX_EG};
parameter integer SOURCES_EGAMMA2[0:1] = {
	EGAMMA1, TOPOC_PU};
parameter integer SOURCES_ANTIKT_CONEJET1[0:1] = {
	GFEX, TOPOC_PU};
parameter integer SOURCES_CONE_JET2[0:0] = {
	TOPOC_PU};
parameter integer SOURCES_JET_TAG[0:0] = {
	ANTIKT_CONEJET1};
parameter integer SOURCES_CLUSTER_MET[0:0] = {
	TOPOC_PU};
parameter integer SOURCES_TOWER_MET[0:0] = {
	TOPOC_PU};
parameter integer SOURCES_MET[0:1] = {
	TOPOC_PU, ANTIKT_CONEJET1};
parameter integer SOURCES_HYPOTHESIS[0:21] = {
	EFEX_EG, EFEX_EG, EFEX_EG, EFEX_TAU, EFEX_TAU,
	EFEX_TAU, FFEX, JFEX, JFEX, GFEX, MUCTPI,
	MUCTPI, MUCTPI, MUCTPI, ZDC, EGAMMA2, TAU2,
	JET_TAG, CLUSTER_MET, MET, CONE_JET2, TOWER_MET};
parameter integer SOURCES_TIP[0:0] = {
	HYPOTHESIS};
parameter integer SOURCES_TEMPLATE[0:3] = {
	LAR_A, TILE, EFEX_EG, JFEX};

parameter integer DATA_WIDTH[0:31 ] = '{
	LAR_A           : 128,
    LAR_B           : 128,
    TILE            : 128,
    EFEX_EG         : 128,
    EFEX_TAU        : 128,
    JFEX            : 128,
    FFEX            : 128,
    GFEX            : 128,
    MUCTPI          : 128,
    ZDC             : 128,
    LAR_PREPROC1    : 256,
    LAR_PREPROC2    : 256,
    TILECAL_PREPROC : 128,
    TOPOC_PU        : 256,
    TAU1            : 64,
    TAU2            : 64,
    EGAMMA1         : 64,
    EGAMMA2         : 64,
    ANTIKT_CONEJET1 : 64,
    CONE_JET2       : 64,
    JET_TAG         : 64,
    CLUSTER_MET     : 64,
    TOWER_MET       : 64,
    MET             : 64,
    HYPOTHESIS      : 256,
    TIP             : 128,
    default : 0};

parameter integer ADDR_WIDTH[0:31] = '{
    LAR_A            : 8,
    LAR_B            : 8,
    TILE             : 8,
    EFEX_EG          : 7,
    EFEX_TAU         : 7,
    JFEX             : 7,
    FFEX             : 8,
    GFEX             : 7,
    MUCTPI           : 7,
    ZDC              : 7,
    LAR_PREPROC1     : 8,
    LAR_PREPROC2     : 8,
    TILECAL_PREPROC  : 8,
    TOPOC_PU         : 8,
    TAU1             : 5,
    TAU2             : 5,
    EGAMMA1          : 5,
    EGAMMA2          : 5,
    ANTIKT_CONEJET1  : 5,
    CONE_JET2        : 5,
    JET_TAG          : 5,
    CLUSTER_MET      : 5,
    TOWER_MET        : 5,
    MET              : 5,
    HYPOTHESIS       : 4,
    TIP              : 3,
    default : 0};
    
parameter integer NUM_DST[0:31] = '{
    LAR_A           : 1,
    LAR_B           : 1,
    TILE            : 1,
    EFEX_EG         : 2,
    EFEX_TAU        : 2,
    JFEX            : 1,
    FFEX            : 1,
    GFEX            : 2,
    MUCTPI          : 1,
    ZDC             : 1,
    LAR_PREPROC1    : 2,
    LAR_PREPROC2    : 1,
    TILECAL_PREPROC : 2,
    TOPOC_PU        : 7,
    TAU1            : 1,
    TAU2            : 1,
    EGAMMA1         : 1,
    EGAMMA2         : 1,
    ANTIKT_CONEJET1 : 2,
    CONE_JET2       : 1,
    JET_TAG         : 1,
    CLUSTER_MET     : 1,
    TOWER_MET       : 1,
    MET             : 1,
    HYPOTHESIS      : 1,
    TIP             : 1,
    default : 0};

parameter integer USE_CLK[0:31] = '{
    LAR_A           : 240,
    LAR_B           : 240,
    TILE            : 240,
    EFEX_EG         : 240,
    EFEX_TAU        : 240,
    JFEX            : 240,
    FFEX            : 240,
    GFEX            : 240,
    MUCTPI          : 240,
    ZDC             : 240,
    LAR_PREPROC1    : 240,
    LAR_PREPROC2    : 240,
    TILECAL_PREPROC : 240,
    TOPOC_PU        : 240,
    TAU1            : 240,
    TAU2            : 240,
    EGAMMA1         : 240,
    EGAMMA2         : 240,
    ANTIKT_CONEJET1 : 240,
    CONE_JET2       : 240,
    JET_TAG         : 240,
    CLUSTER_MET     : 240,
    TOWER_MET       : 240,
    MET             : 240,
    HYPOTHESIS      : 240,
    TIP             : 240,
    default : 100};

parameter integer SLR_PLACEMENT[0:31] = '{
    LAR_A           : 3,
    LAR_B           : 3,
    TILE            : 3,
    EFEX_EG         : 2,
    EFEX_TAU        : 2,
    JFEX            : 2,
    FFEX            : 2,
    GFEX            : 2,
    MUCTPI          : 0,
    ZDC             : 0,
    LAR_PREPROC1    : 3,
    LAR_PREPROC2    : 3,
    TILECAL_PREPROC : 3,
    TOPOC_PU        : 3,
    TAU1            : 3,
    TAU2            : 2,
    EGAMMA1         : 3,
    EGAMMA2         : 2,
    ANTIKT_CONEJET1 : 1,
    CONE_JET2       : 1,
    JET_TAG         : 1,
    CLUSTER_MET     : 1,
    TOWER_MET       : 1,
    MET             : 1,
    HYPOTHESIS      : 0,
    TIP             : 0,
    default : 4};

parameter integer CHANNELS[0:31] = '{
    LAR_A    : 16,
    LAR_B    : 16,
    TILE     : 4,
    EFEX_EG  : 3,
    EFEX_TAU : 3,
    JFEX     : 2,
    FFEX     : 1,
    GFEX     : 1,
    MUCTPI   : 4,
    ZDC      : 1,
	default : 1};

parameter logic[31:0] APP_MODE[0:31] = '{
//parameter string APP_MODE[0:31] = '{
    LAR_PREPROC1    : "FIFO",
    LAR_PREPROC2    : "FIFO",
    TILECAL_PREPROC : "FIFO",
    TOPOC_PU        : "FIFO",
    TAU1            : "FIFO",
    TAU2            : "FIFO",
    EGAMMA1         : "FIFO",
    EGAMMA2         : "FIFO",
    ANTIKT_CONEJET1 : "FIFO",
    CONE_JET2       : "FIFO",
    JET_TAG         : "FIFO",
    CLUSTER_MET     : "FIFO",
    TOWER_MET       : "FIFO",
    MET             : "FIFO",
    HYPOTHESIS      : "FIFO",
    TIP             : "FIFO",
	default : "NONE"};

parameter logic[31:0] NAMES[0:31] = '{
    LAR_A           : 'h000001A4,
    LAR_B           : 'h000001A4,
    TILE            : 'h0000711e,
    EFEX_EG         : 'h0000efee,
    EFEX_TAU        : 'h0000efe7,
    JFEX            : 'h000001fe,
    FFEX            : 'h00000ffe,
    GFEX            : 'h000006fe,
    MUCTPI          : 'h00000c71,
    ZDC             : 'h00000000,
    LAR_PREPROC1    : 'h08070c1a,
    LAR_PREPROC2    : 'h08070c1a,
    TILECAL_PREPROC : 'h08070c7a,
    TOPOC_PU        : 'h00000000,
    TAU1            : 'h000007a1,
    TAU2            : 'h000007a2,
    EGAMMA1         : 'h0000e1e1,
    EGAMMA2         : 'h0000e1e2,
    ANTIKT_CONEJET1 : 'h00000000,
    CONE_JET2       : 'h00000000,
    JET_TAG         : 'h00000000,
    CLUSTER_MET     : 'h00000000,
    TOWER_MET       : 'h00000000,
    MET             : 'h00000000,
    HYPOTHESIS      : 'h00000000,
    TIP             : 'h00000000,
	default : 'h00000000};

//parameter integer MAX_ADDR_WIDTH = maximum(ADDR_WIDTH);
//parameter integer MAX_DATA_WIDTH = maximum(DATA_WIDTH);
function integer myaddrmax;
	integer i, ret_val;
	ret_val = ADDR_WIDTH[0];
	for(i=1;i<31;i++) begin
		ret_val = (ADDR_WIDTH[i] > ret_val) ? ADDR_WIDTH[i] : ret_val;
	end
	myaddrmax = ret_val;
endfunction
function integer mydatamax;
	integer i, ret_val;
	ret_val = DATA_WIDTH[0];
	for(i=1;i<31;i++) begin
		ret_val = (DATA_WIDTH[i] > ret_val) ? DATA_WIDTH[i] : ret_val;
	end
	mydatamax = ret_val;
endfunction
 function integer mymax(input integer v[],s);
	integer i, ret_val;
	ret_val = v[0];
	for(i=1;i<s;i++) begin
		ret_val = (v[i] > ret_val) ? v[i] : ret_val;
	end
	mymax = ret_val;
endfunction

// These work with Vivado
`ifdef VIVADO
//parameter integer MAX_ADDR_WIDTH = mymax(ADDR_WIDTH,32);
parameter integer MAX_ADDR_WIDTH = mymax(ADDR_WIDTH,$size(ADDR_WIDTH));
//parameter integer MAX_DATA_WIDTH = mymax(DATA_WIDTH,32);
parameter integer MAX_DATA_WIDTH = mymax(DATA_WIDTH,$size(DATA_WIDTH));
`endif
// These work with Riviera-PRO
`ifdef RIVIERA_PRO
parameter integer MAX_ADDR_WIDTH = myaddrmax();
parameter integer MAX_DATA_WIDTH = mydatamax();
`endif
parameter integer EvTID_WIDTH = 4;

typedef struct packed {
	logic wr_en;
	logic sync_set;
	logic[1:0] wr_EvTID;
	logic[MAX_ADDR_WIDTH-1:0] wr_addr;
	logic[MAX_DATA_WIDTH-1:0] wr_data;
	logic[3:0] data_type;
	logic[11:0] BCID;
} app_data;
parameter app_data APP_DATA_INIT = '{
	wr_en : 0,
	sync_set : 0,
	wr_EvTID  : '{default:0},
    wr_addr   : '{default:0},
    wr_data   : '{default:0},
    data_type : '{default:0},
    BCID      : '{default:0}};

typedef app_data app_data_v[];
typedef logic[EvTID_WIDTH-1:0] overflow_t;
typedef overflow_t overflow_v[];

typedef struct packed {
	overflow_t overflow;
} app_ctrl;
parameter app_ctrl APP_CTRL_INIT = '{
	overflow : '{default : 0}};
typedef app_ctrl app_ctrl_v[];
typedef app_ctrl_v tgen_ctrl_a[];

// APU TO APP
typedef struct packed {
	logic rd_EvTID_done;
	logic wr_en;
	logic wr_EvTID_done;
	logic[MAX_ADDR_WIDTH-1:0] wr_addr;
	logic[MAX_DATA_WIDTH-1:0] wr_data;
} apu_to_app;
parameter apu_to_app APU_TO_APP_INIT = '{
	rd_EvTID_done : 0,
	wr_en : 0,
	wr_EvTID_done : 0,
	wr_addr : '{default : 0},
	wr_data : '{default : 0}};
typedef apu_to_app apu_to_app_v[];

// Internal record type for the algorithm app
typedef struct packed {
    logic[MAX_DATA_WIDTH-1:0] rd_data_in;
    logic[MAX_ADDR_WIDTH-1:0] rd_addr;
    logic[MAX_ADDR_WIDTH-1:0] rd_topp;
    logic rd_sync_data;
    logic[1:0] rd_EvTID;
    logic rd_en;
} app_internal_t;
parameter app_internal_t APP_INTERNAL_INIT = '{
	rd_data_in : '{default:0},
    rd_addr : '{default:0},
    rd_EvTID : '{default:0},
    rd_topp : '{default:0},
    rd_sync_data : 0,
    rd_en : 0};
typedef app_internal_t app_internal_vector_t[];

// Control and data interconnection between app and apu
// APU CONTROL
typedef struct packed {
	logic rd_en;
	logic[MAX_ADDR_WIDTH-1:0] rd_addr;
} apu_ctrl;

parameter apu_ctrl APU_CTRL_INIT = '{
	rd_en : 0,
    rd_addr : '{default:0}};

typedef apu_ctrl apu_ctrl_v[];

// APU DATA
typedef struct packed {
    logic rd_last;  // Last data from this source
    logic fifo_empty;  // Fifo empty from this source
    logic fifo_valid;  // Fifo empty from this source
    logic[MAX_DATA_WIDTH-1:0] rd_data;	// Data from this source
} apu_data;

parameter apu_data APU_DATA_INIT =' {
	rd_last : 0,
    fifo_empty : 0,
    fifo_valid : 0,
    rd_data : '{default:0}};

typedef apu_data apu_data_v[];

typedef logic[MAX_ADDR_WIDTH-1:0] apu_topp_v[];

`endif