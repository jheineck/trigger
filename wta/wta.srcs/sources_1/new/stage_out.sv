`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Justin Heinecke
// 
// Create Date: 07/24/2024 10:27:58 AM
// Design Name: wta
// Module Name: stage_out
// Project Name: wta
// Target Devices: 
// Tool Versions: 
// Description: implements a single stage to accumulate energy from constituents
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "defines.vh"

module stage_out #(parameter integer NUM_STAGES = 10,
						integer BLOCK = 0,
						integer STAGE = 0,
						type OBJECT_TYPE = gblock_type) (
	input logic clk,
	input logic reset,
	input logic clear,
	input logic load,
	input logic constit,
	input logic assoc_winner,
	input logic empty_in,
	output logic empty_out,
	input OBJECT_TYPE D,
	output OBJECT_TYPE Q
    );
    
    OBJECT_TYPE qreg;
    assign Q = qreg;
    logic im_next;
    assign im_next = !empty_in;
    
    always @(posedge clk or posedge reset) begin
    	if(reset) begin
    		qreg <= 0;
    		empty_out <= 1;
    	end else if(clear) begin
    		qreg <= 0;
    		empty_out <= 1;
    	end else begin
    		if(assoc_winner) begin
`ifdef SIMULATION_DEBUG
$display("%5d block %0d stage %0d, adding %p to %p = %0d",$time,BLOCK,STAGE,D,qreg,qreg.ptt+D.ptt);
`endif
				qreg.ptt <= qreg.ptt + D.ptt;
			end else if(load & im_next & empty_out) begin
`ifdef SIMULATION_DEBUG
$display("%5d block %0d stage %0d, storing %p",$time,BLOCK,STAGE,D);
`endif
				qreg <= D;
				empty_out <= 0;
			end
    	end
    end
    				
    			
    		
endmodule
