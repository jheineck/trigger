`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: Indiana University Department of Physics
// Engineer: Justin Heinecke
// 
// Create Date: 05/17/2024 09:57:20 AM
// Design Name: stagesort
// Module Name: stageblock
// Project Name: wta
// Target Devices: 
// Tool Versions: 
// Description: instantiates NUM_STAGES stages of stagesort and stage constituent lists
//					instantiates main consituent list
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "defines.vh"
module stageblock #(parameter integer NUM_STAGES = 4,
					integer BLOCK = 0,
					type OBJECT_TYPE = gblock_type) (
	
    input logic clk,
    input logic reset,
	input e_stagesort_input_mode stagesort_input_mode,
    input OBJECT_TYPE ddin,
	input logic new_event,
	input logic end_event,
	output OBJECT_TYPE ddout,
	output logic disabled,
	output OBJECT_TYPE roll_out,
	output logic anybusy,
	output [1:0]empty,
	input [1:0]pop,
	output [1:0]last_one,
	output OBJECT_TYPE stage_clist_out
/*
	output [NUM_STAGES-1:0] redundant,
	output [NUM_STAGES:0] empty,
	input [NUM_STAGES:0] pop,
	output [NUM_STAGES:0] last_one,
	output OBJECT_TYPE stage_clist_out
*/
    );
	
	logic load, unload;//, constit;
	assign load = (stagesort_input_mode == STAGESORT_LOAD);
	assign unload = (stagesort_input_mode == STAGESORT_UNLOAD);
//	assign constit = (stagesort_input_mode == STAGESORT_CONSTITUENT);
	
	typedef struct packed {
		logic empty;
		OBJECT_TYPE gblock;
	} qtype;
	
//	typedef struct packed {
//		logic active;
//		phi_type phi;
//		eta_type eta;
//	} ctype;
	
	OBJECT_TYPE D[NUM_STAGES-1:0];
	OBJECT_TYPE P[NUM_STAGES-1:0];
	qtype Q[NUM_STAGES-1:0];
//	qtype Qout[NUM_STAGES-1:0];
	qtype Qnext[NUM_STAGES-1:0];
//	ctype Cin[NUM_STAGES-1:0], Cout[NUM_STAGES-1:0];
	
	logic load_effective;
	assign load_effective = load & ((ddin.phi < PHI_SIZE) && (ddin.eta < ETA_SIZE));
	logic [NUM_STAGES-1:0] busy;
	assign anybusy = |busy;
	
	logic ignore;
	assign ignore = (load_effective && (ddin.ptt != 0) && (ddin.ptt < SEED_PTT_THRESHOLD));
	
//	logic [NUM_STAGES-1:0] assoc_mask;
	always_comb begin
		D[0] <= ddin;
	end

	assign ddout = Q[0].gblock;
	assign disabled = Q[0].empty;
	assign roll_out = P[NUM_STAGES-1];
//	assign Cin[0] = '{default:0};

/*	
	OBJECT_TYPE o_jet [NUM_STAGES:0];
	function [($clog2(NUM_STAGES)):0] mylog2 (input [NUM_STAGES:0] arg);
		integer i;
		reg [NUM_STAGES:0] j;
		begin
			if(arg == 0) begin
				mylog2 = 0;
			end else begin
				j = arg - 1;
				mylog2 = 0;
				for(i=0;i<NUM_STAGES+1;i++) begin
					if(j[i]) mylog2 = i+1;
				end
			end
		end
	endfunction
	assign stage_clist_out = o_jet[mylog2(pop)];
*/
//	assign stage_clist_out = o_jet;

	OBJECT_TYPE o_jet[1:0];
	assign stage_clist_out = (pop[1]) ? o_jet[1] : o_jet[0];

genvar STAGE;
	generate
		for(STAGE=0;STAGE<NUM_STAGES;STAGE++) begin : stage
			stagesort #(.STAGE(STAGE),
						.BLOCK(BLOCK),
						.OBJECT_TYPE(OBJECT_TYPE),
						.QTYPE(qtype)//,
//						.CTYPE(ctype)
						) ss (
				.clk(clk),
				.reset(reset),
				.new_event(new_event),
				.busy(busy[STAGE]),
//				.redundant(redundant[STAGE]),
				.load(load_effective),
//				.constit(constit),
//				.update_in_place(update_in_place),
				.unload(unload),
				.Qnext(Qnext[STAGE]),
//				.Qout(Qout[STAGE]),
//				.Cin(Cin[STAGE]),
//				.Cout(Cout[STAGE]),
				.D(D[STAGE]),
				.P(P[STAGE]),
				.Q(Q[STAGE])
			);
			
			if(STAGE<(NUM_STAGES-1)) begin
				assign D[STAGE+1] = P[STAGE];
//				assign Qnext[STAGE] = Qout[STAGE+1];
				assign Qnext[STAGE] = Q[STAGE+1];
//				assign Cin[STAGE+1] = Cout[STAGE];
			end else begin
				assign Qnext[STAGE] = '{default:0};
			end

/*
`ifdef SIMULATION_DEBUG
			always @(posedge clk or posedge reset) begin
				if(reset) begin
				end else begin
					if(redundant[STAGE]) begin // && (stagesort_input_mode == STAGESORT_LOAD) && (!(Q[STAGE].push_off))) begin
						$display("%5d block %0d stage %0d, push_off %p",$time,BLOCK,STAGE,Q[STAGE].gblock);
					end
				end
			end
`endif
			clist_fifo #(.OBJECT_TYPE(OBJECT_TYPE),
						.NUM_SLOTS(STAGE_CLIST_SIZE)) clf (
				.clk(clk),
				.reset(reset),
				.clear(new_event),//(clear[STAGE]),
		.thread_id_in(2'b00), // for now
		.thread_id_out(2'b00), // for now
				.jet(Q[STAGE].gblock),
				.o_jet(o_jet[STAGE]),
				.push_enable(redundant[STAGE] & (!(Q[STAGE].empty))),
				.pop_enable(pop[STAGE]),
				.last_one(last_one[STAGE]),
				.empty(empty[STAGE])
			);
*/
				
		end
	endgenerate
	
	integer rollout_count;
	integer rolloff_count;
	// make the main constituent list look just like stage constituent lists
	OBJECT_TYPE lowet_olist_out;
	assign o_jet[1] = lowet_olist_out;
	logic olist_pop;
	assign olist_pop = pop[1];
	logic olist_last_one;
	assign last_one[1] = olist_last_one;
	logic olist_empty;
	assign empty[1] = olist_empty;

	clist_fifo #(.OBJECT_TYPE(OBJECT_TYPE),
				.NUM_SLOTS(MAIN_CLIST_SIZE)) olf_m (
		.clk(clk),
		.reset(reset),
		.clear(new_event),
		.thread_id_in(2'b00), // for now
		.thread_id_out(2'b00), // for now
		.jet(ddin),
		.o_jet(lowet_olist_out),
		.push_enable(ignore),//(roll_out.ptt != 0)),
		.pop_enable(olist_pop),
		.last_one(olist_last_one),
		.empty(olist_empty)
	);
	OBJECT_TYPE main_clist_out;
	assign o_jet[0] = main_clist_out;
	logic clist_pop;
	assign clist_pop = pop[0];
	logic clist_last_one;
	assign last_one[0] = clist_last_one;
	logic clist_empty;
	assign empty[0] = clist_empty;

	clist_fifo #(.OBJECT_TYPE(OBJECT_TYPE),
				.NUM_SLOTS(MAIN_CLIST_SIZE)) clf_m (
		.clk(clk),
		.reset(reset),
		.clear(new_event),
		.thread_id_in(2'b00), // for now
		.thread_id_out(2'b00), // for now
		.jet(roll_out),
		.o_jet(main_clist_out),
		.push_enable((roll_out.ptt != 0)),
		.pop_enable(clist_pop),
		.last_one(clist_last_one),
		.empty(clist_empty)
	);
`ifdef SIMULATION_DEBUG3
	always @(posedge clk or posedge reset) begin
		if(reset) begin
			rolloff_count <= 0;
			rollout_count <= 0;
		end else if(new_event) begin
			rolloff_count <= 0;
			rollout_count <= 0;
		end else if(end_event) begin
$display("%05d block %0d, %0d rolloffs, %0d rollouts",$time,BLOCK,rolloff_count,rollout_count);
		end else begin
			if(roll_out.ptt != 0) begin
$display("%05d block %0d, rollout %p",$time,BLOCK,roll_out);
				rollout_count <= rollout_count + 1;
			end
			if(ignore) begin
$display("%05d block %0d, rolloff %p low Et",$time,BLOCK,ddin);
				rolloff_count <= rolloff_count + 1;
			end
		end
	end
`endif
endmodule
