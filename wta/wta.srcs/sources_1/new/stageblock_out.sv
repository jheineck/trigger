`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Justin Heinecke
// 
// Create Date: 07/24/2024 10:06:34 AM
// Design Name: wta
// Module Name: stageblock_out
// Project Name: wta
// Target Devices: 
// Tool Versions: 
// Description: instantiates the stagesort_out stages
//  allows seeds to be loaded
//  identifies the winning seed[s] for a constituent
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "defines.vh"

module stageblock_out #(parameter integer NUM_STAGES = 10,
						integer BLOCK = 0,
						type OBJECT_TYPE = gblock_type) (
	input logic clk,
	input logic reset,
	input logic new_event,
	input logic load,
	input logic constit,
	output logic [($clog2(NUM_STAGES)):0] num_seeds,
	input logic [($clog2(NUM_STAGES)):0] stage_num,
	input OBJECT_TYPE jet,
	output OBJECT_TYPE ddout
	);
	
	genvar STAGE;
	
	OBJECT_TYPE Q [NUM_STAGES-1:0];
	assign ddout = Q[stage_num];
	const logic [FLAGS_NUM_BITS-1:0] core_flag = 1<<FLAGS_CORE_BIT;
	const OBJECT_TYPE core_template = '{flags:core_flag,default:0};
	logic [NUM_STAGES-1:0] empty_out;
	
	logic [NUM_BLOCKS-1:0] its_core;
	assign its_core = core_num(jet.phi);
	logic is_it_core;
	assign is_it_core = its_core[BLOCK];
	logic [NUM_STAGES-1:0] assoc_mask;
	logic [NUM_STAGES-1:0] assoc_winner;
	assign assoc_winner = ((~assoc_mask + 1) & assoc_mask);
	logic load_effective;
	assign load_effective = (load & !(|assoc_mask));
	
	generate
		for(STAGE=0;STAGE<NUM_STAGES;STAGE++) begin
			stage_out #(.NUM_STAGES(NUM_STAGES),
						.BLOCK(BLOCK),
						.STAGE(STAGE),
						.OBJECT_TYPE(OBJECT_TYPE)
			) sto_0 (
				.clk(clk),
				.reset(reset),
				.clear(new_event),
				.load(load_effective),
				.constit(constit),
				.empty_in((STAGE==0) ? 1'b0 : empty_out[STAGE-1]),
				.empty_out(empty_out[STAGE]),
				.assoc_winner(assoc_winner[STAGE]),
				.D((is_it_core) ? (jet | core_template) : jet),
				.Q(Q[STAGE])
			);
			assign assoc_mask[STAGE] = assoc(Q[STAGE].phi,Q[STAGE].eta,jet.phi,jet.eta);
		end
	endgenerate
	always @(posedge clk or posedge reset) begin
		if(reset) begin
			num_seeds <= 0;
		end else if(new_event) begin
			num_seeds <= 0;
		end else begin
			if(load & (assoc_winner==0)) begin
				if(num_seeds != NUM_STAGES) begin
					num_seeds <= num_seeds + 1;
				end
			end
		end
	end
`ifdef SIMULATION_DEBUG
always @(posedge clk) begin
	if(constit & (assoc_winner==0)) begin
		$display("%5d block %0d, nobody wants %p",$time,BLOCK,jet);
	end
end
`endif
endmodule

