`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: Indiana University Department of Physics
// Engineer: Justin Heinecke
// 
// Create Date: 05/10/2024 02:41:20 PM
// Design Name: wta
// Module Name: stagesort
// Project Name: wta
// Target Devices: 
// Tool Versions: 
// Description: Accepts new records of any type
//	type must include fields ptt,phi,eta
//  ends up with (max) 10 highest ptt seeds, all of which are not local to any others
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "defines.vh"	
module stagesort #(parameter integer STAGE = 0,
					integer BLOCK = 0,
					parameter type OBJECT_TYPE = gblock_type,
					type QTYPE = gblock_type//,
					) (
    input OBJECT_TYPE D,
    output OBJECT_TYPE P,
    output QTYPE Q,
	input QTYPE Qnext,
	input logic new_event,
	input logic unload,
	output logic busy,
    input logic clk,
    input logic reset,
    input logic load
    );
	
	OBJECT_TYPE preg;
	QTYPE qreg;
	logic ignore;
	assign ignore = (preg.ptt < SEED_PTT_THRESHOLD);
	
	// what to do about busy
	always @(posedge clk or posedge reset) begin
		if(reset) begin
			busy <= 0;
		end else begin
			// this stage is busy if anything is still propagating through it
			busy <= (P != 0);
		end
	end

	// what to do about preg
	always @(posedge clk or posedge reset) begin
		if(reset) begin
			preg <= 0;
		end else if(new_event) begin
			preg <= 0;
		end else begin
			if(load) begin
				preg <= D;
			end
		end
	end
			
	always_comb begin
		Q <= qreg;
	end
	
	// what to do about qreg
	always @(posedge clk or posedge reset) begin
		if(reset) begin
			qreg <= '{empty:1'b0, default:0};
		end else if(new_event) begin
			qreg <= '{empty:1'b1,default:0};
		end else if(unload) begin
			qreg <= Qnext;
		end else if(load) begin
			if(ignore) begin
			end else if((qreg.empty) || (preg.ptt > qreg.gblock.ptt)) begin // replace, even if empty
				qreg <= '{gblock:preg, empty:1'b0};
`ifdef SIMULATION_DEBUG
if(preg.ptt != 0)
$display("%5d block %0d stage %0d, replace qreg %p with preg %p",$time,BLOCK,STAGE,qreg,preg);
`endif
			end else begin // (ptt <=)
				// ignore it here
			end
		end
	end

	// what to do about P
	// P is an output of what's getting bumped down, either the new input or the old value
	//    or zero if something is getting pushed out to constituent list
	always_comb begin
		if((reset === 0) && (load === 1'b1)) begin
			if(ignore) begin
				P <= '{default:0};
			end else if(preg.ptt > qreg.gblock.ptt) begin
				P <= qreg.gblock;
`ifdef SIMULATION_DEBUG3
if(qreg.gblock.ptt != 0)
$display("%5d block %0d stage %0d, bumping %p",$time,BLOCK,STAGE,qreg.gblock);
`endif
			end else begin
				P <= preg;
`ifdef SIMULATION_DEBUG3
if(preg.ptt != 0)
$display("%5d block %0d stage %0d, bumping %p",$time,BLOCK,STAGE,preg);
`endif
			end
		end else begin
			P <= '{default:0};
		end
	end
			
endmodule
