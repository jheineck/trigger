`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Justin Heinecke
// 
// Create Date: 07/18/2024 11:13:07 AM
// Design Name: wta
// Module Name: wrapper_parallel
// Project Name: wta
// Target Devices: 
// Tool Versions: 
// Description: outermost block for both input sorting, and output accumulating,
//   as well as the inter-APP/APU FIFO
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "defines.vh"

// outermost block
// all inputs and outputs will be assigned to package pins for implementation
module wrapper_parallel #(parameter integer NUM_STAGES = 10,
			parameter integer NUM_SORT_STAGES = 15,
			parameter integer NUM_BLOCKS = 4,
			parameter type OBJECT_TYPE = gblock_type) (
	input logic clk,
	input logic reset,
	input OBJECT_TYPE [NUM_BLOCKS-1:0] ddin,
	input logic [NUM_BLOCKS-1:0] ddin_valid,
	output OBJECT_TYPE [NUM_BLOCKS-1:0] ddout,
	output logic [NUM_BLOCKS-1:0] ddout_valid,
	input logic new_event,
	input logic end_event,
	input logic fsm_go,
	output logic [NUM_BLOCKS-1:0] fsm_busy,
	output logic [NUM_BLOCKS-1:0] fsm_done,
	output logic [NUM_BLOCKS-1:0] out_fsm_busy,
	output logic [NUM_BLOCKS-1:0] out_fsm_done
    );
    
    // some flags to facilitate emptying the inter-APP/APU FIFO
    logic [NUM_BLOCKS-1:0] last_one;
    logic [NUM_BLOCKS-1:0] empty;
	logic [NUM_BLOCKS-1:0] ddout_ready;
	logic [NUM_BLOCKS-1:0] pop_enable;
	
	// output from the inter-APP/APU FIFO
	gblock_type o_jet [NUM_BLOCKS-1:0];
	// input to the inter-APP/APU FIFO
	gblock_type ddout_bridge [NUM_BLOCKS-1:0];
	logic ddout_valid_bridge [NUM_BLOCKS-1:0];
    

   // xpm_cdc_async_rst: Asynchronous Reset Synchronizer
   // Xilinx Parameterized Macro, version 2024.1
	logic reset1;
   xpm_cdc_async_rst #(
      .DEST_SYNC_FF(4),    // DECIMAL; range: 2-10
      .INIT_SYNC_FF(0),    // DECIMAL; 0=disable simulation init values, 1=enable simulation init values
      .RST_ACTIVE_HIGH(1)  // DECIMAL; 0=active low reset, 1=active high reset
   )
   xpm_cdc_async_rst_inst (
      .dest_arst(reset1), // 1-bit output: src_arst asynchronous reset signal synchronized to destination
                             // clock domain. This output is registered. NOTE: Signal asserts asynchronously
                             // but deasserts synchronously to dest_clk. Width of the reset signal is at least
                             // (DEST_SYNC_FF*dest_clk) period.

      .dest_clk(clk),   // 1-bit input: Destination clock.
      .src_arst(reset)    // 1-bit input: Source asynchronous reset signal.
   );

   // End of xpm_cdc_async_rst_inst instantiation
				
    genvar BLOCK;
    
    generate
    	for(BLOCK=0;BLOCK<NUM_BLOCKS;BLOCK++) begin : wta_block
    		// input block
			wta_in #(.NUM_STAGES(NUM_SORT_STAGES),
					.OBJECT_TYPE(gblock_type),
					.BLOCK(BLOCK)) wta_in_0 (
				.clk(clk),
				.reset(reset1),
				.ddin(ddin[BLOCK]),
				.ddin_valid(ddin_valid[BLOCK]),
				.ddout(ddout_bridge[BLOCK]),
				.ddout_valid(ddout_valid_bridge[BLOCK]),
				.ddout_ready(ddout_ready[BLOCK]),
				.new_event(new_event),
				.end_event(end_event),
				.fsm_go(fsm_go),
				.fsm_busy(fsm_busy[BLOCK]),
				.fsm_done(fsm_done[BLOCK])
			);
			
			// inter-APP/APU FIFO
			clist_fifo #(.OBJECT_TYPE(gblock_type),
						.NUM_SLOTS(MAIN_CLIST_SIZE)) cf_0 (
				.clk(clk),
				.reset(reset1),
				.clear(new_event),
				.thread_id_in(2'b00), // for now
				.thread_id_out(2'b00), // for now
				.push_enable(ddout_valid_bridge[BLOCK]),
				.pop_enable(pop_enable[BLOCK]),
				.jet(ddout_bridge[BLOCK]),
				.o_jet(o_jet[BLOCK]),
				.last_one(last_one[BLOCK]),
				.empty(empty[BLOCK])
			);
			
			// output block
			wta_out #(.NUM_STAGES(NUM_STAGES),
					.OBJECT_TYPE(gblock_type),
					.BLOCK(BLOCK)) wta_out_0 (
				.clk(clk),
				.reset(reset1),
				.new_event(new_event),
				.fsm_go(fsm_done[BLOCK]),
				.pop_enable(pop_enable[BLOCK]),
				.jet(o_jet[BLOCK]),
				.last_one(last_one[BLOCK]),
				.empty(empty[BLOCK]),
				.ddout_ready(ddout_ready[BLOCK]),
				.ddout_valid(ddout_valid[BLOCK]),
				.ddout(ddout[BLOCK]),
				.out_fsm_busy(out_fsm_busy[BLOCK]),
				.out_fsm_done(out_fsm_done[BLOCK])
			);
			
		end
	endgenerate
endmodule
