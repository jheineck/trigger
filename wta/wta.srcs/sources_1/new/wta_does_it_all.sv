`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06/10/2024 11:10:27 AM
// Design Name: 
// Module Name: wta
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "defines.vh"

module wta #(parameter NUM_STAGES = 10,
			parameter BLOCK = 0,
			parameter type OBJECT_TYPE = gblock_type) (
    input logic clk,
    input logic reset,
`ifdef RESORTABLE
	input logic resort_seeds,
`endif
	input OBJECT_TYPE ddin,
	input logic ddin_valid,
	output OBJECT_TYPE ddout,
	output logic ddout_valid,
    input logic new_event,
    input logic end_event,
    input logic fsm_go,
    output logic fsm_busy,
    output logic fsm_done
    );
	
	e_stagesort_input_mode stagesort_input_mode;
	logic disabled;
	OBJECT_TYPE roll_out;
	logic [NUM_STAGES-1:0] assoc_winner;
	logic anybusy;
	logic [NUM_STAGES-1:0] redundant;
	
	logic [NUM_STAGES:0] empty;
	logic [NUM_STAGES:0] pop;
	integer rollout_count;
	OBJECT_TYPE ddin_mux;
	OBJECT_TYPE stage_clist_out;
	typedef enum integer {
		FSM_IDLE,
		FSM_LOAD,
`ifdef WHAT_THE_HELL_IS_GOING_ON
		FSM_STAGE_CLISTS,
`endif
`ifdef RESORTABLE
		FSM_RESORT,
`endif
		FSM_UNLOAD,
		FSM_DONE
	} FSM_STATE;
	FSM_STATE fsm_state;
	typedef enum integer {
		FSM_RETURN_STAGE_CLISTS,
		FSM_RETURN_UNLOAD
	} FSM_RETURN;
	FSM_RETURN return_address;
	logic [($clog2(NUM_STAGES)):0] counter;
	logic [NUM_STAGES:0] last_one;
	logic wait_state;
`ifdef RESORTABLE
	logic resort_mode;
`endif
`ifdef WHAT_THE_HELL_IS_GOING_ON
	assign ddin_mux = (fsm_state == FSM_STAGE_CLISTS) ? stage_clist_out :
		(((ddin_valid)?ddin:0));
`else
	assign ddin_mux = (((ddin_valid)?ddin:0));
`endif
	assign ddout_valid = ((fsm_state == FSM_UNLOAD) && (!disabled) && (counter != 0));
	
	stageblock #(.NUM_STAGES(NUM_STAGES),
				.BLOCK(BLOCK),
				.OBJECT_TYPE(gblock_type)) sb_0 (
		.clk(clk),
		.reset(reset),
`ifdef RESORTABLE
		.resort_mode(resort_mode),
`endif
		.stagesort_input_mode(stagesort_input_mode),
		.ddin(ddin_mux),
		.wait_state(wait_state),
		.new_event(new_event),
		.end_event(end_event),
`ifdef RESORTABLE
		.update_in_place(!resort_seeds),
`else
		.update_in_place(1'b1),
`endif
		.ddout(ddout),
		.disabled(disabled),
		.roll_out(roll_out),
		.anybusy(anybusy),
		.redundant(redundant),
		.empty(empty),
		.pop(pop),
		.stage_clist_out(stage_clist_out),
		.last_one(last_one),
		.assoc_winner(assoc_winner)
	);
	
	function only_one (input [NUM_STAGES:0] arg);
		integer i;
		logic found_one, found_more;
		begin
			found_one = 0; found_more = 0;
			for(i=0;i<NUM_STAGES+1;i++) begin
				if(!found_one) begin
					if(arg[i]) begin
						found_one = 1;
					end
				end else if(arg[i]) begin
					found_more = 1;
					break;
				end
			end
			only_one = (found_one & !found_more);
		end
	endfunction
	logic time_to_go;
	logic do_stage_pops;
`ifndef DO_IT_COMBINATORIALLY
	assign pop = ((!(&empty))&&(do_stage_pops)) ? ((empty + 1) & ~empty) : 0;
	assign time_to_go = (only_one(~empty) & only_one(last_one));
`else // THIS WON"T WORK, JUST TRYING TO FIND THE TIMING PROBLEM!!!!  TODO
	always @(posedge clk or posedge reset) begin
		if(reset) begin
			pop <= 0;
			time_to_go <= 0;
		end else begin
			pop <= ((!(&empty))&&(do_stage_pops)) ? ((empty + 1) & ~empty) : 0;
			time_to_go <= (only_one(~empty) & only_one(last_one));
		end
	end
`endif
	always @(posedge clk or posedge reset) begin
		if(reset) begin
			fsm_busy <= 0;
			fsm_done <= 0;
			stagesort_input_mode <= STAGESORT_IDLE;
`ifdef RESORTABLE
			resort_mode <= 0;
`endif
			counter <= 0;
			do_stage_pops <= 0;
			wait_state <= 0;
			fsm_state <= FSM_IDLE;
			return_address <= FSM_RETURN_UNLOAD;
		end else begin
			case(fsm_state)
				default: begin
					fsm_state <= FSM_IDLE;
				end
				FSM_IDLE: begin
					if(fsm_go) begin
						fsm_state <= FSM_LOAD;
//						wait_state <= 0;
						counter <= 2;
						fsm_busy <= 1;
						stagesort_input_mode <= STAGESORT_LOAD;
					end
				end
				FSM_LOAD: begin
					if(fsm_busy & !fsm_go) begin
						if(|counter) begin
							counter <= counter - 1;
						end else if(!anybusy) begin
`ifdef WHAT_THE_HELL_IS_GOING_ON
							if(&empty) begin // anything in the stage clists?
								// no
//								if(clist_empty) begin // anything in the main clist?
									// no
								fsm_state <= FSM_UNLOAD;
								stagesort_input_mode <= STAGESORT_UNLOAD;
								counter <= NUM_STAGES;
//								end else begin
									// yes, something in the main clist
//									stagesort_input_mode <= STAGESORT_CONSTITUENT;
//									clist_pop <= 1;
//									fsm_state <= FSM_MAIN_CLIST;
//								end
							// yes, something in the stage clists
							end else begin
								stagesort_input_mode <= STAGESORT_CONSTITUENT;
								fsm_state <= FSM_STAGE_CLISTS;
								do_stage_pops <= 1;
							end
	`else
fsm_state <= FSM_UNLOAD;
stagesort_input_mode <= STAGESORT_UNLOAD;
counter <= NUM_STAGES;
`endif
						end
					end
				end
`ifdef WHAT_THE_HELL_IS_GOING_ON
				FSM_STAGE_CLISTS: begin
					if(time_to_go) begin
						// no
						do_stage_pops <= 0;
//						if(clist_empty) begin // anything in the main clist?
							// no
`ifdef RESORTABLE
						if((resort_seeds)&&(assoc_winner != 0)) begin
`ifdef SIMULATION_DEBUG
$display("%05d block %0d, FSM_STAGE_CLISTS got one going to UNLOAD %b",$time,BLOCK,assoc_winner);
`endif
							return_address <= FSM_RETURN_UNLOAD;
							fsm_state <= FSM_RESORT;
							stagesort_input_mode <= STAGESORT_LOAD;
							resort_mode <= 1;
						end else begin
`else
						begin
`endif
							fsm_state <= FSM_UNLOAD;
							stagesort_input_mode <= STAGESORT_UNLOAD;
							counter <= NUM_STAGES;
						end
//						end else begin
							// yes, something in the main clist
//`ifdef RESORTABLE
//							if((resort_seeds)&&(assoc_winner != 0)) begin
//`ifdef SIMULATION_DEBUG
//$display("%05d FSM_STAGE_CLISTS got one going to MAIN %b",$time,assoc_winner);
//`endif
//								return_address <= FSM_RETURN_MAIN_CLIST;
//								fsm_state <= FSM_RESORT;
//								stagesort_input_mode <= STAGESORT_LOAD;
//								resort_mode <= 1;
//							end else begin
//`else
//							begin
//`endif
//								fsm_state <= FSM_MAIN_CLIST;
//								clist_pop <= 1;
//							end
//						end
					end else begin
						// yes, something in the stage clists
`ifdef RESORTABLE
						if((resort_seeds)&&(assoc_winner != 0)) begin
`ifdef SIMULATION_DEBUG
$display("%05d block %0d, FSM_STAGE_CLISTS got one going to itself %b",$time,BLOCK,assoc_winner);
`endif
							// fsm_state <= FSM_STAGE_CLISTS; // superfluous
							return_address <= FSM_RETURN_STAGE_CLISTS;
							fsm_state <= FSM_RESORT;
							do_stage_pops <= 0;
							stagesort_input_mode <= STAGESORT_LOAD;
							resort_mode <= 1;
						end else begin
`else
						begin
`endif
							do_stage_pops <= 1;
						end
					end
				end
`endif
				FSM_UNLOAD: begin
					counter <= counter - 1;
					if(counter == 0) begin
						fsm_busy <= 0;
						fsm_done <= 1;
						stagesort_input_mode <= STAGESORT_IDLE;
						fsm_state <= FSM_DONE;
					end
				end
				FSM_DONE: begin
					fsm_state <= FSM_IDLE;
					fsm_done <= 0;
				end
`ifdef RESORTABLE
				FSM_RESORT: begin
					if(!wait_state) begin
						wait_state <= 1'b1;
					end else if(anybusy) begin
					end else begin
						wait_state <= 1'b0;
						if(return_address == FSM_RETURN_STAGE_CLISTS) begin
							fsm_state <= FSM_STAGE_CLISTS;
							stagesort_input_mode <= STAGESORT_CONSTITUENT;
							resort_mode <= 0;
							do_stage_pops <= 1;
						end else if(return_address == FSM_RETURN_MAIN_CLIST) begin
							fsm_state <= FSM_MAIN_CLIST;
							stagesort_input_mode <= STAGESORT_CONSTITUENT;
							resort_mode <= 0;
							clist_pop <= 1; // ***
						end else begin // return_address == FSM_UNLOAD
							fsm_state <= FSM_UNLOAD;
							stagesort_input_mode <= STAGESORT_UNLOAD; // ***
							resort_mode <= 0;
							counter <= NUM_STAGES; // ***
						end
					end
				end
`endif
			endcase
		end
	end

`ifdef SIMULATION_DEBUG
	always @(posedge clk or posedge reset) begin
		if(reset) begin
			rollout_count <= 0;
		end else begin
			if(new_event) begin
				rollout_count <= 0;
			end else if(fsm_done) begin
				$display("block %0d, %0d rollouts",BLOCK,rollout_count);
			end else if(roll_out.ptt != 0) begin
				$display("%5d block %0d, roll_out %p",$time,BLOCK,roll_out);
				rollout_count <= rollout_count + 1;
			end
		end
	end
`endif

endmodule
