`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Justin Heinecke
// 
// Create Date: 06/10/2024 11:10:27 AM
// Design Name: wta
// Module Name: wta
// Project Name: wta
// Target Devices: 
// Tool Versions: 
// Description: instantiates the stageblock array of stagesort stages
//   sequences through loading/sorting and unloading to inter-APP/APU FIFO
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "defines.vh"

module wta_in #(parameter NUM_STAGES = 10,
			parameter BLOCK = 0,
			parameter type OBJECT_TYPE = gblock_type) (
    input logic clk,
    input logic reset,
	input OBJECT_TYPE ddin,
	input logic ddin_valid,
	output OBJECT_TYPE ddout,
	output logic ddout_valid,
	input logic ddout_ready,
    input logic new_event,
    input logic end_event,
    input logic fsm_go,
    output logic fsm_busy,
    output logic fsm_done
    );
	
	e_stagesort_input_mode stagesort_input_mode;
	logic disabled;
	OBJECT_TYPE roll_out;
	logic anybusy;
//	logic [NUM_STAGES-1:0] redundant;
	
	logic [1:0] empty;
	logic [1:0] pop;
	
	OBJECT_TYPE ddin_mux;
	OBJECT_TYPE stage_clist_out;
	typedef enum integer {
		FSM_IDLE,
		FSM_LOAD,
		FSM_UNLOAD,
		FSM_STAGE_CLISTS,
		FSM_DONE
	} FSM_STATE;
	FSM_STATE fsm_state;
//	typedef enum integer {
//		FSM_RETURN_STAGE_CLISTS,
//		FSM_RETURN_UNLOAD
//	} FSM_RETURN;
//	FSM_RETURN return_address;
//	logic [($clog2(NUM_STAGES)):0] counter;
	logic [($clog2(MAX_CONSTIT)):0] counter;
	logic [1:0] last_one;
	assign ddin_mux = (((ddin_valid)?ddin:0));
	assign ddout_valid = ((fsm_state == FSM_UNLOAD) && (!disabled) && (counter != 0)) ||
							(fsm_state == FSM_STAGE_CLISTS);
	OBJECT_TYPE ddout_seed;
	const logic [FLAGS_NUM_BITS-1:0] constit_flag = 1<<FLAGS_CONSTIT_BIT;
	const OBJECT_TYPE constit_template = '{flags:constit_flag,default:0};
//	assign ddout = (fsm_state == FSM_STAGE_CLISTS) ? stage_clist_out : ddout_seed;
	always_comb begin
		if(fsm_state == FSM_STAGE_CLISTS) begin
			if(pop[1]) begin
				ddout <= (stage_clist_out | constit_template);
			end else begin
				ddout <= stage_clist_out;
			end
		end else begin
			ddout <= ddout_seed;
		end
	end
	
	stageblock #(.NUM_STAGES(NUM_STAGES),
				.BLOCK(BLOCK),
				.OBJECT_TYPE(gblock_type)) sb_0 (
		.clk(clk),
		.reset(reset),
		.stagesort_input_mode(stagesort_input_mode),
		.ddin(ddin_mux),
		.new_event(new_event),
		.end_event(end_event),
		.ddout(ddout_seed),
		.disabled(disabled),
		.roll_out(roll_out),
		.anybusy(anybusy),
//		.redundant(redundant),
		.empty(empty),
		.pop(pop),
		.stage_clist_out(stage_clist_out),
		.last_one(last_one)
	);
	
	function only_one (input [1:0] arg);
		integer i;
		logic found_one, found_more;
		begin
			found_one = 0; found_more = 0;
			for(i=0;i<1+1;i++) begin
				if(!found_one) begin
					if(arg[i]) begin
						found_one = 1;
					end
				end else if(arg[i]) begin
					found_more = 1;
					break;
				end
			end
			only_one = (found_one & !found_more);
		end
	endfunction

	logic time_to_go;
	logic do_stage_pops;
	assign pop = ((!(&empty))&&(do_stage_pops)) ? ((empty + 1) & ~empty) : 0;
	assign time_to_go = (only_one(~empty) & only_one(last_one));

	always @(posedge clk or posedge reset) begin
		if(reset) begin
			fsm_busy <= 0;
			fsm_done <= 0;
			stagesort_input_mode <= STAGESORT_IDLE;
			counter <= 0;
			do_stage_pops <= 0;
			fsm_state <= FSM_IDLE;
		end else begin
			case(fsm_state)
				default: begin
					fsm_state <= FSM_IDLE;
				end
				FSM_IDLE: begin
					if(fsm_go) begin
						fsm_state <= FSM_LOAD;
						counter <= 2;
						fsm_busy <= 1;
						stagesort_input_mode <= STAGESORT_LOAD;
					end
				end
				FSM_LOAD: begin
					if(fsm_busy & !fsm_go) begin
						if(|counter) begin
							counter <= counter - 1;
						end else if(!anybusy) begin
							fsm_state <= FSM_UNLOAD;
							stagesort_input_mode <= STAGESORT_UNLOAD;
							counter <= NUM_STAGES;
						end
					end
				end
				FSM_UNLOAD: begin
					if(|counter) begin
						counter <= counter - 1;
					end else if(ddout_ready) begin
						if(&empty) begin
							fsm_busy <= 0;
							fsm_done <= 1;
							stagesort_input_mode <= STAGESORT_IDLE;
							fsm_state <= FSM_DONE;
						end else begin
							stagesort_input_mode <= STAGESORT_IDLE;
							fsm_state <= FSM_STAGE_CLISTS;
							do_stage_pops <= 1;
							counter <= MAX_CONSTIT;
						end
					end
				end
				
				FSM_STAGE_CLISTS: begin
					if(|counter) begin
						counter <= counter - 1;
					end
					if(time_to_go || !(|counter)) begin
						// no
						do_stage_pops <= 0;
						fsm_done <= 1;
						fsm_busy <= 0;
						fsm_state <= FSM_DONE; // FSM_UNLOAD;
						stagesort_input_mode <= STAGESORT_IDLE;
					end
				end

				FSM_DONE: begin
					fsm_state <= FSM_IDLE;
					fsm_done <= 0;
				end
			endcase
		end
	end

endmodule
