`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Justin Heinecke
// 
// Create Date: 07/23/2024 03:47:36 PM
// Design Name: wta
// Module Name: wta_out
// Project Name: wta
// Target Devices: 
// Tool Versions: 
// Description: instantiates the output stageblock_out
//  sequences through first loading up the stages with seeds,
//  then through all the constituents,
//  then unloading the seeds
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "defines.vh"

module wta_out #(parameter NUM_STAGES = 10,
			parameter BLOCK = 0,
			parameter type OBJECT_TYPE = gblock_type) (
    input logic clk,
    input logic reset,
    input logic new_event,
    input logic fsm_go,
    output logic pop_enable,
    input OBJECT_TYPE jet,
    input logic last_one,
    input logic empty,
    output logic ddout_ready,
    output logic ddout_valid,
    output OBJECT_TYPE ddout,
    output out_fsm_busy,
    output out_fsm_done
    );
    
    logic load, constit;
    logic fsm_busy, fsm_done;
    assign out_fsm_busy = fsm_busy;
    assign out_fsm_done = fsm_done;
    
 	logic [($clog2(NUM_STAGES)):0] counter;
 	logic [($clog2(NUM_STAGES)):0] stage_num;
 	logic [($clog2(NUM_STAGES)):0] num_seeds;
 	logic ddout_valid_state;
	assign ddout_valid = ddout_valid_state & (ddout.flags[FLAGS_CORE_BIT]);
    
    stageblock_out #(.NUM_STAGES(NUM_STAGES),
    				.BLOCK(BLOCK),
    				.OBJECT_TYPE(OBJECT_TYPE)) sbo_0 (
    	.clk(clk),
    	.reset(reset),
    	.new_event(new_event),
    	.load(load&(!jet.flags[0])),
    	.constit(constit|(load&jet.flags[FLAGS_CONSTIT_BIT])),
    	.num_seeds(num_seeds),
    	.stage_num(stage_num),
    	.jet(jet),
    	.ddout(ddout)
    );
    
    typedef enum integer {
    	FSM_IDLE,
    	FSM_LOAD,
    	FSM_STAGE_CLISTS,
    	FSM_UNLOAD,
    	FSM_DONE
    } FSM_STATE;
    FSM_STATE fsm_state;
    
	always @(posedge clk or posedge reset) begin
    	if(reset) begin
    		fsm_state <= FSM_IDLE;
    		pop_enable <= 0;
    		ddout_ready <= 0;
    		counter <= 0;
    		pop_enable <= 0;
    		load <= 0;
    		constit <= 0;
//    		num_seeds <= 0;
    		ddout_valid_state <= 0;
    		fsm_busy <= 0;
    		fsm_done <= 0;
    		stage_num <= 0;
    	end else begin
    		case(fsm_state)
    			FSM_IDLE: begin
    				if(fsm_go & !empty) begin
    					fsm_state <= FSM_LOAD;
    					fsm_busy <= 1;
    					load <= 1;
    					pop_enable <= 1;
    					ddout_ready <= 0;
//    					num_seeds <= 1;
    					counter <= 0;
    					stage_num <= 0;
    				end else begin
    					ddout_ready <= 1;
    				end
    			end
    			FSM_LOAD: begin
    				if(last_one) begin // sorry, no constituents
    					fsm_state <= FSM_UNLOAD;
    					ddout_valid_state <= 1;
    					load <= 0;
    					if(counter != (NUM_STAGES-1)) begin
	    					counter <= counter + 1;
	    				end
//    					stage_num <= ~0;
    					pop_enable <= 0;
    				end else if(jet.flags[0]) begin // first constituent is on the queue
    					load <= 0;
    					constit <= 1;
    					fsm_state <= FSM_STAGE_CLISTS;
    				end else begin
//    					num_seeds <= num_seeds + 1;
    					if(counter != (NUM_STAGES-1)) begin
	    					counter <= counter + 1;
	    				end
//    					stage_num <= stage_num + 1;
    				end
    			end
    			FSM_STAGE_CLISTS: begin
    				if(last_one) begin
    					constit <= 0;
    					fsm_state <= FSM_UNLOAD;
    					ddout_valid_state <= 1;
    					counter <= num_seeds - 1;
    					stage_num <= 0;
    					pop_enable <= 0;
    				end
    			end
    			FSM_UNLOAD: begin
    				counter <= counter - 1;
    				stage_num <= stage_num + 1;
    				if(counter == 1) begin
    					fsm_done <= 1;
    					fsm_busy <= 0;
    					fsm_state <= FSM_DONE;
    				end
    			end
    			FSM_DONE: begin
    				ddout_valid_state <= 0;
    				fsm_state <= FSM_IDLE;
    				fsm_done <= 0;
    			end
    			default: begin
    				fsm_state <= FSM_IDLE;
    			end
    		endcase
    	end
    end
    	
    
endmodule
